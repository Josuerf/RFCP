<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Namechange Logs - RFCP";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th class='align-left'>ID</th>";
    $out .= "<th class='align-left'>Char ID</th>";
    $out .= "<th class='align-left'>Old Name</th>";
    $out .= "<th class='align-left'>New Name</th>";
    $out .= "<th class='align-left'>IP</th>";
    $out .= "</tr>";

    $sql = "SELECT id, username_charid, username_oldname, username_newname, username_ip ";
    $sql .= "FROM gamecp_username_log ";
    $sql .= "WHERE id NOT IN ( SELECT TOP [OFFSET] id FROM gamecp_username_log ";
    $sql .= "ORDER BY id DESC) ORDER BY id DESC";
    $sql_count = "Select COUNT(id) FROM gamecp_username_log";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 30, $links_to_show = 10);
    $results = $pager -> get_data();

    foreach ($results["rows"] as $key => $row)
    {
      $out .= "<tr>";
      $out .= "<td nowrap>" . $row['id'] . "</td>";
      $out .= "<td nowrap>" . $row['username_charid'] . "</td>";
      $out .= "<td nowrap>" . $row['username_oldname'] . "</td>";
      $out .= "<td nowrap>" . $row['username_newname'] . "</td>";
      $out .= "<td nowrap>" . $row['username_ip'] . "</td>";
      $out .= "</tr>";
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No name change logs found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
