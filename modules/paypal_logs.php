<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Paypal Logs";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th class='align-left'>ID</th>";
    $out .= "<th class='align-left'>Transaction ID</th>";
    $out .= "<th class='align-left'>Amount</th>";
    $out .= "<th class='align-left'>Fee</th>";
    $out .= "<th class='align-left'>Account ID</th>";
    $out .= "<th class='align-left'>Credits</th>";
    $out .= "<th class='align-left'>Time</th>";
    $out .= "<th class='align-left'>Payer ID</th>";
    $out .= "<th class='align-left'>Payer Email</th>";
    $out .= "<th class='align-left'>Verified</th>";
    $out .= "<th class='align-left'>Duplicate</th>";
    $out .= "</tr>";

    $sql = "SELECT id, tranid, amount, fee, userid, credits, time, payerid, payeremail, verified, dupetran ";
    $sql .= "FROM gamecp_paypal ";
    $sql .= "WHERE id NOT IN ( SELECT TOP [OFFSET] id FROM gamecp_paypal ";
    $sql .= "ORDER BY id DESC) ORDER BY id DESC";
    $sql_count = "SELECT COUNT(id) FROM gamecp_paypal";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 50, $links_to_show = 10);
    $results = $pager -> get_data();
    foreach ($results["rows"] as $key => $row)
    {
      $out .= "<tr>";
      $out .= "<td nowrap>" . $row['id'] . "</td>";
      $out .= "<td nowrap>" . $row['tranid'] . "</td>";
      $out .= "<td nowrap>" . $row['amount'] . "</td>";
      $out .= "<td nowrap>" . $row['fee'] . "</td>";
      $out .= "<td nowrap>" . $row['userid'] . "</td>";
      $out .= "<td nowrap>" . $row['credits'] . "</td>";
      $out .= "<td nowrap>" . $row['time'] . "</td>";
      $out .= "<td nowrap>" . $row['payerid'] . "</td>";
      $out .= "<td nowrap>" . $row['payeremail'] . "</td>";
      $out .= "<td nowrap>" . $row['verified'] . "</td>";
      $out .= "<td nowrap>" . $row['dupetran'] . "</td>";
      $out .= "</tr>";
    }
    $out .= "<tr>";

    function comma_dot($str)
    {
      return(str_replace(',', '.', $str));
    }

    $sqlsum = "SELECT amount, fee FROM gamecp_paypal";
    $con = connectdb(RFCP);
    $result = sqlsrv_query($con, $sqlsum);
    $sumarray = array ();
    $taxarray = array ();

    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
    {
      $sumarray[] = intval($row['amount']);
      $taxarray[] = $row['fee'];
    }
    $newsum = (array_sum($sumarray) - array_sum($taxarray));

    $out .= "<td colspan=\"3\" style=\"text-align: center; font-weight: bold;\">Total sum:" . array_sum($sumarray) . "</td>";
    $out .= "<td colspan=\"3\" style=\"text-align: center; font-weight: bold;\">Total tax:" . array_sum(array_map('comma_dot', $taxarray)) . "</td>";
    $out .= "<td colspan=\"3\" style=\"text-align: center; font-weight: bold;\">Total:" . $newsum . "</td>";
    $out .= "</tr>";
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No paypal logs found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
