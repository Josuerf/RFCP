<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Guild Market Admin";
  $moduleLabel = "Manage Items";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $categories_attempt = get_market_categories(-1, -1, true);
    $categories = $categories_attempt["categories"];

    $page = isset($_REQUEST['page']) ? strtolower($_REQUEST['page']) : "";
    $cat_id = isset($_GET['cat_id']) && ctype_digit($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
    $query_p1 = "";
    $exit_cat = 0;
    $exit_stage = 0;
    $j = 0;
    $k = 0;
    $ups = 0;
    $sub_name = "";

    $links = array ();
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "'>View Items</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $cat_id . "&page=addedit'>Add New Item</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $cat_id . "&page=addarmor'>Add Armor Sets</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $cat_id . "&page=addweapon'>Add Weapon Sets</a>";

    $active_index = 0;
    if ($page == "addedit")
    {
      $active_index = 1;
    }
    else if ($page == "addarmor")
    {
      $active_index = 2;
    }
    else if ($page == "addweapon")
    {
      $active_index = 3;
    }
    else if ($page == "delete")
    {
      $active_index = 4;
      $links[] = "<a href='#'>Delete Item</a>";
    }

    $out .= generate_module_nav_html($links, $active_index);

    if (empty($page))
    {
      $categories_attempt = get_market_categories($sub_cat_id = $cat_id, -1, true);
      $current_categories = $categories_attempt["categories_in_order"];
      if ($categories_attempt["error"] === False)
      {
        $total_categories = count($current_categories);
        if ($cat_id > 0)
        {
          $by_category = " AND item_cat_id = '" . $cat_id . "'";
        }
        else
        {
          $by_category = "";
        }

        $order = isset($config['market_order_by']) ? $config['market_order_by'] : 1;
        $sort = isset($config['market_sort']) ? $config['market_sort'] : 1;

        if ($order == 1)
        {
          $order = "item_name";
        }
        else if ($order == 2)
        {
          $order = "item_price";
        }
        else if ($order == 3)
        {
          $order = "item_race";
        }
        else if ($order == 4)
        {
          $order = "item_buy_count";
        }
        else if ($order == 5)
        {
          $order = "item_date_added";
        }
        else if ($order == 6)
        {
          $order = "item_date_updated";
        }
        else if ($order == 7)
        {
          $order = "item_dbcode";
        }
        else
        {
          $order = "item_name";
        }

        if ($sort == 1)
        {
          $sort = "ASC";
        }
        else
        {
          $sort = "DESC";
        }

        include( "./core/pagination.php" );

        $sql = "SELECT item_id,item_name,item_dbcode,item_amount,item_upgrade,item_description,item_image_url,";
        $sql .= "item_price,item_buy_count,item_date_added,item_date_updated,item_race ";
        $sql .= "FROM gamecp_g_market_items WHERE item_delete = 0 {$by_category} ";
        $sql .= "AND item_id NOT IN ( SELECT TOP [OFFSET] item_id FROM gamecp_g_market_items ";
        $sql .= "WHERE item_delete = 0 {$by_category} ORDER BY item_dbcode {$sort}) ORDER BY item_dbcode {$sort}";
        $sql_count = "Select COUNT(item_id) FROM gamecp_g_market_items WHERE item_delete = 0 {$by_category}";

        $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
        $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

        $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 20, $links_to_show = 11);
        $results = $pager -> get_data();

        $current_cat_id = $cat_id;
        $nav = array ();
        $nav[] = "<a href=\"" . $script_name . "?action=" . $_GET['action'] . "\" style=\"\">Categories</a>";
        $temp = array ();
        while ($current_cat_id > 0)
        {
          $temp[] = $categories[$current_cat_id]["link"];
          $current_cat_id = $categories[$current_cat_id]["parent"];
        }
        $i = count($temp) - 1;
        while ($i >= 0)
        {
          $nav[] = $temp[$i];
          $i = $i - 1;
        }

        $out .= "<div>";
        $out .= get_breadcrumb_html($nav);
        $out .= generate_category_html($current_categories);
        $out .= "</div>";

        $out .= "<table class=\"\" border=\"0\" width=\"100%\" align=\"center\">";
        $out .= "<tr>";
        $out .= "<td colspan=\"3\" style=\"padding-top: 15px;\">&nbsp;</td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td class=\"\" colspan=\"3\">";
        $out .= "<table class=\"ink-table\" width=\"100%\" cellpadding=\"6\" cellspacing=\"1\" style=\"padding-top: 10px;\">";
        $out .= "<tr>";
        $out .= "<td valign=\"top\" nowrap>ID</td>";
        $out .= "<td valign=\"top\" nowrap>Date Added</td>";
        $out .= "<td valign=\"top\" nowrap>Item Name</td>";
        $out .= "<td valign=\"top\" nowrap>Upgrades</td>";
        $out .= "<td valign=\"top\" nowrap>Price</td>";
        $out .= "<td valign=\"top\" nowrap># bought</td>";
        $out .= "<td valign=\"top\" style=\"text-align: center;\" nowrap></td>";
        $out .= "</tr>";
        foreach ($results["rows"] as $key => $item)
        {
          $k_value = $item['item_dbcode'];
          $u_value = $item['item_upgrade'];
          $item_details = get_item_details($k_value, $u_value);
          $item_code = get_item_code_from_id($item_details["type"], $item_details["id"]);

          if ($item['item_race'] == 1)
          {
            $race = "<span style=\"color: #CC6699;\">Belleto</span>";
          }
          else if ($item['item_race'] == 2)
          {
            $race = "<span style=\"color: #9933CC;\">Cora</span>";
          }
          else if ($item['item_race'] == 3)
          {
            $race = "<span style=\"color: grey;\">Accreatian</span>";
          }
          else if ($item['item_race'] == 4)
          {
            $race = "<span style=\"color: #CC6699;\">Belleto</span> & <span style=\"color: #9933CC;\">Cora</span>";
          }
          else
          {
            $race = "All Races";
          }
          if ($userdata -> credits < $item['item_price'])
          {
            $disable_button = " disabled=\"disabled\"";
          }
          else
          {
            $disable_button = "";
          }
          if ($item['item_image_url'] != "")
          {
            $item_image = $item['item_image_url'];
          }
          else
          {
            $item_image = "./images/items/unknown.gif";
          }
          $out .= "<tr>";
          $out .= "<td nowrap>" . $item['item_id'] . "</td>";
          $out .= "<td nowrap>" . date("m/d/y h:i:s A", $item['item_date_added']) . "</td>";
          $out .= "<td class=\"edit_label\" nowrap>[" . $item_code . "] " . $item['item_name'] . "</td>";
          $out .= "<td nowrap>" . $item_details["images"] . "</td>";
          $out .= "<td nowrap>" . round($item['item_price']) . " NC</td>";
          $out .= "<td nowrap>" . $item['item_buy_count'] . "</td>";
          $out .= "<td style=\"text-align: center;\" nowrap>";
          $out .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=addedit&edit_item_id=" . $item['item_id'] . "\"><i class=\"icon-wrench icon-large\" title=\"Edit\"></i></a>";
          $out .= "<a class=\"icon_link " . BASE_COLOR . "\" href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=delete&item_id=" . $item['item_id'] . "\"><i class=\"icon-trash icon-large\" title=\"Delete\"></i></a>";
          $out .= "</td>";
          $out .= "</tr>";
        }
        $out .= "</table>";
        $out .= "</td>";
        $out .= "</tr>";
        $out .= "<tr>";
        $out .= "<td colspan=\"3\" style=\"padding-top: 15px;\">&nbsp;</td>";
        $out .= "</tr>";

        if (count($results["rows"]) <= 0)
        {
          $out .= "<tr>";
          $out .= "<td class=\"\" colspan=\"3\" style=\"text-align: center; font-weight: bold;\">No items were found in this category</td>";
          $out .= "</tr>";
        }
        else
        {
          $out .= "<tr>";
          $out .= "<td colspan=\"3\">" . $pager -> renderFullNav() . "</td>";
          $out .= "</tr>";
        }
        $out .= "</table>";
      }
    }
    else
    {
      if ($page == "addedit")
      {
        $add_submit = isset($_POST['add_submit']) ? 1 : 0;
        $edit_submit = isset($_POST['edit_submit']) ? 1 : 0;

        $messages = array ();
        $display_form = 1;
        $do_process = 0;
        if (isset($_POST['edit_item_id']) && ctype_digit($_POST['edit_item_id']))
        {
          $edit_item_id = intval($_POST['edit_item_id']);
        }
        else if (isset($_GET['edit_item_id']) && ctype_digit($_GET['edit_item_id']))
        {
          $edit_item_id = intval($_GET['edit_item_id']);
        }
        else
        {
          $edit_item_id = "";
        }

        $item_status = isset($_POST['item_status']) && ctype_digit($_POST['item_status']) ? antiject($_POST['item_status']) : 1;
        $item_cat_id = isset($_POST['item_cat_id']) && ctype_digit($_POST['item_cat_id']) ? antiject($_POST['item_cat_id']) : 0;
        $item_price = isset($_POST['item_price']) ? antiject($_POST['item_price']) : 0;
        $item_code = isset($_POST['item_code']) ? antiject($_POST['item_code']) : "";
        $item_name = isset($_POST['item_name']) ? antiject(trim($_POST['item_name'])) : "";
        $item_description = isset($_POST['item_description']) ? antiject(( $_POST['item_description'])) : "";
        $item_amount = isset($_POST['item_amount']) && ctype_digit($_POST['item_amount']) ? antiject($_POST['item_amount']) : 1;
        $item_custom_amount = isset($_POST['item_custom_amount']) ? antiject($_POST['item_custom_amount']) : 0;
        $item_slots = isset($_POST['item_slots']) && ctype_digit($_POST['item_slots']) ? antiject($_POST['item_slots']) : 0;
        $item_race = isset($_POST['item_race']) && ctype_digit($_POST['item_race']) ? antiject($_POST['item_race']) : 0;
        $item_image_url = isset($_POST['item_image_url']) ? antiject($_POST['item_image_url']) : "";
        $item_upgrades = isset($_POST['talics']) ? $_POST['talics'] : array ();
        $item_rent_time = isset($_POST['item_rent_time']) && ctype_digit($_POST['item_amount']) ? intval($_POST['item_rent_time']) : 0;
        $item_dbcode = 0;
        $items = array ();
        $prices = array ();

        if ($add_submit == 1 || $edit_submit == 1)
        {
          $do_process = 1;
        }
        if ($edit_item_id != "")
        {
          $page_mode = "edit_submit";
          $submit_name = "Update Item";
          $this_mode_title = "Editing a item";
          if ($do_process == 0)
          {
            $attempt = get_market_item($edit_item_id, true);
            if ($attempt["error"] === True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
              $redirect = PREVIOUS_PAGE_LONG;
              $display_form = 0;
              $do_process = 0;
            }
            else
            {
              $item = $attempt["item"];
              $item_status = $item['item_status'];
              $item_cat_id = $item['item_cat_id'];
              $item_price = $item['item_price'];
              $item_dbcode = $item['item_dbcode'];
              $item_name = $item['item_name'];
              $item_description = $item['item_description'];
              $item_amount = $item['item_amount'];
              $item_custom_amount = $item['item_custom_amount'];
              $item_upgrade = $item['item_upgrade'];
              $item_race = $item['item_race'];
              $item_image_url = $item['item_image_url'];
              $item_rent_time = $item['item_rent_time'];

              $item_details = get_item_details($item_dbcode, $item_upgrade);
              $item_code = get_item_code_from_id($item_details["type"], $item_details["id"]);
              $item_slots = $item_details["slots"];
            }
          }
        }
        else
        {
          $page_mode = "add_submit";
          $submit_name = "Add Item";
          $this_mode_title = "Adding a new item";
        }
        if ($do_process == 1)
        {
          $item_ups = 0;
          if ($item_code == "")
          {
            $messages[] = "Item Code was left blank";
          }
          if ($item_price == "")
          {
            $messages[] = "Item Price was left blank";
          }
          if (!ctype_digit($item_amount))
          {
            $messages[] = "Item amount was not a pure number";
          }
          else if ($item_amount < 0)
          {
            $messages[] = "Item amount must be >= 0";
          }
          else if (count($item_upgrades) > 0)
          {
            $empty = array ();
            $item_ups = 0;
            foreach ($item_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if ($temp < 0 || $temp == 14 || $temp > 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if ($temp > 0 && $temp < 14 && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }
              else if ($temp > 0 && $temp < 14)
              {
                $item_ups++;
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }
          }

          if (count($messages) == 0)
          {
            $item_prices = explode(",", $item_price);

            foreach ($item_prices as $key => $price)
            {
              if (ctype_digit($price))
              {
                $prices[] = intval($price);
              }
              else
              {
                $messages[] = filter_string_for_html($price) . " is not a valid price";
              }
            }
          }

          if (count($messages) == 0)
          {
            $item_codes = explode(",", $item_code);

            foreach ($item_codes as $key => $item)
            {
              $attempt = get_item_info_from_code($item);
              if ($attempt["error"] === True)
              {
                $messages[] = "Error for item " . $item . ": " . $attempt["errorMessage"];
              }
              else
              {
                $attempt["item"]["item_code"] = $item;
                $items[] = $attempt["item"];
              }
            }
          }

          if (count($messages) == 0)
          {
            //go ahead and parse the talics
            if ($item_slots < $item_ups)
            {
              $item_ups = $item_slots;
            }
            if ($item_slots > 7)
            {
              $item_slots = 7;
            }
            if (count($item_upgrades) < 7)
            {
              $i = count($item_upgrades);
              while ($i < 7)
              {
                $item_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $item_slots . "";
            foreach (array_reverse($item_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);
            //end talic parsing

            $descriptions = explode("|", $item_description);
            $names = explode("|", $item_name);
            $images = explode("|", $item_image_url);
            $temp_name = "";
            foreach ($items as $key => $item)
            {
              $item_info = $item;
              $item_id = $item_info['item_id'];
              $item_kind = parse_item_code($item["item_code"]);
              $table_name = get_table_name($item_kind);
              $item["market_race"] = $item_race;
              $item["market_name"] = "";
              if (isset($names[$key]))
              {
                $item["market_name"] = $names[$key];
              }

              if ($item["market_name"] == "")
              {
                $temp_name = str_replace("_", " ", $item_info['item_name']);
                $pos = strpos($temp_name, "(");
                if ($pos !== False && $table_name != "tbl_code_weapon")
                {
                  $race = explode("(", $temp_name);
                  $race = str_replace("(", "", $race[1]);
                  $race = str_replace(")", "", $race);

                  if ($race == "A" || $race == "Accretia")
                  {
                    $item["market_race"] = "3";
                  }
                  else if ($race == "B" || $race == "Bellato")
                  {
                    $item["market_race"] = "1";
                  }
                  else if ($race == "C" || $race == "Cora")
                  {
                    $item["market_race"] = "2";
                  }

                  $item["market_name"] = substr($temp_name, 0, $pos);
                }
                else
                {
                  $item["market_name"] = $temp_name;
                }
              }

              $item["market_description"] = "";
              $item["market_image"] = "";
              if (isset($descriptions[$key]))
              {
                $item["market_description"] = $descriptions[$key];
              }
              if (isset($images[$key]))
              {
                $item["market_image"] = $images[$key];
              }
              if (isset($prices[$key]))
              {
                $item["market_price"] = $prices[$key];
              }
              else
              {
                $item["market_price"] = $prices[0];
              }

              $item["market_amount"] = $item_amount;
              if (in_array($item_kind, $upgrade_types))
              {
                $item["market_amount"] = 0;
              }
              else if ($item_amount < 1)
              {
                $item["market_amount"] = 1;
              }

              $item["market_dbcode"] = get_item_db_code($item_id, $item_kind, 0);

              if (!in_array($item_kind, $upgrade_types))
              {
                $item["market_upgrade"] = BASE_CODE;
              }
              else
              {
                $item["market_upgrade"] = $item_upgrade;
              }

              $items[$key] = $item;
            }
          }

          if (count($messages) == 0)
          {
            $time_now = time();
            if ($add_submit == 1)
            {
              foreach ($items as $key => $item)
              {
                $attempt = add_market_item($item_status, $item_cat_id, $time_now, $time_now, $item["market_race"], $item["market_price"], $item["market_dbcode"], $item["market_upgrade"], $item["market_name"], $item["market_description"], $item["market_image"], $item["market_amount"], $item_custom_amount, $item_rent_time, true);

                if ($attempt["error"] === True)
                {
                  $messages[] = "Item " . $item["item_code"] . ": " . $attempt["errorMessage"];
                }
                else
                {
                  gamecp_log(0, $userdata -> username, "ADMIN - MANAGE ITEMS - ADDED - New item: {$item_name}", 1);
                }
              }
              if (count($messages) == 0)
              {
                $display_form = 0;
                $out .= get_notification_html("Successfully added the new item(s)!", SUCCESS);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $item_cat_id;
              }
            }
            else if ($edit_submit == 1)
            {
              $item = $items[0];
              $attempt = update_market_item($edit_item_id, $item_status, $item_cat_id, $time_now, $item["market_race"], $item["market_price"], $item["market_dbcode"], $item["market_upgrade"], $item["market_name"], $item["market_description"], $item["market_image"], $item["market_amount"], $item_custom_amount, $item_rent_time, true);
              if ($attempt["error"] === True)
              {
                $messages[] = "Error updating the item: " . $attempt["errorMessage"];
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                $out .= get_notification_html("Successfully updated this item!", SUCCESS);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $item_cat_id;
                gamecp_log(0, $userdata -> username, "ADMIN - MANAGE ITEMS - UPDATED - Item ID: {$edit_item_id} | Item Name: {$item_name}", 1);
              }
            }
          }

          if (count($messages) > 0)
          {
            $out .= "<table border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td>";
            $out .= get_notification_html($messages, ERROR);
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "</table>";
          }
        }
        if ($display_form == 1)
        {
          $item_cat_id = $item_cat_id ? $item_cat_id : $cat_id;
          generate_menu($categories, 0, "", array (), $item_cat_id);
          $subcategory_list = $options;
          $total_categories = count($categories);
          if (0 < $total_categories)
          {
            $out .= "<form class=\"ink-form\" method=\"post\">";
            $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align:center;\"><span style=\"font-weight: bold; font-size: 14pt;\">" . $this_mode_title . "</span><br/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Enable Item</span><br/><i>Enable/Disable this item from being bought</i></td>";
            if ($item_status == 0)
            {
              $check_no = " checked";
              $check_yes = "";
            }
            else
            {
              $check_yes = " checked";
              $check_no = "";
            }
            $out .= "<td class=\"\" width=\"350px\">Yes <input name=\"item_status\" type=\"radio\" value=\"1\"" . $check_yes . "/> No <input name=\"item_status\" type=\"radio\" value=\"0\"" . $check_no . "/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Category</span><br/><i>The price of game points for this item</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"item_cat_id\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Price</span><br/><i>The price of game points for this item</i></td>";
            $out .= "<td class=\"\"><input name=\"item_price\" type=\"text\" value=\"" . $item_price . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Item Code</span><br/><i>The GM Command code for the item (i.e. iwkna01)</i></td>";
            $out .= "<td class=\"\"><input name=\"item_code\" type=\"text\" value=\"" . $item_code . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Item Name</span><br/><i>Optional Name for this item (default name will be item name from the database)</i></td>";
            $out .= "<td class=\"\"><input name=\"item_name\" type=\"text\" value=\"" . $item_name . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Description</span><br/><i>A brief description that will show up on the item list</i></td>";
            $out .= "<td class=\"\"><input name=\"item_description\" type=\"text\" value=\"" . $item_description . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Amount</span><br/><i>Quantity of this item (i.e. 99 HP Pots). 0 for non-stackable items.</i></td>";
            $out .= "<td class=\"\"><input name=\"item_amount\" type=\"text\" value=\"" . $item_amount . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Rent Time</span><br/><i>Length of time to rent (in hours)</i></td>";
            $out .= "<td class=\"\"><input name=\"item_rent_time\" type=\"text\" value=\"" . $item_rent_time . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Upgrades</span><br/><i>Select slots, then upgrade.  Right click to remove talic.</i></td>";
            $out .= "<td class=\"\">";
            if (isset($item_details))
            {
              $out .= generate_upgrade_input_html($item_details);
            }
            else
            {
              $out .= generate_upgrade_input_html(array ());
            }
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Race</span><br/><i>An external URL to display an image for the item</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"item_race\">";
            $m = 0;
            while ($m <= 4)
            {
              if ($m == 0)
              {
                $race = "All Races";
              }
              else if ($m == 1)
              {
                $race = "Bellato";
              }
              else if ($m == 2)
              {
                $race = "Cora";
              }
              else if ($m == 3)
              {
                $race = "Accretia";
              }
              else if ($m == 4)
              {
                $race = "Bellato & Cora";
              }
              else
              {
                $race = "All Races";
              }
              $select_race = "";
              if ($m == $item_race)
              {
                $select_race = " selected";
              }
              $out .= "<option value=\"" . $m . "\"" . $select_race . ">" . $race . "</option>";
              ++$m;
            }
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Item Image URL</span><br/><i>An external URL to display an image for the item</i></td>";
            $out .= "<td class=\"\"><input name=\"item_image_url\" type=\"text\" value=\"" . $item_image_url . "\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align: center;\">";
            $out .= "<input name=\"page\" type=\"hidden\" value=\"addedit\"/>";
            $out .= "<input class=\"ink-button\" name=\"" . $page_mode . "\" type=\"submit\" value=\"" . $submit_name . "\"/></td>";
            $out .= "</tr>";
            $out .= "</table>";
            $out .= "</form>";
          }
          else
          {
            $out .= "No categories found";
          }
        }
      }
      else if ($page == "delete" || $page == "delete_item")
      {
        if ($page == "delete")
        {
          $item_id = isset($_POST["item_id"]) ? intval($_POST["item_id"]) : intval($_GET["item_id"]);
          if ($item_id == "")
          {
            $out .= get_notification_html("Item not found", ERROR);
            $redirect = LAST_PAGE_SHORT;
          }
          else
          {
            $attempt = get_market_item($item_id, true);
            if ($attempt["error"] === True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
              $redirect = PREVIOUS_PAGE_LONG;
            }
            else
            {
              $out .= "<form class=\"ink-form\" method=\"post\">";
              $out .= "<p style=\"text-align: center; font-weight: bold;\">Are you sure you want the delete the Item: <u>" . $attempt['item']['item_name'] . "</u>?</p>";
              $out .= "<p style=\"text-align: center;\"><input type=\"hidden\" name=\"item_id\" value=\"" . $item_id . "\"/><input type=\"hidden\" name=\"page\" value=\"delete_item\"/><input class=\"ink-button\" type=\"submit\" name=\"yes\" value=\"Yes\"/> <input class=\"ink-button\" type=\"submit\" name=\"no\" value=\"No\"/></p>";
              $out .= "</form>";
            }
          }
        }
        else
        {
          if ($page == "delete_item")
          {
            $yes = isset($_POST['yes']) ? 1 : 0;
            $no = isset($_POST['no']) ? 1 : 0;
            if (isset($_POST['item_id']) && ctype_digit($_POST['item_id']))
            {
              $item_id = antiject($_POST['item_id']);
            }
            else
            {
              $item_id = 0;
            }
            if ($no != 1 && $item_id != 0)
            {
              $attempt = get_market_item($item_id, true);
              if ($attempt["error"] === True)
              {
                $out .= get_notification_html($attempt["errorMessage"], ERROR);
                $redirect = PREVIOUS_PAGE_LONG;
              }
              else
              {
                delete_market_item($item_id, true);
                $out .= get_notification_html("Deleted the item id: " . $attempt["item"]["item_id"], SUCCESS);
                $redirect = CUSTOM_PAGE_SHORT;
                $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
                gamecp_log(3, $userdata -> username, "ADMIN - MANAGE ITEMS - DELETED - Item Name:  " . $attempt["item"]['item_name'] . " | Item ID: " . $attempt["item"]['item_id'], 1);
              }
            }
            else
            {
              header("Location: {$script_name}?action=" . $_GET['action']);
            }
          }
          else
          {
            $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
            $redirect = INDEX_PAGE_SHORT;
          }
        }
      }
      else if ($page == "addarmor")
      {
        $add_armor_set_submit = isset($_POST['add_armor_set_submit']) ? 1 : 0;
        $messages = array ();
        $display_form = 1;
        $this_mode_title = "Add Armor Set";

        $armor_categories = array ();
        $armor_level = isset($_POST['armor_level']) ? $_POST['armor_level'] : -1;
        $armor_categories['acc_war_cat'] = isset($_POST['acc_war_cat']) ? intval($_POST['acc_war_cat']) : -1;
        $armor_categories['acc_ran_cat'] = isset($_POST['acc_ran_cat']) ? intval($_POST['acc_ran_cat']) : -1;
        $armor_categories['acc_lau_cat'] = isset($_POST['acc_lau_cat']) ? intval($_POST['acc_lau_cat']) : -1;
        $armor_categories['bell_war_cat'] = isset($_POST['bell_war_cat']) ? intval($_POST['bell_war_cat']) : -1;
        $armor_categories['bell_ran_cat'] = isset($_POST['bell_ran_cat']) ? intval($_POST['bell_ran_cat']) : -1;
        $armor_categories['bell_for_cat'] = isset($_POST['bell_for_cat']) ? intval($_POST['bell_for_cat']) : -1;
        $armor_categories['cora_war_cat'] = isset($_POST['cora_war_cat']) ? intval($_POST['cora_war_cat']) : -1;
        $armor_categories['cora_ran_cat'] = isset($_POST['cora_ran_cat']) ? intval($_POST['cora_ran_cat']) : -1;
        $armor_categories['cora_for_cat'] = isset($_POST['cora_for_cat']) ? intval($_POST['cora_for_cat']) : -1;
        $armor_price = isset($_POST['armor_price']) && ctype_digit($_POST['armor_price']) ? intval($_POST['armor_price']) : -1;
        $armor_slots = isset($_POST['item_slots']) && ctype_digit($_POST['item_slots']) ? intval($_POST['item_slots']) : -1;
        $armor_upgrades = isset($_POST['talics']) ? $_POST['talics'] : array ();
        $armor_add_mercy = isset($_POST['mercy_boots']) ? intval($_POST['mercy_boots']) : 0;
        $armor_add_grace = isset($_POST['grace_gloves']) ? intval($_POST['grace_gloves']) : 0;
        $armor_add_darkness = isset($_POST['darkness_gloves']) ? intval($_POST['darkness_gloves']) : 0;
        $armor_add_wisdom = isset($_POST['wisdom_helmet']) ? intval($_POST['wisdom_helmet']) : 0;
        $armor_type = isset($_POST['type']) ? antiject($_POST['type']) : "a";

        if ($add_armor_set_submit == 1)
        {
          if ($armor_price < 1)
          {
            $messages[] = "Enter a valid price";
          }
          else if ($armor_type != "a" && $armor_type != "b")
          {
            $messages[] = "Armor type was not valid";
          }
          else if (count($armor_upgrades) > 0)
          {
            $empty = array ();
            foreach ($armor_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if ($temp != 5 && $temp != 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if ($temp == 5 && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }
          }
          if (count($messages) == 0)
          {
            if ($armor_slots > 7)
            {
              $armor_slots = 7;
            }
            if ($armor_slots < count($armor_upgrades))
            {
              $item_slots = count($armor_upgrades);
            }
            if (count($armor_upgrades) < 7)
            {
              $i = count($armor_upgrades);
              while ($i < 7)
              {
                $armor_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $armor_slots . "";
            foreach (array_reverse($armor_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);
          }

          if (count($messages) > 0)
          {
            $out .= "<table border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td>";
            $out .= get_notification_html($messages, ERROR);
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "</table>";
          }
          else
          {
            $display_form = 0;
            add_market_armor_set($armor_level, $armor_categories, $armor_price, $item_upgrade, $armor_add_mercy, $armor_add_grace, $armor_add_darkness, $armor_add_wisdom, $armor_type, true);
            $out .= get_notification_html("Added Armor Set Level: " . $armor_level, SUCCESS);
            gamecp_log(3, $userdata -> username, "ADMIN - MANAGE ITEMS GUILD - ADDED ARMOR SET - Level:  " . $armor_level, 1);
          }
        }

        if ($display_form == 1)
        {
          generate_menu($categories, 0, "", array ());
          $subcategory_list = $options;
          $total_categories = count($categories);
          if (0 < $total_categories)
          {
            $out .= "<form class=\"ink-form\" method=\"post\">";
            $out .= "<table class=\"ink-table\" border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align:center;\"><span style=\"font-weight: bold; font-size: 14pt;\">" . $this_mode_title . "</span><br/>This will use the database item names</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Armor Level</span><br/><i>What level armor set do you want?</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select onchange=\"on_select_armor_level();\" name=\"armor_level\">";
            $out .= get_armor_set_options_html();
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Accretian Warrior Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"acc_war_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Accretian Ranger Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"acc_ran_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Accretian Launcher Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"acc_lau_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Bellato Warrior Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"bell_war_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Bellato Ranger Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"bell_ran_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Bellato Force Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"bell_for_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Cora Warrior Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"cora_war_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Cora Ranger Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"cora_ran_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Cora Force Category</span><br/><i>Category where you want this set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"cora_for_cat\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";

            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Price</span><br/><i>The price of game points for this item</i></td>";
            $out .= "<td class=\"\"><input name=\"armor_price\" type=\"text\" value=\"\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Upgrades</span><br/><i>Select slots, then upgrade.  Right click to remove talic.</i></td>";
            $out .= "<td class=\"\">";
            $out .= generate_upgrade_input_html(array (), array (5));
            $out .= "</td>";
            $out .= "</tr>";

            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Add Mercy Boots</span><br/><i>Add mercy boots?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Yes <input name=\"mercy_boots\" type=\"radio\" value=\"1\" checked/> No <input name=\"mercy_boots\" type=\"radio\" value=\"0\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Add Grace Gloves</span><br/><i>Add grace gloves?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Yes <input name=\"grace_gloves\" type=\"radio\" value=\"1\" checked/> No <input name=\"grace_gloves\" type=\"radio\" value=\"0\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Add Darkness Gloves</span><br/><i>Add darkness gloves?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Yes <input name=\"darkness_gloves\" type=\"radio\" value=\"1\" checked/> No <input name=\"darkness_gloves\" type=\"radio\" value=\"0\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Add Wisdom Helmet</span><br/><i>Add wisdom helmet?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Yes <input name=\"wisdom_helmet\" type=\"radio\" value=\"1\" checked/> No <input name=\"wisdom_helmet\" type=\"radio\" value=\"0\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Type</span><br/><i>Normal or Intense?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Normal <input name=\"type\" type=\"radio\" value=\"a\" checked/> Intense <input name=\"type\" type=\"radio\" value=\"b\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align: center;\">";
            $out .= "<input name=\"page\" type=\"hidden\" value=\"addarmor\"/>";
            $out .= "<input class=\"ink-button\" name=\"add_armor_set_submit\" type=\"submit\" value=\"Add Armor Set\"/></td>";
            $out .= "</tr>";
            $out .= "</table>";
            $out .= "</form>";
          }
          else
          {
            $out .= "No categories found";
          }
        }
      }
      else if ($page == "addweapon")
      {
        $add_weapon_set_submit = isset($_POST['add_weapon_set_submit']) ? 1 : 0;
        $messages = array ();
        $display_form = 1;
        $this_mode_title = "Add Weapon Set";

        $weapon_level = isset($_POST['weapon_level']) ? intval($_POST['weapon_level']) : -1;
        $weapon_category = isset($_POST['category']) ? intval($_POST['category']) : -1;
        $weapon_price = isset($_POST['weapon_price']) && ctype_digit($_POST['weapon_price']) ? intval($_POST['weapon_price']) : -1;
        $weapon_slots = isset($_POST['item_slots']) && ctype_digit($_POST['item_slots']) ? intval($_POST['item_slots']) : -1;
        $weapon_upgrades = isset($_POST['talics']) ? $_POST['talics'] : array ();
        $weapon_type = isset($_POST['type']) ? antiject($_POST['type']) : "a";

        if ($add_weapon_set_submit == 1)
        {
          if ($weapon_price < 1)
          {
            $messages[] = "Enter a valid price";
          }
          else if ($weapon_type != "a" && $weapon_type != "b")
          {
            $messages[] = "Weapon type was not valid";
          }
          else if (count($weapon_upgrades) > 0)
          {
            $valid_talics = array (0, 1, 3, 4, 7, 8, 9, 10);
            $empty = array ();
            foreach ($weapon_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if (!in_array($temp, $valid_talics) && $temp != 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if (in_array($temp, $valid_talics) && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }
          }
          if (count($messages) == 0)
          {
            if ($weapon_slots > 7)
            {
              $weapon_slots = 7;
            }
            if ($weapon_slots < count($weapon_upgrades))
            {
              $weapon_slots = count($weapon_upgrades);
            }
            if (count($weapon_upgrades) < 7)
            {
              $i = count($weapon_upgrades);
              while ($i < 7)
              {
                $weapon_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $weapon_slots . "";
            foreach (array_reverse($weapon_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);
          }

          if (count($messages) > 0)
          {
            $out .= get_notification_html($messages, ERROR);
          }
          else
          {
            $display_form = 0;
            add_market_weapon_set($weapon_level, $weapon_category, $weapon_price, $item_upgrade, $weapon_type, true);
            $out .= get_notification_html("Added Weapon Set Level: " . $weapon_level, SUCCESS);
            $redirect = CUSTOM_PAGE_SHORT;
            $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
            gamecp_log(3, $userdata -> username, "ADMIN - MANAGE ITEMS GUILD - ADDED WEAPON SET - Level:  " . $weapon_level, 1);
          }
        }

        if ($display_form == 1)
        {
          generate_menu($categories, 0, "", array (), $cat_id);
          $subcategory_list = $options;
          $total_categories = count($categories);
          if (0 < $total_categories)
          {
            $out .= "<form class=\"ink-form\" method=\"post\">";
            $out .= "<table class=\"ink-table\" border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align:center;\"><span style=\"font-weight: bold; font-size: 14pt;\">" . $this_mode_title . "</span><br/>This will use the database item names</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Weapon Level</span><br/><i>What level weapon set do you want?</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"weapon_level\">";
            $out .= get_weapon_set_options_html();
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Category</span><br/><i>Category where you want the weapon set</i></td>";
            $out .= "<td class=\"\">";
            $out .= "<select name=\"category\">";
            $out .= $subcategory_list;
            $out .= "</select>";
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Price</span><br/><i>The price of game points for the weapons</i></td>";
            $out .= "<td class=\"\"><input name=\"weapon_price\" type=\"text\" value=\"\" style=\"width: 230px;\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Upgrades</span><br/><i>Select slots, then upgrade.  Right click to remove talic.</i></td>";
            $out .= "<td class=\"\">";
            $out .= generate_upgrade_input_html(array (), array (0, 1, 3, 4, 7, 8,
              9, 10));
            $out .= "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\"><span style=\"font-weight: bold;\">Type</span><br/><i>Normal or Intense?</i></td>";
            $out .= "<td class=\"\" width=\"350px\">Normal <input name=\"type\" type=\"radio\" value=\"a\" checked/> Intense <input name=\"type\" type=\"radio\" value=\"b\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"\" colspan=\"2\" style=\"text-align: center;\">";
            $out .= "<input name=\"page\" type=\"hidden\" value=\"addweapon\"/>";
            $out .= "<input class=\"ink-button\" name=\"add_weapon_set_submit\" type=\"submit\" value=\"Add Weapon Set\"/></td>";
            $out .= "</tr>";
            $out .= "</table>";
            $out .= "</form>";
          }
          else
          {
            $out .= "No categories found";
          }
        }
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
