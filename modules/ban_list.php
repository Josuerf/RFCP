<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Server";
  $moduleLabel = "Wall of Shame!";
  $permission = "all";
  return;
}

if ($this_script == $script_name)
{
  $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
  $page_gen = isset($_GET['page_gen']) ? $_GET['page_gen'] : "1";
  $search_fun = isset($_GET['search_fun']) ? $_GET['search_fun'] : "";
  $account_name = isset($_GET['account_name']) ? $_GET['account_name'] : "";
  $account_name = preg_replace(preg_quote("/(from|select|insert|delete|where|drop table|show tables|#|\\*|--|\\\\)/"), "", $account_name);
  $search_query = "";
  $enable_exit = false;
  $enable_search = false;

  $out .= "<form class=\"ink-form\" method=\"GET\">";
  $out .= "<p style=\"text-align: left; font-weight: bold; margin: 0; padding: 0 2px 4px 0;\">Look up a banned account: <input type=\"text\" name=\"account_name\" value=\"" . $account_name . "\"/> <input class=\"ink-button\" type=\"submit\" name=\"search_fun\" value=\"Search\" /></p>";
  $out .= "<input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\"/>";
  $out .= "</form>";

  $messages = array ();
  $and = "";
  if ($search_fun != "")
  {
    if (!preg_match(REGEX_USERNAME, $account_name))
    {
      $messages[] = "Enter a valid account name";
    }
    if (count($messages) == 0)
    {
      $attempt = get_account_serial($account_name);
      if ($attempt["error"] == True)
      {
        $messages[] = $attempt["errorMessage"];
      }
      else
      {
        $account_serial = $attempt['serial'];
        $search_query .= " B.nAccountSerial = '{$account_serial}' ";
        $and = " AND ";
        $enable_search = true;
      }
      if (count($messages) > 0)
      {
        $search_query = "";
      }
    }
  }

  $out .= "<table class=\"ink-table\" cellpadding=\"5\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
  $out .= "<tr style=\"font-size: 16pt;\">";
  $out .= "<td style=\" text-align: center;\" nowrap><b>#</b></td>";
  $out .= "<td style=\"\" nowrap><b>Start Date</b></td>";
  $out .= "<td style=\"\" nowrap><b>End Date</b></td>";
  $out .= "<td style=\"\" nowrap><b>Account Name</b></td>";
  $out .= "<td style=\"\" nowrap><b>Banned By</b></td>";
  $out .= "<td style=\"\" nowrap><b>Reason for ban</b></td>";
  $out .= "</tr>";

  include( "./core/pagination.php" );

  $sql = "SELECT CONVERT(varchar, U.id) AS username, B.nAccountSerial, B.dtStartDate AS startdate, B.nPeriod, B.nKind, B.szReason, B.GMWriter,";
  $sql .= " U.Serial FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial WHERE " . $search_query . $and;
  $sql .= " U.Serial NOT IN ( SELECT TOP [OFFSET] U.Serial FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial ";
  if ($search_query != "")
  {
    $search_query = "WHERE " . $search_query;
  }
  $sql .= $search_query . " ORDER BY B.dtStartDate DESC) ORDER BY B.dtStartDate DESC";
  $sql_count = "SELECT COUNT(U.id) FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON U.serial = B.nAccountSerial " . $search_query;

  $url = "" . $script_name . "?action=" . $_GET['action'];

  $pager = new Pagination(USER, $sql, $sql_count, $url, array (), array (), $page_size = 25, $links_to_show = 11);
  $results = $pager -> get_data();

  foreach ($results["rows"] as $key => $row)
  {
    $username = filter_string_for_html($row['username']);
    $startdate = $row['startdate'] -> getTimestamp();
    $enddate = $row['nPeriod'] * 3600;
    $enddate = $startdate + $enddate;
    $enddate = date("d/m/Y h:i A", $enddate);
    $startdate = date("d/m/Y h:i A", $startdate);
    if ($row['GMWriter'] == "WS0")
    {
      $row['szReason'] = "Auto-banned by FireGuard";
    }

    $gm_banned = filter_string_for_html($row['GMWriter']);
    if ($gm_banned == "WS0")
    {
      $gm_banned = "-";
    }

    //if (!preg_match("/TEMP/", $row['szReason']))
    //{

    $out .= "<td style=\"font-size: 14pt; text-align: center; font-weight: bold; border-bottom: 0px;\" width=\"1%\">" . ($key + 1) . "</td>";
    $out .= "<td style=\"font-size: 14pt; font-weight: bold; border-bottom: 0px;\" width=\"10%\" nowrap>" . $startdate . "</td>";
    $out .= "<td style=\"font-size: 14pt; font-weight: bold; border-bottom: 0px;\" width=\"10%\" nowrap>" . $enddate . "</td>";
    $out .= "<td style=\"font-size: 14pt; font-weight: bold; border-bottom: 0px;\" width=\"30%\" nowrap>" . $username . "</td>";
    $out .= "<td style=\"font-size: 14pt; font-weight: bold; border-bottom: 0px;\" width=\"30%\" nowrap>" . $gm_banned . "</td>";
    $out .= "<td style=\"font-size: 14pt; font-weight: bold; border-bottom: 0px;\" width=\"30%\" nowrap>" . $row['szReason'] . "</td>";

    $attempt = get_characters_cached($row["nAccountSerial"]);
    if ($attempt["error"] == True)
    {
      $out .= "<tr>";
      $out .= "<td style=\"font-size: 12pt; text-align: center; font-weight: bold;\">&raquo;</td>";
      $out .= "<td style=\"font-size: 12pt;\" colspan=\"5\" nowrap>" . $attempt["errorMessage"] . "</td>";
      $out .= "</tr>";
    }
    else
    {
      foreach ($attempt["rows"] as $key => $char)
      {
        $border = " border-bottom: 0px;";
        if ($key == count($attempt["rows"]) - 1)
        {
          $border = "";
        }
        $out .= "<tr>";
        $out .= "<td style=\"font-size: 12pt; text-align: center; font-weight: bold;" . $border . "\">&raquo;</td>";
        $out .= "<td style=\"font-size: 12pt;" . $border . "\" colspan=\"5\" nowrap>" . $char['Name'] . "</td>";
        $out .= "</tr>";
      }
    }
  }
  if (count($results["rows"]) <= 0)
  {
    $out .= "<tr>";
    $out .= "<td colspan=\"6\" style=\"text-align: center;\">No banned user(s) found</td>";
    $out .= "</tr>";
  }
  else
  {
    $out .= "<tr>";
    $out .= "<td colspan=\"6\" style=\"text-align: center;\">" . $pager -> renderFullNav() . "</td>";
    $out .= "</tr>";
  }
  $out .= "</table>";
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

