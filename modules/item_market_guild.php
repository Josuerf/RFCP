<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Guild Market";
  $moduleLabel = "Item Market";
  $permission = "player";
  return;
}

$out .= "<div class=\"item_market_wrapper\">";
if ($this_script == $script_name)
{
  if ($userdata -> loggedin == True)
  {
    $page = isset($_REQUEST['page']) ? antiject($_REQUEST['page']) : "";
    $cat_id = isset($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
    $out .= "<script> if ('".(isset($_SESSION['lastPage']) ? $_SESSION['lastPage'] : '') ."' !== 'item_market_guild') {simpleCart.empty();}</script>";
    $_SESSION['lastPage'] = $_REQUEST['action'];

    $today = time();
    $exit_cat = 0;
    $race = "";
    $query_p2 = "";
    $j = 0;
    $k = 0;
    $sub_name = "";
    $is_char = false;

    $categories_attempt = get_market_categories(-1, -1, true);
    //$menu_array
    $categories = $categories_attempt["categories"];

    if (isset($_POST['order'], $_POST['sort']))
    {
      $order = antiject($_POST['order']);
      $sort = antiject($_POST['sort']);
      $sortorder_data = $order . chr(255) . $sort;
      setcookie("sortorder", $sortorder_data, time() + 31104000);
    }
    else
    {
      if (isset($_COOKIE['sortorder']))
      {
        $sortorder = explode(chr(255), $_COOKIE['sortorder']);
        $order = $sortorder[0];
        $sort = $sortorder[1];
      }
      else
      {
        $order = isset($config['shop_order_by']) ? $config['shop_order_by'] : "1";
        $sort = isset($config['shop_sort']) ? $config['shop_sort'] : "1";
      }
    }

    $order_raw = $order;
    $sort_raw = $sort;

    if ($order == 1)
    {
      $order = "item_name";
    }
    else if ($order == 2)
    {
      $order = "item_price";
    }
    else if ($order == 3)
    {
      $order = "item_race";
    }
    else if ($order == 4)
    {
      $order = "item_buy_count";
    }
    else if ($order == 5)
    {
      $order = "item_date_added";
    }
    else if ($order == 6)
    {
      $order = "item_date_updated";
    }
    else if ($order == 7)
    {
      $order = "item_dbcode";
    }
    else
    {
      $order = "item_name";
    }

    if ($sort == 1)
    {
      $sort = "ASC";
    }
    else
    {
      $sort = "DESC";
    }

    $config['shop_order_by'] = isset($config['shop_order_by']) ? $config['shop_order_by'] : "item_name";
    $order = $order == "" ? $config['shop_order_by'] : $order;
    $config['shop_sort'] = isset($config['shop_sort']) ? $config['shop_sort'] : "ASC";
    $short = $sort == "" ? $config['shop_sort'] : $sort;
    if (empty($page))
    {
      $out .= "<div class=\"market_current_points_text\" style=\"width:100%; font-size: 14pt; text-align: center;\">You currently have <span style=\"color: #4259FF; font-weight:bold;\">" . number_format($userdata -> gcredits, 2) . "</span> Guild Credits (<b>GC</b>)</div>";
      $out .= "<script>available_novus_credits = " . $userdata -> gcredits . ";</script>";

      $categories_attempt = get_market_categories($sub_cat_id = $cat_id, -1, true);
      $current_categories = $categories_attempt["categories_in_order"];

      if ($categories_attempt["error"] === False)
      {
        $total_categories = count($current_categories);
        if ($cat_id > 0)
        {
          $by_category = " AND item_cat_id = '" . $cat_id . "'";
        }
        else
        {
          $by_category = "";
        }

        include( "./core/pagination.php" );

        $sql = "SELECT item_id,item_name,item_dbcode,item_amount,item_upgrade,item_description,item_image_url,";
        $sql .= "item_price,item_buy_count,item_date_added,item_date_updated,item_race,item_rent_time ";
        $sql .= "FROM gamecp_g_market_items WHERE item_delete = 0 AND item_status = 1 {$by_category} ";
        $sql .= "AND item_id NOT IN ( SELECT TOP [OFFSET] item_id FROM gamecp_g_market_items ";
        $sql .= "WHERE item_delete = 0 AND item_status = 1 {$by_category} ORDER BY {$order} {$sort}) ORDER BY {$order} {$sort}";
        $sql_count = "Select COUNT(item_id) FROM gamecp_g_market_items WHERE item_delete = 0 {$by_category}";

        $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
        $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

        $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 40, $links_to_show = 11);
        $results = $pager -> get_data();

        $current_cat_id = $cat_id;
        $nav = array ();
        $nav[] = "<a href=\"" . $script_name . "?action=" . $_GET['action'] . "\" style=\"padding-left: 18px !important;\">Guild Market Categories</a>";
        $temp = array ();
        while ($current_cat_id > 0)
        {
          $temp[] = $categories[$current_cat_id]["link"];
          $current_cat_id = $categories[$current_cat_id]["parent"];
        }
        $i = count($temp) - 1;
        while ($i >= 0)
        {
          $nav[] = $temp[$i];
          $i = $i - 1;
        }

        $out .= "<div>";
        $out .= get_breadcrumb_html($nav);
        $out .= generate_category_html($current_categories, true);
        $out .= "</div>";

        $order_array = array ("", "Name", "Price", "Race", "Popularity", "Date Added",
          "Date Updated", "Item Group");
        $sort_array = array ("", "Ascending", "Descending");

        $out .= "<div style=\"clear:both; width: 100%; text-align:center; padding-top: 1px; padding-bottom: 10px;\">";
        $out .= "<hr>";
        $out .= "<form class=\"ink-form\" method=\"post\">";
        $out .= "Sort By: <select name=\"order\">";
        $f = 1;
        while ($f < count($order_array))
        {
          if ($order_raw == $f)
          {
            $selected = " selected=\"selected\"";
          }
          else
          {
            $selected = "";
          }
          $out .= "<option value=\"" . $f . "\"" . $selected . ">" . $order_array[$f] . "</option>";
          ++$f;
        }
        $out .= "</select> ";
        $out .= "<select name=\"sort\">";
        $s = 1;
        while ($s < count($sort_array))
        {
          if ($sort_raw == $s)
          {
            $selected = " selected=\"selected\"";
          }
          else
          {
            $selected = "";
          }
          $out .= "<option value=\"" . $s . "\"" . $selected . ">" . $sort_array[$s] . "</option>";
          ++$s;
        }
        $out .= "  </select> ";
        $out .= "<input class=\"ink-button\" type=\"submit\" name=\"Change\" value=\"Change\"/>";
        $out .= "</form>";
        $out .= "</div>";

        foreach ($results["rows"] as $key => $item)
        {
          $k_value = $item['item_dbcode'];
          $u_value = $item['item_upgrade'];
          $item_details = get_item_details($k_value, $u_value);
          $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
          $item_info = $item_info["item"];
          $upgrades = $item_details["images"];

          $item_price = $item['item_price'];
          $item_final_price = $item_price;

          $race = "All Races";
          $raceImage = "<div class=\"market_race_image\"><img src=\"./images/all.png\" title=\"All Races\"/></div>";

          if ($item['item_race'] == 1)
          {
            $race = "Bellato";
            $raceImage = "<div class=\"market_race_image\"><img src=\"./images/bell.png\" title=\"Bellato\"/></div>";
          }
          else if ($item['item_race'] == 2)
          {
            $race = "Cora";
            $raceImage = "<div class=\"market_race_image\"><img src=\"./images/cora.png\" title=\"Cora\"/></div>";
          }
          else if ($item['item_race'] == 3)
          {
            $race = "Accretian";
            $raceImage = "<div class=\"market_race_image\"><img src=\"./images/acc.png\" title=\"Accretian\"/></div>";
          }
          else if ($item['item_race'] == 4)
          {
            $race = "Bellato & Cora";
            $raceImage = "<div class=\"market_race_image\"><img src=\"./images/cora_bell.png\" title=\"Bellato & Cora\"/></div>";
          }

          $quantity = "";
          if (0 < $item['item_amount'])
          {
            $quantity = "Quantity: " . $item['item_amount'] . "<br>";
          }

          if ($userdata -> gcredits < $item_final_price)
          {
            $disable_button = " DISABLED";
          }
          else
          {
            $disable_button = "";
          }
          if ($item['item_image_url'] != " " && $item['item_image_url'] != "")
          {
            $item_image = $item['item_image_url'];
          }
          else
          {
            $item_id = $item_details["id"];
            $kn = $item_details["type"];
            $item_image = "./images/items/unknown.gif";
            if (isset($item_info["item_icon_id"]))
            {
              $image_path = glob("./images/items/" . get_image_folder_name($kn) . "/(" . $item_info["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
              if (file_exists($image_path[0]))
              {
                $item_image = $image_path[0];
              }
            }
          }

          if ($today < $item['item_date_added'] + 172800)
          {
            $new = " <span style=\"color: red;\">*NEW*</span>";
          }
          else
          {
            $new = "";
          }

          $credits_class = "";
          if ($userdata -> gcredits < $item_final_price)
          {
            $credits_class = "not_enough";
          }

          $item_header_text = $item["item_name"];
          if (strpos($item_header_text, "(") != False)
          {
            $item_header_text = substr($item["item_name"], 0, strpos($item_header_text, "("));
          }
          if (strpos($item_header_text, "[") != False && strpos($item_header_text, "[W]") == False && strpos($item_header_text, "[R]") == False && strpos($item_header_text, "[F]") == False && strpos($item_header_text, "[L]") == False)
          {
            $item_header_text = substr($item["item_name"], 0, strpos($item_header_text, "["));
          }


          $out .= "<div class=\"market_item_block simpleCart_shelfItem tooltip\" onclick=\"simpleCart.add({'quantity':1, ";
          $out .= "'name': '" . $item["item_name"] . "', 'price': " . number_format($item_final_price, 2, ".", "");
          $out .= ", 'image': '" . $item_image . "', 'rfcp_id': " . $item['item_id'] . ", 'item_race': " . $item['item_race'] . "});\" ";
          $out .= "data-tip-html=\"" . filter_string_for_html(filter_string_for_html($item["item_name"])) . "<br>" . $race . "<br>" . $quantity . "Purchased: " . number_format($item['item_buy_count']) . "<br>" . $item['item_description'] . "\" data-tip-color: \"blue\">";
          $out .= "<div class=\"market_item_title item_name\">" . filter_string_for_html($item_header_text) . $new . "</div>";
          $out .= "<hr style=\"margin:0px;\"><div class=\"market_image_wrapper\">";
          $out .= "<div class=\"market_image_background\">";
          $out .= "<img id=\"market_item_image_" . $item['item_id'] . "\" class=\"market_item_image\" src=\"" . $item_image . "\">";
          $out .= "</div>";
          $out .= "</div>";
          if ($upgrades != "")
          {
            $out .= "<div>" . $upgrades . "</div>";
          }
          else if (0 < $item['item_amount'])
          {
            $out .= "<div class=\"market_item_quantity\">Quantity: " . $item['item_amount'] . "</div>";
          }
          $out .= "<div class=\"market_item_credits_amount item_price {$credits_class}\">" . number_format($item_final_price, 2, ".", "") . " <span style=\"font-size:17pt;\">GC</span></div>";
          $out .= $raceImage;
          if ($item['item_rent_time'] > 0)
          {
            $out .= "<div class=\"market_item_rent_time\">Rented<br>" . $item['item_rent_time'] . " Hrs</div>";
          }
          $out .= "<div class=\"item_Quantity\" style=\"display:none;\">1</div>";
          $out .= "</div>";
        }

        $out .= get_cart_info_html();

        $out .= "<script type=\"text/javascript\">";
        $out .= "activate_tooltips();";
        $out .= "initialize_cart(\"" . $script_name . "?action=" . $_GET['action'] . "&page=buy\", true);";
        $out .= "</script>";

        if (count($results["rows"]) <= 0)
        {
          $out .= get_notification_html("No items were found in this category", ERROR);
        }
        else
        {
          $out .= "<div style=\"padding-top: 6px; clear:both;\">" . $pager -> renderFullNav() . "</div>";
        }
      }
    }
    else
    {
      if ($page == "buy")
      {
        $cart_item_count = isset($_POST['itemCount']) && ctype_digit($_POST['itemCount']) ? intval($_POST['itemCount']) : -1;
        $cart_char_serial = isset($_POST['char_serial']) && ctype_digit($_POST['char_serial']) ? intval($_POST['char_serial']) : -1;
        $items_to_buy = array ();
        $credits_needed = 0;
        $char_race = "";

        $errors = array ();

        if ($cart_item_count < 1)
        {
          $errors[] = "No cart items submitted";
        }
        if ($cart_char_serial < 1)
        {
          $errors[] = "Invalid character serial provided";
        }

        if (count($errors) == 0)
        {
          $race = -1;
          $char = $userdata -> get_character($cart_char_serial);
          if (!isset($char))
          {
            $errors[] = "Character not found on the account";
          }
          else
          {
            $char_race = $char["Race"];
          }

          $i = 1;
          while ($i <= $cart_item_count && count($errors) == 0)
          {
            $item_id = intval($_POST['item_rfcp_id_' . $i]);
            $item_quantity = intval($_POST['item_quantity_' . $i]);
            $item_info_attempt = get_market_item($item_id, true);
            if ($item_info_attempt["error"])
            {
              $errors[] = $item_info_attempt["errorMessage"];
            }
            else
            {
              $item_info = $item_info_attempt["item"];
              $item_info["quantity"] = $item_quantity;

              if ($item_info["item_status"] != 1)
              {
                $errors[] = "Item is not enabled";
              }

              if (!($item_info["item_race"] == 0 ||
              ($item_info["item_race"] == 1 && $char_race == "B") ||
              ($item_info["item_race"] == 2 && $char_race == "C") ||
              ($item_info["item_race"] == 3 && $char_race == "A") ||
              ($item_info["item_race"] == 4 && ($char_race == "B" || $char_race == "C"))))
              {
                echo $item_info["item_race"] . "_" . $char_race;
                $errors[] = "Item is not the same race as the character";
              }

              $items_to_buy[] = $item_info;
              $credits_needed = $credits_needed + ($item_info["item_price"] * $item_quantity);
            }

            $i++;
          }

          if ($credits_needed > $userdata -> gcredits)
          {
            $errors[] = "Not enough credits!";
          }
        }

        if (count($errors) == 0)
        {
          //remove credits
          $current_credits = $userdata -> gcredits;
          $attempt = remove_user_g_credits($userdata -> serial, $credits_needed);
          if ($attempt["error"])
          {
            $errors[] = $attempt["errorMessage"];
          }
          else
          {
            foreach ($items_to_buy as $key => $item)
            {
              $k_value = $item["item_dbcode"];
              $u_value = $item["item_upgrade"];
              $amount = $item["item_amount"];
              $quantity = $item["quantity"];
              $rent_time = $item["item_rent_time"] * 3600;

              for ($y = 1; $y <= $quantity; $y++)
              {
                $attempt = give_character_item($cart_char_serial, $k_value, $amount, $u_value, $rent_time);
                if ($attempt["error"] == False)
                {
                  $current_credits = $current_credits - $item["item_price"];
                  item_redeem_g_log($userdata -> serial, $cart_char_serial, $item["item_price"], $item["item_id"], $item["item_name"], $item["item_amount"], $item["item_dbcode"], $current_credits);
                  increment_market_item_purchased($item["item_id"], true);
                }
                else
                {
                  $errors[] = $attempt["errorMessage"];
                  gamecp_log(1, $userdata -> username, "GAMECP - ITEM SHOP - Failed to give item: " . $item["item_id"], 1);
                }
              }
            }
          }
        }

        if (count($errors) == 0)
        {
          $out .= get_notification_html("Item(s) successfully purchased", SUCCESS);
          $out .= "<script type=\"text/javascript\">";
          $out .= "initialize_cart(\"" . $script_name . "?action=" . $_GET['action'] . "&page=buy\", true);";
          $out .= "simpleCart.empty();";
          $out .= "</script>";
          $redirect = PREVIOUS_PAGE_SHORT;
        }
        else
        {
          $out .= get_notification_html(implode("<br>", $errors), ERROR);
        }
      }
      else
      {
        $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
        $redirect = INDEX_PAGE_SHORT;
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

$out .= "</div>";
