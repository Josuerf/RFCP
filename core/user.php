<?php

class RFUser
{

  protected $_properties = array ();

  public function __construct()
  {
    $this -> loggedin = False;
  }

  public function __get($property)
  {
    if (isset($this -> _properties[$property]))
    {
      return $this -> _properties[$property];
    }
    else
    {
      return null;
    }
  }

  public function get_property($property_name, $default_value = null)
  {
    if (isset($this -> _properties[$property_name]))
    {
      return $this -> _properties[$property_name];
    }
    else
    {
      return $default_value;
    }
  }

  public function __set($property_name, $value)
  {
    $this -> _properties[$property_name] = $value;
  }

  public function load($serial)
  {
    $load_return = array (
      "error" => False,
      "errorMessage" => ""
    );

    $user_data = get_user_data($serial);

    if ($user_data["error"] === False)
    {
      $data = $user_data["data"];
      $this -> username = trim($data["AccountName"]);
      $this -> serial = $data["Serial"];
      $this -> email = trim($data["EMail"]);
      $this -> createtime = $data["CreateTime"];
      $this -> lastconnectip = $data["LastConnectIP"];
      $this -> lastlogintime = $data["lastlogintime"];
      $this -> lastlogofftime = $data["lastlogofftime"];
      $this -> facebookUserId = $data["FacebookUserId"];

      $t_login = $data['lastlogintime'];
      $t_logout = $data['lastlogofftime'];
      if ($t_login <= $t_logout)
      {
        $this -> status = False;
      }
      else
      {
        $this -> status = True;
      }

      $this -> loggedin = True;

      $this -> permissions = get_user_permissions($this -> serial);
      $this -> display_name = isset($this -> permissions["displayName"]) ? $this -> permissions["displayName"] : $this -> username;
      $this -> permissions = array_merge($this -> permissions["access"], $this -> permissions["customPermissions"]);

      $credits_result = get_user_credits($serial);
      if ($credits_result["error"] === False)
      {
        $this -> credits = $credits_result["credits"];
        $this -> bonus_credits = $credits_result["bonus_credits"];
        $this -> gcredits = $credits_result["guild_credits"] ? : 0;
      }
      else
      {
        $load_return["error"] = True;
        $load_return["errorMessage"] = $credits_result["errorMessage"];
      }
      $premium_result = getUserPremiumStatus($this -> username);
      if ($premium_result["error"] === False)
      {
        $this -> premiumData = $premium_result["data"];
        $this -> premium = $this -> premiumData['BillingType'] == 2;
      }
      else
      {
        $load_return["error"] = True;
        $load_return["errorMessage"] = $credits_result["errorMessage"];
      }
    }
    else
    {
      $load_return["error"] = True;
      $load_return["errorMessage"] = $user_data["errorMessage"];
    }
  }

  public function is_superadmin()
  {
    global $admin;
    $super_admin = explode(",", $admin["super_admin"]);

    if (in_array($this -> username, $super_admin))
    {
      return True;
    }

    return False;
  }

  public function has_permission($page)
  {
    $page = str_replace(".php", "", $page);
    if ($this -> permissions == null)
    {
      $this -> permissions = array ();
    }

    if (in_array($page, $this -> permissions))
    {
      return True;
    }
    if ($this -> is_superadmin())
    {
      return True;
    }

    return False;
  }

  public function get_character_list_short()
  {
    $attempt = get_characters($this -> serial);
    return $attempt["rows"];
  }

  public function get_character_list_full()
  {
    $attempt = get_characters($this -> serial, True);
    return $attempt["rows"];
  }

  public function get_character($serial)
  {
    $return;
    $attempt = get_characters($this -> serial);

    foreach ($attempt["rows"] as $key => $char)
    {
      if ($char["Serial"] == $serial)
      {
        $return = $char;
      }
    }

    return $return;
  }

  public function get_password()
  {
    $username = $this -> username;
    return get_user_password($username);
  }

  public function set_new_password($newpassword)
  {
    $username = $this -> username;
    return change_user_password($username, $newpassword);
  }

}
