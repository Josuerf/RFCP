USE [RF_ItemsDB]
GO
/****** Object:  Table [dbo].[tbl_code_accgsp_portal]    Script Date: 06/05/2014 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_code_accgsp_portal](
	[item_id] [int] NOT NULL,
	[item_code] [varchar](50) NULL,
	[item_name] [varchar](255) NULL,
	[item_client_id] [int] NULL,
	[item_icon_id] [int] NULL,
	[item_level] [int] NULL,
	[item_min_attack] [int] NULL,
	[item_max_attack] [int] NULL,
	[item_adr] [float] NULL,
	[item_adp] [float] NULL,
	[item_dsr] [float] NULL,
 CONSTRAINT [PK_tbl_code_accgsp_portal] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_code_accgsp_portal] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr]) VALUES (0, N'dpgoto_ac_grsp', N'Empire Accretia HQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_accgsp_portal] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr]) VALUES (1, N'dpfrom_ac_grsp', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
