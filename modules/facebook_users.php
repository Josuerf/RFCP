<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Facebook users";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th class='align-left'>Username</th>";
    $out .= "<th class='align-left'>Email</th>";
    $out .= "<th class='align-left'>Facebook</th>";
    $out .= "</tr>";

    $sql = "SELECT CAST(id AS varchar(255)) username, Email, FacebookUserId ";
    $sql .= "FROM " . TABLE_LUACCOUNT . " ";
    $sql .= "WHERE FacebookUserId IS NOT NULL ";
    $sql .= "ORDER BY id DESC";
    $sql_count = "SELECT COUNT(*) FROM " . TABLE_LUACCOUNT . " WHERE FacebookUserId IS NOT NULL";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(USER, $sql, $sql_count, $url, array (), array (), $page_size = 50, $links_to_show = 10);
    $results = $pager -> get_data();
    foreach ($results["rows"] as $key => $row)
    {
      $out .= "<tr>";
      $out .= "<td nowrap>" . $row['username'] . "</td>";
      $out .= "<td nowrap>" . $row['Email'] . "</td>";
      $out .= "<td nowrap><a href='https://www.facebook.com/" . $row['FacebookUserId'] . "' target='_blank'>https://www.facebook.com/" . $row['FacebookUserId'] . "</a></td>";
      $out .= "</tr>";
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No users with facebook found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
