<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "HIDDEN";
  $moduleLabel = "show ticket";
  $permission = "player";
  return;
}
if ($this_script == $script_name)
{
  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  if ($userdata -> loggedin == True)
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $add_reply = isset($_POST['add_reply']) ? $_POST['add_reply'] : "";
    $comment = isset($_POST['comment']) ? $_POST['comment'] : "";

    if (empty($page))
    {
      $messages = array ();

      if ($add_reply != "")
      {
        if ($comment != "")
        {
          //need to make sure the user is editing their ticket and not someone else's
          $sql = "SELECT E.id, D.replyid ";
          $sql .= "FROM gamecp_ticket_entry AS E INNER JOIN gamecp_ticket_data AS D ON E.id = D.entryid WHERE E.id = ? AND E.account = ? ";
          $sql .= "ORDER BY D.replyid DESC ";
          $param = array ($_POST['entryid'], $userdata -> username);
          $tdata = sqlsrv_query(connectdb(RFCP), $sql, $param);
          if ($tdata === false)
          {
            $messages[] .= get_notification_html(array ('Entry ID did not match for reply'), ERROR);
          }
          else
          {
            $row = sqlsrv_fetch_array($tdata);
            $postid = $row["replyid"] + 1;
            $entryid = $row["id"];
            $replysql = "INSERT INTO gamecp_ticket_data (isgm, ticket, replyid, entryid, name) VALUES (?,?,?,?,?)";
            $datesql = "UPDATE gamecp_ticket_entry SET date = getdate(), status = 0 WHERE id = ?";
            $dvalues = array ($entryid);
            $values = array (0, $comment, $postid, $entryid, $userdata -> username);

            $dupdate = sqlsrv_query(connectdb(RFCP), $datesql, $dvalues);
            $tupdate = sqlsrv_query(connectdb(RFCP), $replysql, $values);
            if ($tupdate === false)
            {
              $messages[] = get_notification_html(array ('Error updating Ticket Data'), ERROR);
            }
            if ($dupdate === false)
            {
              $messages[] = get_notification_html(array ('Error updating Ticket Date'), ERROR);
            }
          }
        }
        else
        {
          $messages[] = "Your Reply is empty";
        }
      }

      $url = "./" . $script_name . "?action=my_tickets";

      $out .= "<div style = 'margin-left: 50px; padding-top:15;'>";
      $out .= "<a href='$url'>Return to Ticket List</a>";
      $out .= "</div>";

      $sql = "SELECT E.id, E.date, E.type, E.account, E.title, E.status, D.isgm, D.ticket, D.replyid, D.name, D.date AS postdate ";
      $sql .= "FROM gamecp_ticket_entry AS E INNER JOIN gamecp_ticket_data AS D ON E.id = D.entryid WHERE E.id = ? AND E.account = ? ";
      $param = array ($_GET['id'], $userdata -> username);
      $tdata = sqlsrv_query(connectdb(RFCP), $sql, $param, array ("Scrollable" => SQLSRV_CURSOR_KEYSET));
      if ($tdata === false)
      {
        $out .= get_notification_html(array ('Error Retrieving Ticket Data'), ERROR);
      }
      else
      {
        $row_count = sqlsrv_num_rows($tdata);
        sqlsrv_fetch($tdata);

        $out .= "<div style = 'width: 1000px; margin: 25px 50px 25px 50px; height: auto; min-height: 500px; border-top: solid #bbb; border-width: thin;'>";
        $i = 0;
        while ($i < $row_count)
        {
          $id = sqlsrv_get_field($tdata, 0);
          $time = sqlsrv_get_field($tdata, 1);
          $type = sqlsrv_get_field($tdata, 2);
          $account = sqlsrv_get_field($tdata, 3);
          $title = filter_string_for_html(sqlsrv_get_field($tdata, 4));
          $status = sqlsrv_get_field($tdata, 5);
          $isgm = sqlsrv_get_field($tdata, 6);
          $content = sqlsrv_get_field($tdata, 7, SQLSRV_PHPTYPE_STRING(SQLSRV_ENC_CHAR));
          $replyid = sqlsrv_get_field($tdata, 8);
          $entryid = $id;
          $replyname = sqlsrv_get_field($tdata, 9);
          $postdate = sqlsrv_get_field($tdata, 10);

          $gm_color = "";
          $gm_div_style = "";
          if ($isgm == 2)
          {
            $gm_color = "color: #E6161E; ";
            $gm_div_style = " style = 'margin-left:25px;'";
          }
          else if ($isgm == 1)
          {
            $gm_color = "color: #16E616; ";
            $gm_div_style = " style = 'margin-left:25px;'";
          }

          $category = get_ticket_category($type);
          if ($replyid == 0)
          {
            $out .= "<div style = 'float: left; width: 670px; height:auto;'>";
            $out .= "<h1 style = 'height: 58px; width: 670px; margin: 18px 0 18px 0; font-family: Helvetica, Arial, sans-serif; font-size: 26px;'><span style = 'color: #bbb; font-size: 15px; display: block; margin-bottom: 15px;'>Ticket: " . $id . "</span>" . $title . "</h1>";
            $out .= "<ul style = 'border-top: solid #bbb; border-width:thin; display: block; width: 670px;'>";
            $out .= "<li style = 'list-style-type: none; min-height:150px;'>";
            $out .= "<div" . $gm_div_style . ">";
            $out .= "<strong style = '" . $gm_color . "display: inline-block; font-size: 15px; margin: 0 0 15px 0;'>" . $replyname . "</strong>";
            $out .= "<span style = 'float: right; font-size: 15px; margin-right: 12px; margin-left: 12px; color: #bbb;'>" . $postdate -> format('Y-m-d') . " Post: " . $replyid . "</span>";
            $out .= "<div style = 'font-weight: lighter; line-height: 1.7; word-wrap: break-word;'>" . $content . "</div>";
            $out .= "</div>";
            $out .= "</li>";
            $out .= "</ul>";
            $out .= "</div>";

            $out .= "<div style = 'float: right; width: 270px;  height:auto;'>";
            $out .= "<dl style = 'width: 270px; background: #f6f6f6; border: 1px solid #ECECEC; margin: 30px 0 30px; padding: 15px 20px;'>";
            $out .= "<dt style = 'height: 18px; width: 228px;'>You submitted this request</dt>";
            $out .= "<dt style = 'height: 18px; width: 228px;'>Status</dt>";
            $out .= "<dd style = 'height: 20px; width: 228px;'><span>" . status($status) . "</span></dd>";
            $out .= "<dt style = 'height: 18px; width: 228px;'>Category</dt>";
            $out .= "<dd style = 'height: 20px; width: 228px;'><span>" . $category['category'] . "</span></dd>";
            $out .= "</dl>";
            $out .= "</div>";
            sqlsrv_fetch($tdata, SQLSRV_SCROLL_NEXT);
          }
          else if ($replyid > 0)
          {
            $out .= "<div style = 'float: left; width: 670px; 'height:auto;  min-height:150px;'>";
            $out .= "<ul style = 'border-top: solid #bbb; border-width:thin; display: block; width: 670px;'>";
            $out .= "<li style = 'list-style-type: none; min-height:150px;'>";
            $out .= "<div" . $gm_div_style . ">";
            $out .= "<strong style = '" . $gm_color . "display: inline-block; font-size: 15px; margin: 0 0 15px 0;'>" . $replyname . "</strong>";
            $out .= "<span style = 'float: right; font-size: 15px; margin-right: 12px; margin-left: 12px; color: #bbb;'>" . $postdate -> format('Y-m-d') . " Post: " . $replyid . "</span>";
            $out .= "<div style = 'font-weight: lighter; line-height: 1.7; word-wrap: break-word;'>" . $content . "</div>";
            $out .= "</div>";
            $out .= "</li>";
            $out .= "</ul>";
            $out .= "</div>";
            sqlsrv_fetch($tdata, SQLSRV_SCROLL_NEXT);
          }

          $i++;
        }

        if ($status <= 1)
        {
          $out .= "<form class='ink-form' method='post' action='" . $script_name . "?action=" . $_GET['action'] . "&id=" . $entryid . "' style = 'float: left; width: 670px; height:auto; min-height:250px;'>";
          $out .= "<div>";
          $out .= "<input type='hidden' name='action' value='" . $_GET['action'] . "'/>";
          $out .= "<input type='hidden' name='entryid' value='" . $entryid . "'/>";
          $out .= "</div>";
          $out .= "<div style = 'width: 670px; height:auto; padding-bottom: 20px; padding-top: 30px; border-top: solid #bbb; border-width:thin;'>";
          $out .= "<textarea id='reply' name='comment' placeholder='Add your reply'></textarea>";
          $out .= "</div>";
          $out .= "<div style = 'width: 670px; height: 30px; margin-top: 10px; text-align: right;'>";
          $out .= "<input class ='ink-button' type='submit' name='add_reply' value='Add reply'/>";
          $out .= "</div>";
          $out .= "</form>";

          $out .= "<link rel='stylesheet' type='text/css' href='../framework/jquery.cleditor.css' />";
          $out .= "<script src='https://code.jquery.com/jquery-2.1.1.js'></script>";
          $out .= "<script type='text/javascript'' src='../framework/jquery.cleditor.min.js''></script>";
          $out .= "<script type='text/javascript'>";
          $out .= "$(document).ready(function () { $('#reply').cleditor(); });";
          $out .= "</script>";
        }
        $out .= "</div>";
        $out .= "<div style = 'clear:both;'></div>";
      }

      if (count($messages) > 0)
      {
        $out .= get_notification_html($messages, ERROR);
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
