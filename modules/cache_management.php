<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Cache Management";
  $permission = "administrator";
  return;
}
if ($this_script == $script_name)
{
  $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
  if ($userdata -> loggedin == True && $userdata -> is_superadmin())
  {
    $out .= "<center><h4>Handle RFCP Caching</h4></center>";

    $clear_all_cache = isset($_REQUEST['clear_all_cache']) ? intval($_REQUEST['clear_all_cache']) : 0;
    if ($clear_all_cache == 1)
    {
      clear_all_cache();
      $out .= get_notification_html("All Cache Cleared!", SUCCESS);
    }

    $out .= "<br>";
    $out .= "<a class=\"ink-button\" href='./" . $script_name . "?action=" . $_GET['action'] . "&clear_all_cache=1'>Clear All Cache</a>";
    $out .= "<br>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
