USE [RF_GAMECP]
GO
/****** Object:  Table [dbo].[ticket_categories]    Script Date: 06/05/2014 21:52:13 ******/
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (2, N'Donations', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (3, N'Client and login problems', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (1, N'Harrassment, abuse and scams', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (4, N'Dupes And Hacks', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (5, N'Staff abuse', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (6, N'Godzilla', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (999, N'Category Name', 1)
