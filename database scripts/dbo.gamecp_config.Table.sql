USE [RF_GAMECP]
GO
/****** Object:  Table [dbo].[gamecp_config]    Script Date: 06/05/2014 21:52:13 ******/
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'banner_url', N'http://freedomrf.com/downloads/FreedomBanner.gif', N'URL for Promo Banner')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'donations_credit_muntiplier', N'2', N'The number of times to muntiply the donation payment amount')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'donations_number_of_pay_options', N'21', N'Number of possible payment options to display')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'donations_start_credits', N'175', N'The starting credit for the starting price')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'donations_start_price', N'5', N'Starting price, or first payment option')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_bonus_formula', N'((x * 1) + (10 * ((x - 25) / 25))) - x', N'The formula to set the bonus a person recieves when purchsing (x = credits, set to 0 to disable bonuses)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_filename', N'index.php', N'The file name of the gamecp file (default: gamecp.php)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_gold_conversion_rate', N'0', N'The rate of 1 GP to Gold for the conversion (i.e How much can 1 GP make you?)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_log_types', N'', N'Game CP Log Types')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_logs_url', N'', N'Full URL to your logs viewer (i.e. http://server.aarondm.com/yourname/index.php)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_money_conversion_rate', N'0', N'The rate of 1 GP to Money for the conversion (i.e How much can 1 GP make you?)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_programname', N'Game CP', N'Short name of the RF Online Game CP (default: Game CP)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_enable', N'1', N'Enable or disable usage of the SMTP server to send mails')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_enable_ssl', N'1', N'Does your SMTP server require SSL?')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_password', N'w^iPtyPZiHtc', N'Password needed to log into the SMTP server')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_port', N'465', N'The SMTP port for your server (default: 25)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_server', N'host385.hostmonster.com', N'SMTP Server name (i.e. smtp.example.com)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_smtp_username', N'noreply+freedomrf.com', N'Username needed to log into the SMTP server')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_url', N'http://rfcp.freedomrf.com', N'The complete web address to the RF Online Game Control Panel')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'gamecp_version', N'1.0.0', N'Current version of the rf game control panel')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'google_currency', N'USD', N'The currency of accepted payment')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'google_merchant_id', N'0', N'Google Merchant ID for merchant payments')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'google_merchant_key', N'0', N'Google Merchant Key')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'google_server_type', N'sandbox', N'Google Server Type (standbox or google)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'lostpass_email', N'noreply@freedomrf.com', N'Email that will be used to send the password recovery emails')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'lostpass_message', N'A password retrieval has been requested at [RF YourServerName] for your account(s) with this email.', N'A message that will be displayed in a password recovery email')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'lostpass_subject', N'Your Server Password Retrieval', N'The subject of a password recovery email')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'market_order_by', N'4', N'Default sort order for the shop items (i.e. item_name)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'market_sort', N'2', N'Default sort (DESC or ASC)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_cancel_url', N'http://rfcp.freedomrf.com/index.php', N'The url a user will be sent to if cancelling a payment')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_currency', N'USD', N'Currency you want to the users to be charged (i.e. USD, GBP, EUR)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_email', N'donations@freedomrf.com', N'The paypal payment email address')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_ipn_url', N'http://rfcp.freedomrf.com/ipn/paypal_ipn.php', N'The complete URL or WEB ADDRESS to this game cp''s paypal paypal_ipn.php')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_return_url', N'http://rfcp.freedomrf.com/index.php', N'A URL that a user will be redirected to after completing a payment')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'paypal_ssl_connection', N'0', N'Enable the usage of SSL to connect to paypal.com for the IPN')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_confirm_email', N'1', N'Do you wish to use email confirmation?')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_enable_debug', N'0', N'Enable Game CP Debugging. Can be a security risk!')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_max_accounts', N'3', N'Maximum number of accounts per ip that can register (-1 to disable)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_recaptcha_enable', N'0', N'Enable re-captcha for your login page')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_recaptcha_private_key', N'6Lc1r-4SAAAAAAHX2p12LP5TdSJYAj6rm78F7C7t', N'Your private re-cpatcha key (get it at recpatcha.net)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_recaptcha_public_key', N'6Lc1r-4SAAAAALRWxwoaGSXC6OdWMcuRJUiF9RBi', N'Your public re-cpatcha key (get it at recpatcha.net)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_require_forum_login', N'0', N'If using a forum, enable to require users to log into the forum to visit the game cp')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_require_game_login', N'1', N'Require users to log in game before they can log into game cp')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_salt', N'1f2df4323d', N'A randomly generated ""slalt"" key added to passwords for security')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'security_show_version', N'0', N'Display the current game cp version to the public (buttom of the page)')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'server_name', N'Freedom RF', N'Your RF Online Server Name')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'specialgp_accname', N'100000', N'Special static cost for an account name change')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'specialgp_charname', N'50', N'Special static cost for a character name change')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'specialgp_gender', N'25', N'Special static cost for a gender change')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'vote_max_gp', N'7', N'Maximum number of game points a user can get by voting')
INSERT [dbo].[gamecp_config] ([config_name], [config_value], [config_description]) VALUES (N'vote_min_gp', N'4', N'Minimum number of game points a user can get (0 is the lowest!)')
