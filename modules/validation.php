<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "HIDDEN";
  $moduleLabel = "validation";
  $permission = "restricted";
  return;
}

$out_type = OUTPUT_JSON;
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "";

    if ($type == "item_name")
    {
      $out_json["name"] = "Invalid Item Code";
      $item_code = isset($_REQUEST["item_code"]) ? $_REQUEST["item_code"] : "";
      $attempt = get_item_info_from_code($item_code);
      if ($attempt["error"] == False)
      {
        $out_json["name"] = str_replace("_", " ", $attempt["item"]["item_name"]);
      }
    }
  }
}
