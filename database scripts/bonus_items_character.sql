USE [RF_GAMECP]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[gamecp_bonus_items_character](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bonus_item_account_serial] [int] NOT NULL,
	[bonus_item_char_serial] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_bonus_items_character] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


