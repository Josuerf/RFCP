<?php

function antiject($str)
{
  $str = trim($str);
  $str = str_replace("/'/", "''", $str);
  $str = str_replace("/\"/", "\"\"", $str);
  $str = str_replace("`", "", $str);
  $str = str_replace(";\$", "", $str);
  $str = str_replace("\\\\", "", $str);
  return $str;
}

$dbconnect = array ();

define("RFCP", "rfcp");
define("USER", "user");
define("DATA", "data");
define("ITEMS", "items");
define("HISTORY", "history");
define("BILLING", "billing");

//user, data, rfcp, items
function connectdb($db_to_connect)
{
  global $dbconnect;

  initiate_connection($db_to_connect);
  return $dbconnect[$db_to_connect];
}

function initiate_connection($db_to_connect)
{
  global $mssql;
  global $dbconnect;

  $connectionInfo = Array (
    "Database" => $mssql[$db_to_connect]['db'],
    "UID" => $mssql[$db_to_connect]['username'],
    "PWD" => $mssql[$db_to_connect]['password']
  );

  if (!($dbconnect[$db_to_connect] = @sqlsrv_connect($mssql[$db_to_connect]['host'], $connectionInfo)))
  {
    exit("Couldn't connect to user database server. " . db_error());
  }
}

function close_all_db()
{
  global $dbconnect;

  foreach ($dbconnect as $connection)
  {
    @sqlsrv_close($connection);
  }
}

function close_db($db_to_close)
{
  global $dbconnect;

  if (isset($dbconnect[$db_to_close]))
  {
    @sqlsrv_close($dbconnect[$db_to_close]);
  }
}

function db_error()
{
  return print_r(sqlsrv_errors(), True);
}

/*
 * 	User Functions
 */

function get_user_password($username)
{
  $username = antiject($username);

  $return = array (
    "error" => False,
    "errorMessage" => "",
    "password" => ""
  );

  $sql = "SELECT password = CAST(password as varchar(255)) FROM " . TABLE_LUACCOUNT . " WHERE id=CONVERT(binary,?)";
  $params = array ($username);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params, $options)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error.";
  }
  else if (!($row = sqlsrv_fetch_array($result)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "User not found";
  }
  else
  {
    $return["password"] = $row["password"];
  }

  return $return;
}

function change_user_password($username, $password)
{
  $username = antiject($username);
  $password = antiject($password);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE " . TABLE_LUACCOUNT . " SET Password = CONVERT(binary,?) WHERE ID = CONVERT(binary,?)";
  $params = array ($password, $username);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error.";
  }

  return $return;
}

function attempt_user_login($username, $password)
{
  global $super_admin;
  $username = antiject($username);
  $password = antiject($password);

  $login_return = array (
    "error" => False,
    "errorMessage" => "",
    "sessionid" => "",
    "username" => $username,
    "serial" => "-1"
  );

  $sql = "SELECT username = CAST(id as varchar(255)), password = CAST(password as varchar(255)) FROM " . TABLE_LUACCOUNT . " WHERE id=CONVERT(binary,?)";
  $params = array ($username);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $query_user = sqlsrv_query(connectdb(USER), $sql, $params, $options);
  if (sqlsrv_num_rows($query_user) <= 0)
  {
    $login_return["error"] = True;
    $login_return["errorMessage"] = "Invalid Username or Password. Please try again.";
  }
  else
  {
    $user_data = sqlsrv_fetch_array($query_user);
    if ($password == trim($user_data["password"]) && $username == trim($user_data["username"]))
    {
      $sql = "SELECT serial FROM tbl_UserAccount WHERE id=CONVERT(binary,?)";
      $params = array ($username);
      $query_account = sqlsrv_query(connectdb(USER), $sql, $params);
      $query_account = sqlsrv_fetch_array($query_account);
      $login_return["serial"] = $query_account['serial'];

      $sql = "SELECT nAccountSerial FROM tbl_UserBan WHERE nAccountSerial = ? AND nKind = 0";
      $params = array ($login_return['serial']);
      $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
      $query_ban = sqlsrv_query(connectdb(USER), $sql, $params, $options);
      if ($login_return['serial'] != "" && 0 < sqlsrv_num_rows($query_ban) && !in_array($login_return['username'], $super_admin))
      {
        $login_return["error"] = True;
        $login_return["errorMessage"] = "Your account has been blocked! Please contact an Administrator.";
        $_SESSION = array ();
        unset_cookies();
        gamecp_log(4, $login_return['username'], "GAMECP - LOGIN - Blocked account user tried to login", 1);
      }
      else if ($login_return['serial'] == "")
      {
        $login_return["error"] = True;
        $login_return["errorMessage"] = "You must log in the game (via the launcher) at least once in order to use the Game CP.";
        $_SESSION = array ();
        unset_cookies();
      }
      @sqlsrv_free_stmt($query_account);
      @sqlsrv_free_stmt($query_ban);
    }
    else
    {
      $login_return["error"] = True;
      $login_return["errorMessage"] = "Invalid Username or Password. Please try again.";
    }
  }

  if (!$login_return["error"])
  {
    $login_return["sessionid"] = getGUID();
    $sql = "INSERT INTO gamecp_sessions (session_username, session_account_serial, session_created_time, session_refreshed_time, session_guid, session_ip) VALUES (?, ?, ?, ?, ?, ?)";
    $params = array ($login_return['username'], $login_return['serial'], time(),
      time(), $login_return["sessionid"], $_SERVER['REMOTE_ADDR']);
    $query_session = sqlsrv_query(connectdb(RFCP), $sql, $params);
  }

  @sqlsrv_free_stmt($query_user);

  return $login_return;
}

function validate_session($sessionid, $username)
{
  $sessionid = antiject($sessionid);
  $username = antiject($username);

  $session_return = array (
    "error" => False,
    "errorMessage" => "",
    "sessionId" => $sessionid,
    "username" => $username,
    "serial" => "-1"
  );
  $sql = "SELECT [session_username], [session_account_serial], [session_refreshed_time], [session_ip] FROM [gamecp_sessions] WHERE [session_guid] = ?";
  $params = array ($sessionid);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $query_session = sqlsrv_query(connectdb(RFCP), $sql, $params, $options);
  if (sqlsrv_num_rows($query_session) == 1)
  {
    $session_row = sqlsrv_fetch_array($query_session);
    $time_diff = time() - $session_row["session_refreshed_time"];
    if ($session_row["session_username"] == $username && $_SERVER['REMOTE_ADDR'] == $session_row["session_ip"] && $time_diff < 3600)
    {
      $sql = "UPDATE [gamecp_sessions] SET [session_refreshed_time] = ? WHERE [session_guid] = ?";
      $params = array (time(), $sessionid);
      $query_session_update = sqlsrv_query(connectdb(RFCP), $sql, $params);
      $session_return["serial"] = $session_row["session_account_serial"];
    }
    else
    {
      terminate_session($sessionid);
      $session_return["error"] = True;
      $session_return["errorMessage"] = "Session is not valid.";
    }
  }
  else
  {
    $session_return["error"] = True;
    $session_return["errorMessage"] = "Session is not valid.";
  }

  @sqlsrv_free_stmt($query_session);

  return $session_return;
}

function terminate_session($sessionid)
{
  $sessionid = antiject($sessionid);

  $sql = "DELETE FROM [gamecp_sessions] WHERE [session_guid] = ?";
  $params = array ($sessionid);
  $query_session_delete = sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function get_user_data($serial)
{
  $serial = antiject($serial);

  $user_return = array (
    "error" => False,
    "errorMessage" => "",
    "data" => array ()
  );

  $sql = "SELECT AccountName = CAST(L.id AS varchar(255)), L.EMail, L.FacebookUserId, U.Serial, U.CreateTime, U.LastConnectIP, U.lastlogintime, U.lastlogofftime FROM " . TABLE_LUACCOUNT . " AS L INNER JOIN tbl_UserAccount AS U ON L.id = U.id WHERE U.serial = ?";
  $params = array ($serial);
  if (!($query_result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $user_return["error"] = True;
    $user_return["errorMessage"] = "SQL Error while trying to obtain user information";
    throw new Exception($user_return["errorMessage"]);
  }
  else if (!($row = sqlsrv_fetch_array($query_result)))
  {
    $user_return["error"] = True;
    $user_return["errorMessage"] = "Unable to find your user information!";
  }
  else
  {
    $user_return["data"] = $row;
  }

  @sqlsrv_free_stmt($query_result);
  close_db(USER);
  return $user_return;
}

/*
 *
 * 	Begin permissions
 *
 */

function get_user_permissions($serial)
{
  $serial = antiject($serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "exists" => False,
    "access" => array (),
    "customPermissions" => array (),
    "displayName" => ""
  );

  $sql = "SELECT admin_permission, admin_custom_permissions, admin_display_name FROM gamecp_permissions WHERE admin_serial = ?";
  $params = array ($serial);

  if (!($permission_query = @sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying retrieve permissions";
  }

  if (!($access = @sqlsrv_fetch_array(@$permission_query)))
  {
    $return["exists"] = False;
  }
  else
  {
    $return["exists"] = True;
    $return["access"] = explode(",", $access["admin_permission"]);
    $return["displayName"] = isset($access["admin_display_name"]) ? $access["admin_display_name"] : "";
    $return["customPermissions"] = explode(",", $access["admin_custom_permissions"]);
  }

  @sqlsrv_free_stmt(@$permission_query);
  return $return;
}

function get_all_permissions()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  $sql = "SELECT admin_id, admin_serial, admin_permission, admin_custom_permissions, admin_display_name FROM gamecp_permissions";
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying retrieve permissions";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($result))
    {
      $return["rows"][] = $row;
    }
  }

  return $return;
}

function update_user_permissions($serial, $permissions, $custom_permissions = "", $display_name = "")
{
  $serial = antiject($serial);
  $permissions = antiject($permissions);
  $custom_permissions = antiject($custom_permissions);
  $display_name = antiject($display_name);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_permissions SET admin_permission = ?, admin_custom_permissions = ?, admin_display_name = ? WHERE admin_serial = ?";
  $params = array ($permissions, $custom_permissions, $display_name, $serial);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to update permissions";
  }
  return $return;
}

function add_user_permissions($serial, $permissions, $custom_permissions = "", $display_name = "")
{
  $serial = antiject($serial);
  $permissions = antiject($permissions);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO gamecp_permissions (admin_serial, admin_permission, admin_custom_permissions, admin_display_name) VALUES (?,?,?,?)";
  $params = array ($serial, $permissions, $custom_permissions, $display_name);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to add permissions";
  }
  return $return;
}

function delete_user_permissions($serial)
{
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $serial = antiject($serial);
  $sql = "DELETE FROM gamecp_permissions WHERE admin_serial = ?";
  $params = array ($serial);
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to delete user permissions";
  }

  return $return;
}

//END PERMISSIONS

function get_user_credits($serial)
{
  $serial = antiject($serial);

  $user_return = array (
    "error" => False,
    "errorMessage" => "",
    "credits" => 0,
    "bonus_credits" => 0,
    "guild_credits" => 0
  );

  $sql = "SELECT user_points,user_vote_points,user_guild_points FROM gamecp_gamepoints WHERE user_account_id = ?";
  $params = array ($serial);
  if (!($user_points_result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $user_return["error"] = True;
    $user_return["errorMessage"] = "SQL Error while trying to obtain user credits";
  }
  else if (!($row = sqlsrv_fetch_array($user_points_result)))
  {
    $sql = "INSERT INTO gamecp_gamepoints (user_account_id, user_vote_points, user_points) VALUES (?, 0, 0)";
    $params = array ($serial);
    if (!($user_points_result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
    {
      $user_return["error"] = True;
      $user_return["errorMessage"] = "SQL Error while trying to insert user credits";
    }
  }
  else
  {
    $user_return["credits"] = $row["user_points"];
    $user_return["bonus_credits"] = $row["user_vote_points"];
    $user_return["guild_credits"] = $row["user_guild_points"];
  }

  @sqlsrv_free_stmt($user_points_result);
  close_db(RFCP);
  return $user_return;
}

function add_user_credits($serial, $credits)
{
  $serial = antiject($serial);
  $credits = antiject($credits);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_gamepoints SET user_points = user_points + ? WHERE user_account_id = ?";
  $params = array ($credits, $serial);

  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to add credits";
  }

  return $return;
}

function add_user_g_credits($serial, $credits)
{
  $serial = antiject($serial);
  $credits = antiject($credits);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_gamepoints SET user_guild_points = user_guild_points + ? WHERE user_account_id = ?";
  $params = array ($credits, $serial);

  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to add credits";
  }

  return $return;
}

function remove_user_g_credits($serial, $credits)
{
  $serial = antiject($serial);
  $credits = antiject($credits);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_gamepoints SET user_guild_points = user_guild_points - ? WHERE user_account_id = ?";
  $params = array ($credits, $serial);
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to remove credits";
  }

  return $return;
}

function remove_user_credits($serial, $credits)
{
  $serial = antiject($serial);
  $credits = antiject($credits);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_gamepoints SET user_points = user_points - ? WHERE user_account_id = ?";
  $params = array ($credits, $serial);
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to remove credits";
  }

  return $return;
}

function is_user_banned($serial)
{
  $serial = antiject($serial);
  $return_value = False;

  $sql = "SELECT nAccountSerial FROM tbl_UserBan WHERE nAccountSerial = ?";
  $params = array ($serial);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $query_ban = sqlsrv_query(connectdb(USER), $sql, $params, $options);
  if (0 < sqlsrv_num_rows($query_ban))
  {
    $return_value = True;
  }

  @sqlsrv_free_stmt($query_ban);
  close_db(USER);

  return $return_value;
}

function ban_user($serial, $period, $reason, $kind, $user_who_banned)
{
  $serial = antiject($serial);
  $period = antiject($period);
  $reason = antiject($reason);
  $kind = antiject($kind);
  $user_who_banned = antiject($user_who_banned);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO tbl_UserBan (nAccountSerial, nPeriod, nKind, szReason, GMWriter) VALUES (?,?,?,?,?)";
  $params = array ($serial, $period, $kind, $reason, $user_who_banned);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to ban user";
  }

  return $return;
}

function remove_ban($serial)
{
  $serial = antiject($serial);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "DELETE FROM tbl_UserBan WHERE nAccountSerial = ?";
  $params = array ($serial);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to remove";
  }

  return $return;
}

function update_ban($serial, $period, $reason, $kind)
{
  $serial = antiject($serial);
  $period = antiject($period);
  $reason = antiject($reason);
  $kind = antiject($kind);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE tbl_UserBan SET nPeriod = ?, szReason = ?, nKind = ? WHERE nAccountSerial = ?";
  $params = array ($period, $reason, $kind, $serial);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to ban user";
  }

  return $return;
}

function get_ban_record($serial)
{
  $serial = antiject($serial);

  $return = array (
    "error" => False,
    "errorMessage" => "",
    "row" => array ()
  );

  $sql = "SELECT nPeriod, szReason, nKind FROM tbl_UserBan WHERE nAccountSerial = ?";
  $params = array ($serial);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an SQL error while trying to find the ban record";
  }
  if ($row = sqlsrv_fetch_array($result))
  {
    $return["row"] = $row;
  }
  else
  {
    $return["error"] = True;
    $return["errorMessage"] = "No ban record found";
  }

  return $return;
}

function get_banned_username($serial)
{
  $serial = antiject($serial);

  $return = array (
    "error" => False,
    "errorMessage" => "",
    "username" => ""
  );

  $sql = "SELECT convert(varchar,id) as Account FROM tbl_UserBan AS B INNER JOIN tbl_UserAccount AS U ON B.nAccountSerial = U.Serial WHERE B.nAccountSerial = ?";
  $params = array ($serial);
  if (!($result = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an SQL error while trying to find the ban record";
  }
  if ($row = sqlsrv_fetch_array($result))
  {
    $return["username"] = $row["Account"];
  }
  else
  {
    $return["error"] = True;
    $return["errorMessage"] = "No ban record found";
  }

  return $return;
}

function is_username_unique($username)
{
  $username = antiject($username);

  $sql = "SELECT ID FROM " . TABLE_LUACCOUNT . " WHERE id=CONVERT(binary,?)";
  $params = array ($username);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $username_check = sqlsrv_query(connectdb(USER), $sql, $params, $options);

  if (0 < sqlsrv_num_rows($username_check))
  {
    return False;
  }

  $sql = "SELECT username FROM gamecp_confirm_email WHERE username=?";
  $params = array ($username);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $username_check = sqlsrv_query(connectdb(RFCP), $sql, $params, $options);

  if (0 < sqlsrv_num_rows($username_check))
  {
    return False;
  }

  return True;
}

function is_email_unique($email)
{
  $email = antiject($email);

  $sql = "SELECT Email FROM " . TABLE_LUACCOUNT . " WHERE Email=?";
  $params = array ($email);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $email_check = sqlsrv_query(connectdb(USER), $sql, $params, $options);

  if (0 < sqlsrv_num_rows($email_check))
  {
    return False;
  }

  $sql = "SELECT email FROM gamecp_confirm_email WHERE email=?";
  $params = array ($email);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $email_check = sqlsrv_query(connectdb(RFCP), $sql, $params, $options);

  if (0 < sqlsrv_num_rows($email_check))
  {
    return False;
  }

  return True;
}

function confirmed_user_exists($username)
{
  $username = antiject($username);

  $sql = "SELECT ID FROM " . TABLE_LUACCOUNT . " WHERE id=CONVERT(binary,?)";
  $params = array ($username);
  $options = array ("Scrollable" => SQLSRV_CURSOR_STATIC);
  $username_check = sqlsrv_query(connectdb(USER), $sql, $params, $options);

  if (0 < sqlsrv_num_rows($username_check))
  {
    return True;
  }

  return False;
}

function add_new_user($username, $password, $email, $ip, $securityLogin)
{
  $username = antiject($username);
  $password = antiject($password);
  $email = antiject($email);
  $ip = antiject($ip);

  $adduser_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO " . TABLE_LUACCOUNT . " (id,password,BCodeTU,Email) VALUES ((CONVERT (binary,?)),(CONVERT (binary,?)),1,?)";
  $params = array ($username, $password, $email);
  if (!($register_query = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $adduser_return["error"] = True;
    $adduser_return["errorMessage"] = "There was an SQL error while trying to add the account.";
  }
  else
  {
    if (!$securityLogin)
    {
      $sql = "INSERT INTO tbl_UserAccount (id, createip) VALUES(convert(binary,?), ?)";
      $params = array ($username, $ip);
      if (!($insert_result = sqlsrv_query(connectdb(USER), $sql, $params)))
      {
        $adduser_return["error"] = True;
        $adduser_return["errorMessage"] = "There was an SQL error while trying to add the account.";
      }
      else
      {
        new_register_log($username, time(), $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']);
        sqlsrv_free_stmt($insert_result);
      }
    }
    else
    {
      new_register_log($username, time(), $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']);
      sqlsrv_free_stmt($register_query);
    }
  }

  return $adduser_return;
}

function add_user_confirm($username, $email, $confirm_key)
{
  $username = antiject($username);
  $email = antiject($email);
  $confirm_key = antiject($confirm_key);

  $confirm_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO gamecp_confirm_email (username, email, confirm_key, created_at) VALUES (?, ?, ?, ?)";
  $params = array ($username, $email, $confirm_key, time());
  if (!($insert_result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $adduser_return["error"] = True;
    $adduser_return["errorMessage"] = "There was an SQL error while trying to add the confirmation record.";
  }

  return $confirm_return;
}

function delete_confirm_record($username, $email)
{
  $username = antiject($username);
  $email = antiject($email);

  $sql = "DELETE FROM gamecp_confirm_email WHERE username = ? AND email = ?";
  $params = array ($username, $email);
  $delete_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function validate_confirm_key($username, $confirm_key)
{
  $username = antiject($username);

  $confirm_return = array (
    "error" => False,
    "errorMessage" => "",
    "dbemail" => ""
  );

  $sql = "SELECT confirm_key, email FROM gamecp_confirm_email WHERE username = ?";
  $params = array ($username);
  $select_query = sqlsrv_query(connectdb(RFCP), $sql, $params);
  if ($row = sqlsrv_fetch_array($select_query))
  {
    if ($row["confirm_key"] == $confirm_key)
    {
      $confirm_return["dbemail"] = $row["email"];
    }
    else
    {
      $confirm_return["error"] = True;
      $confirm_return["errorMessage"] = "Confirmation key does not match";
    }
  }
  else
  {
    $confirm_return["error"] = True;
    $confirm_return["errorMessage"] = "No confirmation found for user";
  }

  return $confirm_return;
}

function add_user_referral($referred_by_serial, $new_user_name)
{
  $referred_by_serial = antiject($referred_by_serial);
  $new_user_name = antiject($new_user_name);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO gamecp_referrals (referred_by_serial, new_user_name) VALUES (?,?)";
  $params = array ($referred_by_serial, $new_user_name);
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an SQL error while trying to add the referral record.";
  }

  return $return;
}

function get_account_serial($account_name)
{
  $account_name = antiject($account_name);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "serial" => -1
  );

  $sql = "SELECT serial FROM tbl_UserAccount WHERE id = CONVERT(binary,?)";
  $params = array ($account_name);

  if (!($query = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the account";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find the account: " . $account_name;
  }
  else
  {
    $return["serial"] = $row["serial"];
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function check_account_serial($serial)
{
  $serial = antiject($serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "serial" => -1
  );

  $sql = "SELECT serial FROM tbl_UserAccount WHERE serial = ?";
  $params = array ($serial);

  if (!($query = sqlsrv_query(connectdb(USER), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an SQL error while trying to find the account";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "No such account serial found";
  }
  else
  {
    $return["serial"] = $row["serial"];
  }

  return $return;
}

function search_characters($search_query)
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  $sql = "SELECT B.DCK, B.Serial, B.Name, B.AccountSerial, B.Account, B.Lv, B.LastConnTime, B.CreateTime, B.DeleteName, B.Class, G.Class0, G.Class1, G.Class2, G.ClassInitCnt, G.GuildSerial, G.X, G.Y, G.Z FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial WHERE " . $search_query . " ORDER BY B.LastConnTime DESC";
  $params = array ();

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the characters";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find any matches to your criteria";
  }
  else
  {
    $return["rows"][] = $row;
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
    }
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function get_characters($account_serial, $full = False)
{
  $account_serial = antiject($account_serial);

  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  $sql = "SELECT Serial, Name, SUBSTRING(Class, 1, 1) AS Race, Lv FROM tbl_base WHERE DCK = 0 AND AccountSerial = ?";
  if (isset($full) && $full == True)
  {
    $sql = "SELECT TOP 3 B.Serial,B.Name,B.Lv,B.Class,B.Race,B.Dalant,B.Gold,B.EK0,B.EK1,B.EK2,B.EK3,B.EK4,B.EK5,B.EK6,B.EK7,B.EU0,B.EU1,B.EU2,B.EU3,B.EU4,B.EU5,B.EU6,B.EU7,G.PvpPoint,G.TotalPlayMin, G.Class0 FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial WHERE B.DCK = 0 AND B.AccountSerial = ?";
  }

  $params = array ($account_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the characters";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find any characters for the account";
  }
  else
  {
    $return["rows"][] = $row;
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
    }
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function get_characters_cached($account_serial)
{
  $account_serial = antiject($account_serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  if (get_cache("get_characters_cached_" . $account_serial))
  {
    $return = get_cache("get_characters_cached_" . $account_serial);
  }
  else
  {
    $return = get_characters($account_serial);
    if ($return["error"] == False)
    {
      add_cache("get_characters_cached_" . $account_serial, $return, 86400);
    }
  }

  return $return;
}

//serial or name
function get_character_info($char_serial)
{
  $char_serial = antiject($char_serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "char" => array ()
  );

  if (get_cache("get_character_info_" . $char_serial))
  {
    $return["char"] = get_cache("get_character_info_" . $char_serial);
    return $return;
  }

  if (is_numeric($char_serial))
  {
    $sql = "SELECT Serial, DCK, Name, SUBSTRING(Class, 1, 1) AS Race, Lv, AccountSerial, Race as RaceNum FROM tbl_base WHERE Serial = ?";
  }
  else
  {
    $sql = "SELECT Serial, DCK, Name, SUBSTRING(Class, 1, 1) AS Race, Lv, AccountSerial, Race as RaceNum FROM tbl_base WHERE Name = ?";
  }
  $params = array ($char_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the character";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find a character with that serial or name";
  }
  else
  {
    $return["char"] = $row;
    add_cache("get_character_info_" . $char_serial, $row, 86400);
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function get_character_full_info($char_serial)
{
  $char_serial = antiject($char_serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "char" => array ()
  );

  $sql = "SELECT B.Serial, B.Name, B.DCK, B.AccountSerial, B.Account, B.Slot, B.Race, ";
  $sql .= "B.Class, B.Lv, B.Dalant, B.Gold, B.Baseshape, G.PvpPoint, G.GuildSerial, ";
  $sql .= "G.TotalPlayMin, G.Map, G.Class0, G.Class1, G.Class2, G.ClassInitCnt, G.MaxLevel, ";
  $sql .= "G.Exp, G.LossExp, G.WM0, G.WM1, G.DM, G.PM ";
  $sql .= "FROM tbl_base AS B INNER JOIN tbl_general AS G ON G.Serial = B.Serial ";
  $sql .= "WHERE G.Serial = ?";
  $params = array ($char_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the character";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find a character with that serial";
  }
  else
  {
    $return["char"] = $row;
  }

  return $return;
}

function delete_character($char_serial)
{
  $char_serial = antiject($char_serial);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $params = array ($char_serial);

  sqlsrv_begin_transaction(connectdb(DATA));
  $sql = "EXEC pDelete_CharAllStep ?";
  $query = sqlsrv_query(connectdb(DATA), $sql, $params);

  if ($query)
  {
    sqlsrv_commit(connectdb(DATA));
  }
  else
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error deleting the character";
    sqlsrv_rollback(connectdb(DATA));
  }

  return $return;
}

function restore_character($char_serial)
{
  $char_serial = antiject($char_serial);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );
  $continue = false;
  $params = array ($char_serial);

  $sql = "SELECT deletename, AccountSerial from tbl_base WHERE serial = ?";
  $query = sqlsrv_query(connectdb(DATA), $sql, $params);
  if ($row = sqlsrv_fetch_array($query))
  {
    $name = $row["deletename"];
    $sql = "SELECT serial from tbl_base WHERE name = ?";
    $params = array ($name);
    $query = sqlsrv_query(connectdb(DATA), $sql, $params);
    if ($row = sqlsrv_fetch_array($query))
    {
      $return["error"] = True;
      $return["errorMessage"] = "The character name is already in use by serial: " . $row["serial"];
    }
    else
    {
      $continue = True;
    }

    if ($continue)
    {
      $account = $row["AccountSerial"];
      $sql = "SELECT serial from tbl_base WHERE DCK = 1 AND AccountSerial = ?";
      $params = array ($account);
      $query = sqlsrv_query(connectdb(DATA), $sql, $params);
      if (sqlsrv_num_rows($query) >= 3)
      {
        $return["error"] = True;
        $return["errorMessage"] = "Too many characters are active on the account, delete one before restoring";
      }
      else
      {
        $continue = True;
      }
    }
  }
  else
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find character with serial: " . $char_serial;
  }

  if ($continue)
  {
    $params = array ($char_serial);
    sqlsrv_begin_transaction(connectdb(DATA));
    $sql = "UPDATE tbl_base SET name = deletename  WHERE serial = ?";
    $query1 = sqlsrv_query(connectdb(DATA), $sql, $params);
    $sql = "UPDATE tbl_base SET deletename = '*'  WHERE serial = ?";
    $query2 = sqlsrv_query(connectdb(DATA), $sql, $params);
    $sql = "UPDATE tbl_base SET DCK = 0 WHERE serial =  ?";
    $query3 = sqlsrv_query(connectdb(DATA), $sql, $params);

    if ($query1 && $query2 && $query3)
    {
      sqlsrv_commit(connectdb(DATA));
    }
    else
    {
      $return["error"] = True;
      $return["errorMessage"] = "There was an error while restoring the character, process aborted";
      sqlsrv_rollback(connectdb(DATA));
    }
  }

  return $return;
}

function update_character_race($char_serial, $race)
{
  $char_serial = antiject($char_serial);
  $race = antiject($race);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE tbl_base SET Race = ? WHERE Serial = ?";
  $params = array ($race, $char_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Error while trying to update character race: " . $char_serial;
  }

  return $return;
}

function update_character_base_class($char_serial, $class)
{
  $char_serial = antiject($char_serial);
  $class = antiject($class);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE tbl_general SET Class0 = ? WHERE Serial = ?";
  $params = array ($class, $char_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Error while trying to update character baseclass: " . $char_serial;
  }

  return $return;
}

function update_character_currency($char_serial, $amount)
{
  $char_serial = antiject($char_serial);
  $amount = antiject($amount);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE tbl_base SET Dalant = ? WHERE Serial = ?";
  $params = array ($amount, $char_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Error while trying to update character baseclass: " . $char_serial;
  }

  return $return;
}

function update_character($char_serial, $dck, $name, $account_serial, $account, $map, $slot, $race, $base_class, $first_class, $second_class, $final_class, $level, $gold, $dalant, $maxlevel, $baseshape, $exp, $lossexp, $pvppoint, $guildserial, $totalplaymin)
{
  $char_serial = antiject($char_serial);
  $dck = antiject($dck);
  $name = antiject($name);
  $account_serial = antiject($account_serial);
  $account = antiject($account);
  $map = antiject($map);
  $slot = antiject($slot);
  $race = antiject($race);
  $base_class = antiject($base_class);
  $first_class = antiject($first_class);
  $second_class = antiject($second_class);
  $final_class = antiject($final_class);
  $level = antiject($level);
  $gold = antiject($gold);
  $dalant = antiject($dalant);
  $maxlevel = antiject($maxlevel);
  $baseshape = antiject($baseshape);
  $exp = antiject($exp);
  $guildserial = antiject($guildserial);
  $totalplaymin = antiject($totalplaymin);
  $lossexp = antiject($lossexp);
  $pvppoint = antiject($pvppoint);
  $params = array ();
  $sql = "UPDATE tbl_base SET DCK = ?";
  $params[] = $dck;
  if ($name != "")
  {
    $sql .= ", Name = ?";
    $params[] = $name;
  }
  if ($account_serial != "")
  {
    $sql .= ", AccountSerial = ?";
    $params[] = $account_serial;
  }
  if ($account != "")
  {
    $sql .= ", Account = ?";
    $params[] = $account;
  }
  if ($slot != "")
  {
    $sql .= ", Slot = ?";
    $params[] = $slot;
  }
  if ($race != "")
  {
    $sql .= ", Race = ?";
    $params[] = $race;
  }
  if ($final_class != "")
  {
    $sql .= ", Class = ?";
    $params[] = $final_class;
  }
  if ($level != "")
  {
    $sql .= ", Lv = ?";
    $params[] = $level;
  }
  if ($gold != "")
  {
    $sql .= ", Gold = ?";
    $params[] = $gold;
  }
  if ($dalant != "")
  {
    $sql .= ", Dalant = ?";
    $params[] = $dalant;
  }
  if ($baseshape != "")
  {
    $sql .= ", Baseshape = ?";
    $params[] = $baseshape;
  }
  $sql .= " WHERE Serial = ?";
  $params[] = $char_serial;
  $query = @sqlsrv_query(connectdb(DATA), $sql, $params);

  $params = array ();
  $sql = "UPDATE tbl_general SET MaxLevel = ?";
  $params[] = $maxlevel;
  if ($guildserial != "")
  {
    $sql .= ", GuildSerial = ?";
    $params[] = $guildserial;
  }
  if ($totalplaymin != "")
  {
    $sql .= ", TotalPlayMin = ?";
    $params[] = $totalplaymin;
  }
  if ($map != "")
  {
    $sql .= ", Map = ?";
    $params[] = $map;
  }
  if ($base_class != "")
  {
    $sql .= ", Class0 = ?";
    $params[] = $base_class;
  }
  if ($first_class != "")
  {
    $sql .= ", Class1 = ?";
    $params[] = $first_class;
  }
  if ($second_class != "")
  {
    $sql .= ", Class2 = ?";
    $params[] = $second_class;
  }
  if ($exp != "")
  {
    $sql .= ", Exp = ?";
    $params[] = $exp;
  }
  if ($lossexp != "")
  {
    $sql .= ", LossExp = ?";
    $params[] = $lossexp;
  }
  if ($pvppoint != "")
  {
    $sql .= ", PvpPoint = ?";
    $params[] = $pvppoint;
  }
  $sql .= " WHERE Serial = ?";
  $params[] = $char_serial;
  $query = @sqlsrv_query(connectdb(DATA), $sql, $params);

  $params = array ();
  $sql = "UPDATE tbl_pvporderview SET PvpPoint = ? WHERE Serial = ?";
  $params[] = $pvppoint;
  $params[] = $char_serial;
  $query = sqlsrv_query(connectdb(DATA), $sql, $params);
}

function delete_char_npcdata($char_serial)
{
  $char_serial = antiject($char_serial);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "DELETE FROM tbl_NpcData WHERE serial = ?";
  $params = array ($char_serial);
  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Error while trying to delete npcdata for serial: " . $char_serial;
  }

  return $return;
}

function get_guild_name($guild_serial)
{
  $guild_serial = antiject($guild_serial);

  $return = array (
    "error" => False,
    "errorMessage" => "",
    "id" => null
  );

  $sql = "SELECT id FROM tbl_Guild WHERE serial = ?";
  $params = array ($guild_serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the guild";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find any guilds to your criteria";
  }
  else
  {
    $return["id"] = $row["id"];
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function get_classes()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array (),
    "classes" => array (),
    "classesByCode" => array ()
  );

  if (get_cache("get_classes"))
  {
    return get_cache("get_classes");
  }

  $sql = "SELECT class_id, class_code, class_name FROM tbl_Classes WHERE class_name != '' ORDER BY class_id ASC";
  $params = array ();

  if (!($query = sqlsrv_query(connectdb(ITEMS), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the classes (Items DB may be missing classes table)";
  }
  else if (!($row = sqlsrv_fetch_array($query)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Could not find any matches to your criteria";
  }
  else
  {
    $return["rows"][] = $row;
    $return["classes"][$row["class_id"]] = $row;
    $return["classesByCode"][$row["class_code"]] = $row["class_name"];
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
      $return["classes"][$row["class_id"]] = $row;
      $return["classesByCode"][$row["class_code"]] = $row["class_name"];
    }
    add_cache("get_classes", $return, 86400);
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

function get_maps()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array (),
    "maps" => array ()
  );

  if (get_cache("get_maps"))
  {
    return get_cache("get_maps");
  }

  $sql = "SELECT map_id, map_name FROM tbl_Maps";
  $params = array ();

  if (!($query = sqlsrv_query(connectdb(ITEMS), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "There was an error running the query to find the maps (Items DB may be missing classes table)";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
      $return["maps"][$row["map_id"]] = $row;
    }
    add_cache("get_maps", $return, 86400);
  }

  @sqlsrv_free_stmt($query);
  return $return;
}

/*
 *
 * 	CONFIGURATION
 *
 */

function get_configuration()
{
  $config_return = array (
    "error" => False,
    "errorMessage" => "",
    "data" => array ()
  );

  $sql = "SELECT config_name, config_value, config_description FROM gamecp_config";
  if (!($config_query = sqlsrv_query(connectdb(RFCP), $sql)))
  {
    $config_return["error"] = True;
    $config_return["errorMessage"] = "Unable to retrieve configuration";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($config_query))
    {
      $config_return["data"][] = $row;
    }
  }

  @sqlsrv_free_stmt($config_query);
  return $config_return;
}

function update_configuration($config_items)
{
  $config_return = array (
    "error" => False,
    "errorMessage" => ""
  );


  foreach ($config_items as $key => $value)
  {
    $sql = "UPDATE gamecp_config SET config_value = ? WHERE config_name = ?";
    $params = array (antiject($value), antiject($key));
    if (!($config_query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
    {
      $config_return["error"] = True;
      $config_return["errorMessage"] = "Unable to update the config, please contact someone at NXT";
      return $config_return;
    }
  }

  return $config_return;
}

function load_configuration()
{
  global $config;
  $config = array ();

  $config_query = "SELECT config_name, config_value FROM gamecp_config";
  if (!($config_result = sqlsrv_query(connectdb(RFCP), $config_query)))
  {
    echo "Unable to obtain data from the configuration database";
    exit();
  }
  while ($row = sqlsrv_fetch_array($config_result))
  {
    $config[trim($row['config_name'])] = trim($row['config_value']);
  }
  @sqlsrv_free_stmt($config_result);

  close_db(RFCP);
}

/*
 *
 * 	Novus Market Related
 *
 */

function get_market_categories($sub_cat_id = -1, $cat_id = -1, $guild = false)
{
  $sub_cat_id = antiject($sub_cat_id);
  $cat_id = antiject($cat_id);
  global $script_name;
  $db = $guild ? 'gamecp_g_shop_categories' : 'gamecp_shop_categories';
  $cat_return = array (
    "error" => False,
    "errorMessage" => "",
    "categories" => array (),
    "categories_in_order" => array ()
  );
  $params = array ();

  if ($sub_cat_id > -1)
  {
    $sql = "SELECT cat_id, cat_name, cat_sub_id, cat_order, cat_description FROM ".$db." WHERE cat_sub_id = ? ORDER BY cat_order ASC";
    $params = array ($sub_cat_id);
  }
  else if ($cat_id > -1)
  {
    $sql = "SELECT cat_id, cat_name, cat_sub_id, cat_order, cat_description FROM ".$db." WHERE cat_id = ?";
    $params = array ($cat_id);
  }
  else
  {
    $sql = "SELECT cat_name, cat_id, cat_sub_id, cat_order, cat_description FROM ".$db."";
  }

  if (!($cat_query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $cat_return["error"] = True;
    $cat_return["errorMessage"] = "Unable to retrieve category information";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($cat_query))
    {
      $cat_return["categories"][$row["cat_id"]] = array (
        "label" => $row["cat_name"],
        "parent" => $row["cat_sub_id"],
        "id" => $row["cat_id"],
        "order" => $row["cat_order"],
        "description" => $row["cat_description"],
        "link" => "<a href=\"" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $row["cat_id"] . "\" style=\"text-decoration: none;\">" . $row["cat_name"] . "</a>"
      );
      $cat_return["categories_in_order"][] = array (
        "label" => $row["cat_name"],
        "parent" => $row["cat_sub_id"],
        "id" => $row["cat_id"],
        "order" => $row["cat_order"],
        "description" => $row["cat_description"]
      );
    }
  }

  @sqlsrv_free_stmt($cat_query);
  return $cat_return;
}

function update_category_order($cat_ids, $cat_order, $guild = false)
{
  $cat_return = array (
    "error" => False,
    "errorMessage" => ""
  );
  $db = $guild ? 'gamecp_g_shop_categories' : 'gamecp_shop_categories';

  foreach ($cat_ids as $key => $cat_id)
  {
    $catid = antiject($cat_id);
    $catorder = antiject($cat_order[$key]);
    $sql = "UPDATE ".$db." SET cat_order = ? WHERE cat_id = ?";
    $params = array ($catorder, $catid);
    if (!($cat_order_result = sqlsrv_query(connectdb(RFCP), $sql, $params)))
    {
      $cat_return["error"] = True;
      $cat_return["errorMessage"] = "Unable to update category order";
    }
  }

  return $cat_return;
}

function add_category($cat_name, $cat_sub_id, $cat_description, $cat_order, $guild = false)
{
  $cat_name = antiject($cat_name);
  $cat_sub_id = antiject($cat_sub_id);
  $cat_description = antiject($cat_description);
  $cat_order = antiject($cat_order);

  $db = $guild ? 'gamecp_g_shop_categories' : 'gamecp_shop_categories';

  $cat_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO ".$db." (cat_name, cat_sub_id, cat_description, cat_order) VALUES (?,?,?,?)";
  $params = array ($cat_name, $cat_sub_id, $cat_description, $cat_order);
  if (!($query_insert = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $cat_return["error"] = True;
    $cat_return["errorMessage"] = "SQL Error, Unable to insert new category";
  }

  return $cat_return;
}

function edit_category($cat_name, $cat_sub_id, $cat_description, $cat_order, $cat_id, $guild = false)
{
  $cat_name = antiject($cat_name);
  $cat_sub_id = antiject($cat_sub_id);
  $cat_description = antiject($cat_description);
  $cat_order = antiject($cat_order);
  $cat_id = antiject($cat_id);

  $cat_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $db = $guild ? 'gamecp_g_shop_categories' : 'gamecp_shop_categories';

  $sql = "UPDATE ".$db." SET cat_name = ?, cat_sub_id = ?, cat_description = ?, cat_order = ? WHERE cat_id = ?";
  $params = array ($cat_name, $cat_sub_id, $cat_description, $cat_order, $cat_id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $cat_return["error"] = True;
    $cat_return["errorMessage"] = "SQL Error, Unable to update the category";
  }

  return $cat_return;
}

function delete_category($cat_id, $guild = false)
{
  $cat_id = antiject($cat_id);

  $db = $guild ? 'gamecp_g_shop_categories' : 'gamecp_shop_categories';

  $sql = "DELETE FROM ".$db." WHERE cat_id = ?";
  $params = array ($cat_id);
  $delete_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function get_item_code_from_id($item_type, $item_id)
{
  $item_id = antiject($item_id);
  $sql = "SELECT item_code FROM " . get_table_name(intval($item_type)) . " WHERE item_id = ?";
  $params = array ($item_id);
  $result = @sqlsrv_query(connectdb(ITEMS), $sql, $params);
  $row = @sqlsrv_fetch_array($result);
  return $row["item_code"];
}

function get_item_info_from_id($item_type, $item_id)
{
  $item_id = antiject($item_id);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "item" => array ()
  );

  $sql = "SELECT item_code, item_id, item_name, item_icon_id FROM " . get_table_name(intval($item_type)) . " WHERE item_id = ?";
  $params = array ($item_id);
  $result = @sqlsrv_query(connectdb(ITEMS), $sql, $params);
  $row = @sqlsrv_fetch_array($result);
  $return["item"] = $row;

  return $return;
}

function get_item_info_from_code($item_code)
{
  $item_code = antiject($item_code);

  $item_return = array (
    "error" => False,
    "errorMessage" => "",
    "item" => null
  );
  $item_type = parse_item_code($item_code);
  if ($item_type > -1)
  {
    $sql = "SELECT item_id, item_name FROM " . get_table_name(parse_item_code($item_code)) . " WHERE item_code = ?";
    $params = array ($item_code);
    if (!($result = @sqlsrv_query(connectdb(ITEMS), $sql, $params)))
    {
      $item_return["error"] = True;
      $item_return["errorMessage"] = "SQL Error trying to obtain item id";
    }
    else if (!($item_return["item"] = @sqlsrv_fetch_array($result)))
    {
      $item_return["error"] = True;
      $item_return["errorMessage"] = "The item " . $item_code . " does not exist in the item type table";
    }
  }
  else
  {
    $item_return["error"] = True;
    $item_return["errorMessage"] = "Invalid item code";
  }

  return $item_return;
}

function get_market_item($market_item_id, $guild = false)
{
  $market_item_id = antiject($market_item_id);
  $item_return = array (
    "error" => False,
    "errorMessage" => "",
    "item" => null
  );

  $db = $guild ? 'gamecp_g_market_items' : 'gamecp_market_items';

  $sql = "SELECT TOP 1 item_cat_id,item_status,item_id,item_dbcode,item_upgrade,item_name,item_amount,
    item_custom_amount,item_description,item_image_url,item_price,item_buy_count,item_date_added,item_date_updated,
    item_race,item_rent_time FROM ".$db." WHERE item_delete = 0 AND item_id = ?";
  $params = array ($market_item_id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $item_return["error"] = True;
    $item_return["errorMessage"] = "SQL Error trying to obtain item information";
  }
  else if (!($item_return["item"] = sqlsrv_fetch_array($query)))
  {
    $item_return["error"] = True;
    $item_return["errorMessage"] = "Invalid item id";
  }

  return $item_return;
}

function delete_market_item($market_item_id, $guild = false)
{
  $db = $guild ? 'gamecp_g_market_items' : 'gamecp_market_items';
  $market_item_id = antiject($market_item_id);
  $sql = "UPDATE ".$db." SET item_delete = 1 WHERE item_id = ?";
  $params = array ($market_item_id);
  $query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function increment_market_item_purchased($market_item_id, $guild = false)
{
  $db = $guild ? 'gamecp_g_market_items' : 'gamecp_market_items';
  $market_item_id = antiject($market_item_id);
  $sql = "UPDATE ".$db." SET item_buy_count = item_buy_count + 1 WHERE item_id = ?";
  $params = array ($market_item_id);
  $query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function add_market_item($item_status, $item_cat_id, $item_date_added, $item_date_updated, $item_race, $item_price, $item_dbcode, $item_upgrade, $item_name, $item_description, $item_image_url, $item_amount, $item_custom_amount, $item_rent_time = 0, $guild = false)
{
  $db = $guild ? 'gamecp_g_market_items' : 'gamecp_market_items';
  $item_status = antiject($item_status);
  $item_cat_id = antiject($item_cat_id);
  $item_date_added = antiject($item_date_added);
  $item_date_updated = antiject($item_date_updated);
  $item_race = antiject($item_race);
  $item_price = antiject($item_price);
  $item_dbcode = antiject($item_dbcode);
  $item_upgrade = antiject($item_upgrade);
  $item_name = antiject($item_name);
  $item_description = antiject($item_description);
  $item_image_url = antiject($item_image_url);
  $item_amount = antiject($item_amount);
  $item_custom_amount = antiject($item_custom_amount);
  $item_rent_time = antiject($item_rent_time);
  $item_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO ".$db." (item_status,item_cat_id,item_date_added,item_date_updated,item_race,item_price,item_dbcode,item_upgrade,item_name,item_description,item_image_url,item_amount,item_custom_amount,item_rent_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  $params = array ($item_status, $item_cat_id, $item_date_added, $item_date_updated,
    $item_race, $item_price, $item_dbcode, $item_upgrade, $item_name, $item_description,
    $item_image_url, $item_amount, $item_custom_amount, $item_rent_time);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $item_return["error"] = True;
    $item_return["errorMessage"] = "SQL Error trying to save new item";
  }
  return $item_return;
}

function update_market_item($edit_item_id, $item_status, $item_cat_id, $item_date_updated, $item_race, $item_price, $item_dbcode, $item_upgrade, $item_name, $item_description, $item_image_url, $item_amount, $item_custom_amount, $item_rent_time, $guild = false)
{
  $db = $guild ? 'gamecp_g_market_items' : 'gamecp_market_items';
  $edit_item_id = antiject($edit_item_id);
  $item_status = antiject($item_status);
  $item_cat_id = antiject($item_cat_id);
  $item_date_updated = antiject($item_date_updated);
  $item_race = antiject($item_race);
  $item_price = antiject($item_price);
  $item_dbcode = antiject($item_dbcode);
  $item_upgrade = antiject($item_upgrade);
  $item_name = antiject($item_name);
  $item_description = antiject($item_description);
  $item_image_url = antiject($item_image_url);
  $item_amount = antiject($item_amount);
  $item_custom_amount = antiject($item_custom_amount);
  $item_rent_time = antiject($item_rent_time);
  $item_return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE ".$db." SET item_status = ?, item_date_updated = ?, item_cat_id = ?, item_race = ?, item_price = ?, item_dbcode = ?, item_upgrade = ?, item_name = ?, item_description = ?, item_image_url = ?, item_amount = ?, item_custom_amount = ?, item_rent_time = ? WHERE item_delete = 0 AND item_id = ?";
  $params = array ($item_status, $item_date_updated, $item_cat_id, $item_race, $item_price,
    $item_dbcode, $item_upgrade, $item_name, $item_description, $item_image_url,
    $item_amount, $item_custom_amount, $item_rent_time, $edit_item_id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $item_return["error"] = True;
    $item_return["errorMessage"] = "SQL Error trying to update the item";
  }
  return $item_return;
}

function give_character_item($serial, $k_value, $amount, $u_value, $rent_time)
{
  $serial = antiject($serial);
  $k_value = antiject($k_value);
  $amount = antiject($amount);
  $u_value = antiject($u_value);
  $rent_time = antiject($rent_time);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  if ($rent_time > 0)
  {
    $sql = "INSERT INTO tbl_ItemCharge (nAvatorSerial, nItemCode_K, nItemCode_D, nItemCode_U, T) VALUES (?,?,?,?,?)";
  }
  else
  {
    $sql = "INSERT INTO tbl_ItemCharge (nAvatorSerial, nItemCode_K, nItemCode_D, nItemCode_U) VALUES (?,?,?,?)";
  }

  $params = array ($serial, $k_value, $amount, $u_value, $rent_time);
  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to give the item";
  }

  return $return;
}

function give_character_bonus_items($aserial, $cserial, $credits_level, $txn_id)
{
  $cserial = antiject($cserial);
  $aserial = antiject($aserial);
  $credits_level = antiject($credits_level);
  $txn_id = antiject($txn_id);
  $char_info = get_character_info($cserial);
  $char_race = parse_base_race($char_info["char"]["RaceNum"]);

  $attempt = get_bonus_items_from_config();
  if ($attempt["error"] == False && count($attempt["rows"]) > 0)
  {
    foreach ($attempt["rows"] as $key => $row)
    {
      if ($row['bonus_race'] == 0 || $row['bonus_race'] == $char_race ||
      (($char_race == 1 || $char_race == 2) && $row['bonus_race'] == 4))
      {
        $k_value = $row['bonus_item_k'];
        $u_value = $row['bonus_item_u'];
        $d_value = $row['bonus_item_d'];
        $t_value = $row['bonus_item_t'];

        for ($i = 0; $i < $credits_level; $i++)
        {
          $attempt2 = give_character_item($cserial, $k_value, $d_value, $u_value, $t_value);
          if ($attempt2["error"] == False)
          {
            bonus_item_log($aserial, $cserial, $k_value, $u_value, $d_value, $t_value, $txn_id);
          }
        }
      }
    }
  }
}

function get_unclaimed_items($serial)
{
  $serial = antiject($serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  $sql = "SELECT nSerial, nItemCode_K, nItemCode_D, nItemCode_U, dtGiveDate, dtTakeDate from tbl_ItemCharge where nAvatorSerial = ? and DCK = 0 order by dtGiveDate DESC";
  $params = array ($serial);

  if (!($query = sqlsrv_query(connectdb(DATA), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to retrieve unclaimed items";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
    }
  }

  return $return;
}

function get_server_stats($rows_to_show = 10)
{
  $rows_to_show = antiject($rows_to_show);
  $return = array (
    "latest" => array (),
    "accounts" => 0,
    "characters" => 0,
    "rows" => array ()
  );

  $sql = "SELECT TOP 1 serial,nBellaUser,nCoraUser,nAccUser,nAverageUser,nMaxUser,ServerName FROM tbl_ServerUser_Log ORDER BY serial DESC";
  $result = @sqlsrv_query(connectdb(USER), $sql);
  $return["latest"] = @sqlsrv_fetch_array($result);

  $sql = "SELECT count(Serial) FROM tbl_UserAccount";
  $result = @sqlsrv_query(connectdb(USER), $sql);
  $row = @sqlsrv_fetch_array($result);
  $return["accounts"] = $row[0];

  $sql = "SELECT count(Serial) FROM tbl_base WHERE DCK=0";
  $result = @sqlsrv_query(connectdb(DATA), $sql);
  $row = @sqlsrv_fetch_array($result);
  $return["characters"] = $row[0];

  $sql = "SELECT TOP " . $rows_to_show . " Convert(nvarchar(5),dtDate,108) AS Date, serial, nAverageUser, nMaxUser FROM tbl_ServerUser_Log ORDER BY serial DESC";
  $result = @sqlsrv_query(connectdb(USER), $sql);
  while ($row = @sqlsrv_fetch_array($result))
  {
    $return["rows"][] = $row;
  }

  return $return;
}

/*
 *
 * 	Voting
 *
 */

function get_vote_sites()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  if (get_cache("get_vote_sites"))
  {
    return get_cache("get_vote_sites");
  }

  $sql = "SELECT vote_id, vote_site_name, vote_site_url, vote_site_image, vote_reset_time, vote_count FROM gamecp_vote_sites";
  $query = sqlsrv_query(connectdb(RFCP), $sql);
  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to obtain vote sites";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][$row["vote_id"]] = $row;
    }

    add_cache("get_vote_sites", $return, 86400);
  }

  return $return;
}

function add_vote_site($name, $url, $image, $time)
{
  $name = antiject($name);
  $url = antiject($url);
  $image = antiject($image);
  $time = antiject($time);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO gamecp_vote_sites (vote_site_name, vote_site_url, vote_site_image, vote_reset_time) VALUES (?,?,?,?)";
  $params = array ($name, $url, $image, $time);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = true;
    $return["errorMessage"] = "SQL Error while trying to add vote site";
  }

  return $return;
}

function update_vote_site($id, $name, $url, $image, $time)
{
  $id = antiject($id);
  $name = antiject($name);
  $url = antiject($url);
  $image = antiject($image);
  $time = antiject($time);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "UPDATE gamecp_vote_sites SET vote_site_name = ?, vote_site_url = ?, vote_site_image = ?, vote_reset_time = ? WHERE vote_id = ?";
  $params = array ($name, $url, $image, $time, $id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = true;
    $return["errorMessage"] = "SQL Error while trying to update the vote site";
  }

  return $return;
}

function delete_vote_site($id)
{
  $id = antiject($id);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "DELETE FROM gamecp_vote_sites WHERE vote_id = ?";
  $params = array ($id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = true;
    $return["errorMessage"] = "SQL Error while trying to delete the vote site";
  }

  return $return;
}

function get_last_vote($serial, $vote_id)
{
  $serial = antiject($serial);
  $vote_id = antiject($vote_id);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "vote_log" => array ()
  );

  $sql = "SELECT TOP 1 log_id, log_account_serial, log_time, log_ip, log_points_gained, log_total_points, site_id FROM gamecp_vote_log WHERE log_account_serial = ? AND site_id = ? ORDER BY log_id DESC";
  $params = array ($serial, $vote_id);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "DB Error trying to obtain last vote info";
  }
  else
  {
    $return["vote_log"] = sqlsrv_fetch_array($query);
  }

  return $return;
}

function get_last_vote_any($serial)
{
  $serial = antiject($serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "vote_log" => array ()
  );

  $sql = "SELECT TOP 1 log_id, log_account_serial, log_time, log_ip, log_points_gained, log_total_points, site_id FROM gamecp_vote_log WHERE log_account_serial = ? ORDER BY log_id DESC";
  $params = array ($serial);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "DB Error trying to obtain last vote info";
  }
  else
  {
    $return["vote_log"] = sqlsrv_fetch_array($query);
  }

  return $return;
}

function get_ticket_category($id)
{
  $error = "";
  $category = "";
  $permission = "";
  $csql = "SELECT category, permission FROM ticket_categories WHERE id = ?";
  $param = array ($id);
  $cdata = sqlsrv_query(connectdb(RFCP), $csql, $param);
  if ($cdata === false)
  {
    $error = ('Error selecting caregory');
  }
  sqlsrv_fetch($cdata);
  $category = sqlsrv_get_field($cdata, 0);
  $permission = sqlsrv_get_field($cdata, 1);

  return array ("category" => $category, "error" => $error, "permission" => $permission);
}

function get_ticket_category_list()
{
  $error = "";
  $category = "";
  $csql = "SELECT id, category, permission FROM ticket_categories";
  $cdata = sqlsrv_query(connectdb(RFCP), $csql);
  if ($cdata === false)
  {
    $error = ('Error selecting caregory');
  }
  else
  {
    while ($row = sqlsrv_fetch_array($cdata))
    {
      $category[] = array ("id" => $row['id'], "category" => $row['category'], "permission" => $row['permission']);
    }
  }
  return array ("categories" => $category, "error" => $error);
}

function get_ticketconfig()
{
  $error = "";
  $category = "";
  $cat = get_ticket_category_list();
  if ($cat['error'] == "")
  {
    $category = $cat['categories'];
  }
  else
  {
    $error[] = $cat['error'];
  }

  return array (
    "error" => $error,
    "categories" => $category
  );
}

//***************
//
// Bonus items
//
//***************

function add_bonus_item_to_config($k_value, $amount, $u_value, $rent_time, $race)
{
  $k_value = antiject($k_value);
  $amount = antiject($amount);
  $u_value = antiject($u_value);
  $rent_time = antiject($rent_time);
  $race = antiject($race);

  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "INSERT INTO [gamecp_bonus_items] (bonus_item_k, bonus_item_u, bonus_item_d, bonus_item_t, bonus_race) VALUES (?,?,?,?,?)";

  $params = array ($k_value, $amount, $u_value, $rent_time, $race);
  if (!($query = sqlsrv_query(connectdb(RFCP), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to give the item";
  }

  return $return;
}

function get_bonus_items_from_config()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "rows" => array ()
  );

  $sql = "SELECT id, bonus_item_k, bonus_item_u, bonus_item_d, bonus_item_t, bonus_race FROM gamecp_bonus_items";
  $query = sqlsrv_query(connectdb(RFCP), $sql);

  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to obtain bonus items";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["rows"][] = $row;
    }
  }

  return $return;
}

function delete_bonus_item($id)
{
  $id = antiject($id);
  $sql = "DELETE FROM [gamecp_bonus_items] WHERE [id] = ?";
  $params = array ($id);
  $query_session_delete = sqlsrv_query(connectdb(RFCP), $sql, $params);
}

function get_bonus_item_character($serial)
{
  $serial = antiject($serial);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "char_serial" => -1
  );

  $sql = "SELECT bonus_item_char_serial FROM gamecp_bonus_items_character WHERE bonus_item_account_serial = ?";
  $query = sqlsrv_query(connectdb(RFCP), $sql, array ($serial));
  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to obtain bonus items character";
  }
  else if ($row = sqlsrv_fetch_array($query))
  {
    $return["char_serial"] = $row[0];
  }

  return $return;
}

function set_bonus_item_character($aserial, $cserial)
{
  $aserial = antiject($aserial);
  $cserial = antiject($cserial);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $attempt = get_bonus_item_character($aserial);
  if ($attempt["char_serial"] == -1)
  {
    $sql = "INSERT INTO [gamecp_bonus_items_character] (bonus_item_char_serial, bonus_item_account_serial) VALUES (?,?)";
  }
  else
  {
    $sql = "UPDATE gamecp_bonus_items_character SET bonus_item_char_serial = ? WHERE bonus_item_account_serial = ?";
  }

  $query = sqlsrv_query(connectdb(RFCP), $sql, array ($cserial, $aserial));

  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to save bonus items character";
  }

  return $return;
}

function add_optout($email)
{
  $time = time();
  $email = antiject($email);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $attempt = get_optouts($email);
  if (count($attempt["users"]) == 0)
  {
    $sql = "INSERT INTO [gamecp_email_optout] (email_optout_email, email_optout_time) VALUES (?,?)";
  }
  else
  {
    return $return;
  }

  $query = sqlsrv_query(connectdb(RFCP), $sql, array ($email, $time));

  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to save bonus items character";
  }

  return $return;
}

function get_optouts($email = "")
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "users" => array ()
  );

  if ($email == "")
  {
    $sql = "SELECT email_optout_email FROM gamecp_email_optout";
    $query = sqlsrv_query(connectdb(RFCP), $sql);
  }
  else
  {
    $sql = "SELECT email_optout_email FROM gamecp_email_optout WHERE email_optout_email = ?";
    $query = sqlsrv_query(connectdb(RFCP), $sql, array ($email));
  }

  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to get optouts";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      $return["users"][] = trim($row[0]);
    }
  }

  return $return;
}

function get_user_emails()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "emails" => array ()
  );

  $optouts = get_optouts();
  $optouts = $optouts["users"];

  $sql = "SELECT convert(varchar, id) as username, email FROM " . TABLE_LUACCOUNT;
  $query = sqlsrv_query(connectdb(USER), $sql);
  if (!$query)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error trying to email addresses";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($query))
    {
      if (!in_array(trim($row[1]), $optouts))
      {
        $return["emails"][] = $row[1];
      }
    }
  }

  return $return;
}

function get_mav_suspects()
{
  global $mssql;
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "passwords" => array (),
    "fireguard_passwords" => array (),
    "fireguard_hints" => array (),
    "bank_passwords" => array (),
    "bank_hints" => array (),
    "createips" => array (),
    "lastips" => array ()
  );

  $sql = "SELECT Convert(varchar,password), COUNT(password) FROM " . TABLE_LUACCOUNT . " A LEFT JOIN tbl_UserAccount U ON A.id = U.id WHERE serial NOT IN (SELECT nAccountSerial FROM tbl_UserBan) GROUP BY password HAVING COUNT(password) > 2 ORDER BY COUNT(password) desc";
  $query = sqlsrv_query(connectdb(USER), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["passwords"][] = $row;
  }

  $sql = "SELECT Convert(varchar,createip), COUNT(createip) FROM tbl_UserAccount WHERE serial NOT IN (SELECT nAccountSerial FROM tbl_UserBan) GROUP BY createip HAVING COUNT(createip) > 2 ORDER BY COUNT(createip) desc";
  $query = sqlsrv_query(connectdb(USER), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["createips"][] = $row;
  }

  $sql = "SELECT Convert(varchar,uilock_pw), COUNT(uilock_pw) FROM tbl_UserAccount WHERE uilock_pw != convert(binary(13),'') AND serial NOT IN (SELECT nAccountSerial FROM tbl_UserBan) GROUP BY uilock_pw HAVING COUNT(uilock_pw) > 2 ORDER BY COUNT(uilock_pw) desc";
  $query = sqlsrv_query(connectdb(USER), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["fireguard_passwords"][] = $row;
  }

  $sql = "SELECT Convert(varchar,uilock_hintanswer), COUNT(uilock_hintanswer) FROM tbl_UserAccount WHERE uilock_hintanswer != convert(binary(17),'') AND serial NOT IN (SELECT nAccountSerial FROM tbl_UserBan) GROUP BY uilock_hintanswer HAVING COUNT(uilock_hintanswer) > 2 ORDER BY COUNT(uilock_hintanswer) desc";
  $query = sqlsrv_query(connectdb(USER), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["fireguard_hints"][] = $row;
  }

  $sql = "SELECT Convert(varchar,lastconnectip), COUNT(lastconnectip) FROM tbl_UserAccount WHERE serial NOT IN (SELECT nAccountSerial FROM tbl_UserBan) GROUP BY lastconnectip HAVING COUNT(lastconnectip) > 2 ORDER BY COUNT(lastconnectip) desc";
  $query = sqlsrv_query(connectdb(USER), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["lastips"][] = $row;
  }

  $sql = "SELECT Convert(varchar,TrunkPass), COUNT(TrunkPass) FROM tbl_AccountTrunk WHERE TrunkPass != convert(binary(24),'') AND AccountSerial NOT IN (SELECT nAccountSerial FROM " . $mssql['user']['db'] . ".dbo.tbl_UserBan) GROUP BY TrunkPass HAVING COUNT(TrunkPass) > 2 ORDER BY COUNT(TrunkPass) desc";
  $query = sqlsrv_query(connectdb(DATA), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["bank_passwords"][] = $row;
  }

  $sql = "SELECT Convert(varchar,HintAnswer), COUNT(HintAnswer) FROM tbl_AccountTrunk WHERE HintAnswer != '*' AND AccountSerial NOT IN (SELECT nAccountSerial FROM " . $mssql['user']['db'] . ".dbo.tbl_UserBan) GROUP BY HintAnswer HAVING COUNT(HintAnswer) > 2 ORDER BY COUNT(HintAnswer) desc";
  $query = sqlsrv_query(connectdb(DATA), $sql);
  while ($row = sqlsrv_fetch_array($query))
  {
    $return["bank_hints"][] = $row;
  }

  return $return;
}

function get_ticket_permission($serial)
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "permission" => "",
    "serial" => $serial
  );
  $sql = "SELECT rank FROM ticket_permission WHERE aserial={$serial}";
  if (!($result = sqlsrv_query(connectdb(RFCP), $sql)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error";
  }
  else if (!($row = sqlsrv_fetch_array($result)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "Account not found";
  }
  else
  {
    $return["permission"] = $row["rank"];
  }

  return $return;
}

function get_tpermission_data()
{
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "data" => array ()
  );
  $sql = "SELECT aserial,rank FROM ticket_permission";
  $result = sqlsrv_query(connectdb(RFCP), $sql);
  if (!$result)
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
    {
      $return["data"][] = $row;
    }
  }

  return $return;
}

function updateSubscriptionStatus($username, $date)
{
  $date = antiject($date);
  $username = antiject($username);
  $return = array (
    "error" => False,
    "errorMessage" => ""
  );

  $sql = "EXEC pUpdate_billing ?, 2, ?";
  $params = array ($username, $date);
  if (!($result = sqlsrv_query(connectdb(BILLING), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to set subscription";
  }

  return $return;
}

function getUserPremiumStatus($username)
{
  $username = antiject($username);
  $return = array (
    "error" => False,
    "errorMessage" => "",
    "data" => array ()
  );

  $sql = "EXEC getAccountStatus ?";
  $params = array ($username);
  if (!($result = sqlsrv_query(connectdb(BILLING), $sql, $params)))
  {
    $return["error"] = True;
    $return["errorMessage"] = "SQL Error while trying to set subscription";
  }
  else
  {
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
    {
      $return["data"] = $row;
    }
  }
  return $return;
}
