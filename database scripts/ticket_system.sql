USE RF_GAMECP

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ticket_permission](
	[aserial] [int] NOT NULL,
	[rank] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[ticket_categories]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ticket_categories](
	[id] [int] NOT NULL,
	[category] [nvarchar](40) NOT NULL,
	[permission] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gamecp_ticket_entry]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gamecp_ticket_entry](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[type] [nvarchar](16) NOT NULL,
	[account] [nvarchar](16) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[status] [nvarchar](16) NOT NULL,
	[permission] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_ticket_entry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gamecp_ticket_data]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gamecp_ticket_data](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[entryid] [int] NOT NULL,
	[isgm] [int] NOT NULL,
	[name] [nvarchar](16) NOT NULL,
	[ticket] [nvarchar](max) NOT NULL,
	[replyid] [int] NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_gamecp_ticket_data] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Default [DF__gamecp_tic__date__49C3F6B7]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_entry] ADD  DEFAULT (getdate()) FOR [date]
GO
/****** Object:  Default [DF_gamecp_ticket_data_replyid]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_data] ADD  CONSTRAINT [DF_gamecp_ticket_data_replyid]  DEFAULT ((0)) FOR [replyid]
GO
/****** Object:  Default [DF__gamecp_tic__date__4AB81AF0]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_data] ADD  DEFAULT (getdate()) FOR [date]

GO
/****** Object:  Table [dbo].[ticket_categories]    Script Date: 06/05/2014 21:52:13 ******/
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (2, N'Donations', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (3, N'Client and login problems', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (1, N'Harrassment, abuse and scams', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (4, N'Dupes And Hacks', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (5, N'Staff abuse', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (6, N'Godzilla', 1)
INSERT [dbo].[ticket_categories] ([id], [category], [permission]) VALUES (999, N'Category Name', 1)