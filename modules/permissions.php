<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Permissions";
  $permission = "administrator";
  return;
}
if ($this_script == $script_name)
{
  $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
  if ($userdata -> loggedin == True && $userdata -> is_superadmin())
  {
    $links = array ();
    $links[] = "<a href=\"" . $script_name . "?action=" . $_GET['action'] . "\">View Users</a>";
    $links[] = "<a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=add\">Add User</a>";

    $active_index = 0;
    if ($page == "add")
    {
      $active_index = 1;
    }
    else if ($page == "edit")
    {
      $active_index = 2;
      $links[] = "<a href='#'>Edit User</a>";
    }
    else if ($page == "delete")
    {
      $active_index = 2;
      $links[] = "<a href='#'>Delete User</a>";
    }

    $out .= generate_module_nav_html($links, $active_index);
    if ($page == "")
    {
      $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td class=\"thead\" nowrap>ID</td>";
      $out .= "<td class=\"thead\" nowrap>Username</td>";
      $out .= "<td class=\"thead\" nowrap>Display Name</td>";
      $out .= "<td class=\"thead\" nowrap>Serial</td>";
      $out .= "<td class=\"thead\" nowrap>Permissions</td>";
      $out .= "<td class=\"thead\" nowrap>Permissions</td>";
      $out .= "<td class=\"thead\" nowrap>Options</td>";
      $out .= "</tr>";

      $permissions = get_all_permissions();

      if ($permissions["error"] == True)
      {
        $out .= get_notification_html($permissions["errorMessage"], ERROR);
      }
      else if (count($permissions["rows"]) <= 0)
      {
        $out .= get_notification_html("No users have been given special permission", ERROR);
      }
      foreach ($permissions["rows"] as $key => $row)
      {
        $user_result = get_user_data($row["admin_serial"]);
        $user_name = filter_string_for_html($user_result["data"]["AccountName"]);
        $display_name = filter_string_for_html($row["admin_display_name"]);
        $custom_permissions = $row["admin_custom_permissions"];

        $module_names = "";
        $explode = explode(",", $row['admin_permission']);
        $fileinfo = get_cache("fileinfo");
        $fileinfo = explode("\n", $fileinfo);
        $setmodules = true;

        foreach ($explode as $module)
        {
          if ($module_names != "")
          {
            $module_names .= ",";
          }

          foreach ($fileinfo as $moduleinfo)
          {
            $info = explode("|", $moduleinfo);
            $module_name = $module . ".php";
            if ($info[0] == $module_name)
            {
              $module_names .= $info[2];
            }
          }
        }

        if ($custom_permissions != "")
        {
          if ($module_names != "")
          {
            $module_names .= ",";
          }
          $module_names .= $custom_permissions;
        }

        $out .= "<tr>";
        $out .= "<td class=\"alt2\" valign=\"top\" nowrap>" . $row['admin_id'] . "</td>";
        $out .= "<td class=\"alt1\" valign=\"top\" nowrap>" . $user_name . "</td>";
        $out .= "<td class=\"alt1\" valign=\"top\" nowrap>" . $display_name . "</td>";
        $out .= "<td class=\"alt1\" valign=\"top\" nowrap>" . $row['admin_serial'] . "</td>";
        $out .= "<td class=\"alt1\" valign=\"top\" nowrap>" . str_replace(",", "<br/>", $module_names) . "</td>";
        $out .= "<td class=\"alt1\" valign=\"top\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=edit&serial=" . $row['admin_serial'] . "\">Edit User</a> | <a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=delete&serial=" . $row['admin_serial'] . "\">Delete User</a></td>";
        $out .= "</tr>";
      }
      $out .= "</table>";
    }
    else
    {
      if ($page == "edit")
      {
        $admin_files = "";
        $support_files = "";

        if (isset($_POST['serial']))
        {
          $serial = $_POST['serial'];
        }
        else if (isset($_GET['serial']))
        {
          $serial = $_GET['serial'];
        }
        else
        {
          $serial = "";
        }
        if (!ctype_digit($serial))
        {
          $serial = "";
        }

        if ($serial == "")
        {
          $out .= get_notification_html("Invalid serial given", ERROR);
        }
        else
        {
          $permissions = get_user_permissions($serial);
          $custom_permissions = $permissions["customPermissions"];
          $display_name = $permissions["displayName"];
          $permissions = $permissions["access"];
          $user_result = get_user_data($serial);
          $user_name = filter_string_for_html($user_result["data"]["AccountName"]);
          $phpEx = "php";
          $fileinfo = get_cache("fileinfo");
          $fileinfo = explode("\n", $fileinfo);

          foreach ($fileinfo as $files)
          {
            $file = explode("|", $files);
            if (count($file) > 1)
            {
              $permission = $file[3];
              if (!in_array($file[0], $dont_allow))
              {
                if ($permission == "administrator")
                {
                  $file[0] = substr($file[0], 0, strpos($file[0], "."));
                  if (in_array($file[0], $permissions))
                  {
                    $selected = " checked=\"checked\"";
                  }
                  else
                  {
                    $selected = "";
                  }

                  $admin_files .= "<input type=\"checkbox\" name=\"pages[]\" value=\"" . $file[0] . "\"" . $selected . "> - " . $file[2] . "<br/>\n";
                }
                else if ($permission == "restricted")
                {
                  $file[0] = substr($file[0], 0, strpos($file[0], "."));
                  if (in_array($file[0], $permissions))
                  {
                    $selected = " checked=\"checked\"";
                  }
                  else
                  {
                    $selected = "";
                  }

                  $support_files .= "<input type=\"checkbox\" name=\"pages[]\" value=\"" . $file[0] . "\"" . $selected . "> - " . $file[2] . "<br/>\n";
                }
              }
            }
          }
          $out .= "<form class=\"ink-form\" method=\"post\">";
          $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
          $out .= "<tr>";
          $out .= "<td valign=\"top\" class=\"alt2\"><b>Serial</b></td>";
          $out .= "<td valign=\"top\" class=\"alt1\">" . $serial . "</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"top\" class=\"alt2\"><b>Name</b></td>";
          $out .= "<td valign=\"top\" class=\"alt1\">" . $user_name . "</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"top\" class=\"alt2\"><b>Admin Permissions</b></td>";
          $out .= "<td valign=\"top\" class=\"alt2\"><b>Restricted Permissions</b></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"top\" class=\"alt1\">" . $admin_files . "</td>";
          $out .= "<td valign=\"top\" class=\"alt1\">" . $support_files . "</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"middle\" class=\"alt1\">Custom Permissions: &nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"special_permissions\" value=\"" . implode(",", $custom_permissions) . "\"></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"middle\" class=\"alt1\">Display Name: &nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"display_name\" value=\"" . $display_name . "\"></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td valign=\"top\" class=\"alt2\" colspan=\"2\" style=\"text-align: left;\"><input type=\"hidden\" name=\"serial\" value=\"" . $serial . "\"/><input type=\"hidden\" name=\"page\" value=\"edit_update\"/><input class=\"ink-button\" type=\"submit\" name=\"submit\" value=\"Edit User\"/></td>";
          $out .= "</tr>";
          $out .= "</table>";
          $out .= "</form>";
        }
      }
      else
      {
        if ($page == "edit_update")
        {
          $listpages = "";
          if (isset($_POST['serial']))
          {
            $serial = $_POST['serial'];
          }
          else
          {
            $serial = "";
          }
          if (!ctype_digit($serial))
          {
            $serial = "";
          }

          if (isset($_POST['pages']))
          {
            $pages = $_POST['pages'];
            $i = 0;
            while ($i < count($pages))
            {
              if ($i == 0)
              {
                $listpages .= "";
              }
              else
              {
                $listpages .= ",";
              }
              $listpages .= $pages[$i];
              ++$i;
            }
          }

          $custom_permissions = isset($_POST["special_permissions"]) ? antiject($_POST["special_permissions"]) : "";
          $display_name = isset($_POST["display_name"]) ? antiject($_POST["display_name"]) : "";

          if ($serial == "")
          {
            $out .= get_notification_html("Invalid serial given", ERROR);
          }
          else
          {
            $attempt = update_user_permissions($serial, $listpages, $custom_permissions, $display_name);
            if ($attempt["error"] == True)
            {
              $out .= get_notification_html($attempt["errorMessage"], ERROR);
            }
            else
            {
              $out .= get_notification_html("User permissions have been updated!", SUCCESS);
              $redirect = CUSTOM_PAGE_SHORT;
              $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
              gamecp_log(2, $userdata -> username, "SUPER ADMIN - PERMISSIONS - EDITED: User serial {$serial}", 1);
            }
          }
        }
        else
        {
          if ($page == "add")
          {
            $support_files = "";
            $admin_files = "";
            $phpEx = "php";
            $fileinfo = get_cache("fileinfo");
            $fileinfo = explode("\n", $fileinfo);
            foreach ($fileinfo as $files)
            {
              $file = explode("|", $files);
              if (!in_array($file[0], $dont_allow) && count($file) > 1)
              {
                $permission = $file[3];
                if ($permission == "administrator")
                {
                  $file[0] = substr($file[0], 0, strpos($file[0], "."));
                  $admin_files .= "<input type=\"checkbox\" name=\"pages[]\" value=\"" . $file[0] . "\"> - " . $file[2] . "<br/>\n";
                }
                if ($permission == "restricted")
                {
                  $file[0] = substr($file[0], 0, strpos($file[0], "."));
                  $support_files .= "<input type=\"checkbox\" name=\"pages[]\" value=\"" . $file[0] . "\"> - " . $file[2] . "<br/>\n";
                }
              }
            }
            $out .= "<form class=\"ink-form\" method=\"post\">";
            $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
            $out .= "<tr>";
            $out .= "<td class=\"alt2\"><b>Account Serial</b></td>";
            $out .= "<td class=\"alt1\"><input type=\"text\" name=\"serials\" value=\"\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td class=\"alt2\"><i>OR</i> &nbsp;&nbsp;<b>Account Name</b></td>";
            $out .= "<td class=\"alt1\"><input type=\"text\" name=\"name\" value=\"\"/></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td valign=\"top\" class=\"alt2\"><b>Admin Permissions</b></td>";
            $out .= "<td valign=\"top\" class=\"alt2\"><b>Support Permissions</b></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td valign=\"top\" class=\"alt1\">" . $admin_files . "</td>";
            $out .= "<td valign=\"top\" class=\"alt1\">" . $support_files . "</td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td valign=\"middle\" class=\"alt1\">Custom Permissions: &nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"special_permissions\" value=\"\"></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td valign=\"middle\" class=\"alt1\">Display Name: &nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"display_name\" value=\"\"></td>";
            $out .= "</tr>";
            $out .= "<tr>";
            $out .= "<td valign=\"top\" class=\"alt1\" colspan=\"2\" style=\"text-align: center;\"><input type=\"hidden\" name=\"page\" value=\"add_user\"/><input type=\"submit\" name=\"submit\" value=\"Add User\"/></td>";
            $out .= "</tr>";
            $out .= "</table>";
            $out .= "</form>";
          }
          else
          {
            if ($page == "add_user")
            {
              $serial = isset($_POST['serials']) ? $_POST['serials'] : "";
              $name = isset($_POST['name']) ? antiject($_POST['name']) : "";
              $listpages = "";
              $search = "";
              if (isset($_POST['pages']))
              {
                $pages = $_POST['pages'];
                $i = 0;
                while ($i < count($pages))
                {
                  if ($i == 0)
                  {
                    $listpages .= "";
                  }
                  else
                  {
                    $listpages .= ",";
                  }
                  $listpages .= $pages[$i];
                  ++$i;
                }
              }

              $custom_permissions = isset($_POST["special_permissions"]) ? antiject($_POST["special_permissions"]) : "";
              $display_name = isset($_POST["display_name"]) ? antiject($_POST["display_name"]) : "";

              if ($serial == "" && $name == "")
              {
                $out .= get_notification_html("Must Enter Account Name or Serial!", ERROR);
              }
              else
              {
                if ($serial != "")
                {
                  $user_serial = $serial;
                }
                else if ($name != "")
                {
                  $user_serial = get_account_serial($name);
                  $user_serial = $user_serial["serial"];
                }

                if ($user_serial == "" || $user_serial <= 0)
                {
                  $out .= get_notification_html("Could not find user, please try again", ERROR);
                }
                else
                {
                  $test_user = get_user_permissions($serial);
                  if ($test_user["error"] == True)
                  {
                    $out .= get_notification_html($test_user["errorMessage"], ERROR);
                  }
                  else if ($test_user["exists"])
                  {
                    $out .= get_notification_html("Permissions already exist for that user", ERROR);
                  }
                  else
                  {
                    $attempt = add_user_permissions($user_serial, $listpages, $custom_permissions, $display_name);
                    if ($attempt["error"] == True)
                    {
                      $out .= get_notification_html($attempt["errorMessage"], ERROR);
                    }
                    else
                    {
                      $out .= get_notification_html("User permissions have been added!", SUCCESS);
                      $redirect = CUSTOM_PAGE_SHORT;
                      $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
                      gamecp_log(2, $userdata -> username, "SUPER ADMIN - PERMISSIONS - ADDED: User {$user_serial}", 1);
                    }
                  }
                }
              }
            }
            else
            {
              if ($page == "delete")
              {
                if (isset($_POST['serial']))
                {
                  $serial = $_POST['serial'];
                }
                else if (isset($_GET['serial']))
                {
                  $serial = $_GET['serial'];
                }
                else
                {
                  $serial = "";
                }

                if ($serial != "")
                {
                  $user_result = get_user_data($serial);

                  if ($user_result["error"] == True)
                  {
                    $out .= get_notification_html($user_result["errorMessage"], ERROR);
                  }
                  else
                  {
                    $user_name = $user_result["data"]["AccountName"];
                    $out .= "<p style=\"text-align: center; font-weight: bold;\">Are you sure you want the delete the user: <u>" . $user_name . "</u>?</p>";
                    $out .= "<form method=\"post\">";
                    $out .= "<p style=\"text-align: center;\"><input type=\"hidden\" name=\"serial\" value=\"" . $serial . "\"/><input type=\"hidden\" name=\"page\" value=\"delete_user\"/><input type=\"submit\" name=\"yes\" value=\"Yes\"/> <input type=\"submit\" name=\"no\" value=\"No\"/></p>";
                  }
                }
              }
              else
              {
                if ($page == "delete_user")
                {
                  $yes = isset($_POST['yes']) ? "1" : "0";
                  $no = isset($_POST['no']) ? "1" : "0";
                  if (isset($_POST['serial']) && ctype_digit($_POST['serial']))
                  {
                    $serial = antiject($_POST['serial']);
                  }
                  else
                  {
                    $serial = "";
                  }

                  if ($no != 1 && $serial != "")
                  {
                    $attempt = delete_user_permissions($serial);
                    if ($attempt["error"] == True)
                    {
                      $out .= get_notification_html($attempt["errorMessage"], ERROR);
                    }
                    else
                    {
                      $out .= get_notification_html("User permissions have been deleted!", SUCCESS);
                      $redirect = CUSTOM_PAGE_SHORT;
                      $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
                      gamecp_log(2, $userdata -> username, "SUPER ADMIN - PERMISSIONS - DELETED: User {$serial}", 1);
                    }
                  }
                  else
                  {
                    header("Location: {$script_name}?action=" . $_GET['action']);
                  }
                }
                else
                {
                  $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
                  $redirect = INDEX_PAGE_SHORT;
                }
              }
            }
          }
        }
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
