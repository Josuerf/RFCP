<?php

function filter_string_for_html($output)
{
  $output = htmlspecialchars($output);
  return filter_var($output, FILTER_SANITIZE_STRING);
}

function print_json($json)
{
  echo json_encode($json);
}

//Handle all UI output
function print_outputs($data, $redirect = "", $redirect_url = "")
{
  global $title;
  global $vote_page;
  global $gamecp_nav;
  global $lefttitle;
  global $out;
  global $script_name;
  global $page_info;

  $header = "";
  gamecp_nav();

  if (!isset($title))
  {
    $title = "";
  }
  if (!isset($vote_page))
  {
    $vote_page = "";
  }
  if (!isset($gamecp_nav))
  {
    $gamecp_nav = "";
  }
  if (!isset($lefttitle))
  {
    $lefttitle = "";
  }
  if (!isset($out))
  {
    $out = "";
  }
  if (!isset($script_name))
  {
    $script_name = "";
  }
  if (!isset($page_info))
  {
    $page_info = "";
  }

  if ($redirect != "")
  {
    if ($redirect == PREVIOUS_PAGE_SHORT)
    {
      $header .= "<meta http-equiv=\"refresh\" content=\"" . REDIRECT_SHORT_TIME . ";url=" . $_SERVER['HTTP_REFERER'] . "\">";
    }
    else if ($redirect == PREVIOUS_PAGE_LONG)
    {
      $header .= "<meta http-equiv=\"refresh\" content=\"" . REDIRECT_LONG_TIME . ";url=" . $_SERVER['HTTP_REFERER'] . "\">";
    }
    else if ($redirect == CUSTOM_PAGE_SHORT)
    {
      $header .= "<meta http-equiv=\"refresh\" content=\"" . REDIRECT_SHORT_TIME . ";url=" . $redirect_url . "\">";
    }
    else if ($redirect == CUSTOM_PAGE_LONG)
    {
      $header .= "<meta http-equiv=\"refresh\" content=\"" . REDIRECT_LONG_TIME . ";url=" . $redirect_url . "\">";
    }
    else if ($redirect == INDEX_PAGE_SHORT)
    {
      $header .= "<meta http-equiv=\"refresh\" content=\"" . REDIRECT_LONG_TIME . ";url=" . $script_name . "\">";
    }
  }

  unset($mssql);

  $data = str_replace("\$title", $title, $data);
  $data = str_replace("\$vote_page", $vote_page, $data);
  $data = str_replace("\$gamecp_nav", $gamecp_nav, $data);
  $data = str_replace("\$lefttitle", $lefttitle, $data);
  $data = str_replace("\$out", $out, $data);
  $data = str_replace("\$script_name", $script_name, $data);
  $data = str_replace("\$page_info", $page_info, $data);
  $data = str_replace("\$footer", get_footer(), $data);
  $data = str_replace("\$header", $header, $data);

  echo ( $data );
}

//Set the global footer text
function get_footer()
{
  global $userdata;
  global $config;

  $footer = "<div class=\"box ink-" . BASE_COLOR . "\">";
  $footer .= "<div class=\"footer_copy\">Copyright &copy;2014, GMoney & Agony</div>"; //<a href=\"http://www.NXTDeveloper.com\" target=\"_blank\">NXTDeveloper.com</a>
  $footer .= "<div class=\"footer_home\"><a href=\"" . $config['gamecp_filename'] . "\" style=\"padding-left: 15px;\">Home</a>";

  if (isset($userdata) && $userdata -> get_property("loggedin", False))
  {
    $footer .= "<a href=\"rfcp_logout.php\" style=\"padding-left: 25px;\">Logout</a>";
  }

  $footer .= "</div>";

  if (isset($userdata) && $userdata -> get_property("credits", null) != null)
  {
    $footer .= "<div class=\"footer_points\">" . $userdata -> credits . " Novus Credits</div>"; // / " . $userdata->bonus_credits . " Bonus Credits
  }

  $footer .= "</div>";

  return $footer;
}

//extract html from templates
function gamecp_template($template_name)
{
//global $config;
//global $is_superadmin;
  if ($template_name == "")
  {
    return false;
  }
  $contents = "";
  $header = "./framework/header.html";
  $footer = "./framework/footer.html";
  $filename = "./framework/" . $template_name . ".html";
  if (file_exists($filename))
  {
    if (file_exists($header))
    {
      $handle = fopen($header, "rb");
      $contents .= fread($handle, filesize($header)) . "\n";
      unset($handle);
    }
    else
    {
      return "Unable to load the header.html file";
    }
    $handle = fopen($filename, "rb");
    $contents .= fread($handle, filesize($filename)) . "\n";
    unset($handle);
    if (file_exists($footer))
    {
      $handle = fopen($footer, "rb");
      $contents .= fread($handle, filesize($footer));
      unset($handle);
    }
    else
    {
      return "Unable to load the footer.html file";
    }

    return ( $contents );
  }
  return "Invalid load of template";
}

function gamecp_nav()
{
  global $gamecp_nav;
  global $config;
  global $userdata;
  global $script_name;
  global $dont_allow;

  $gamecp_nav = "";
  $gamecp_nav .= "<ul class='menu rounded horizontal " . BASE_COLOR . " shadowed'>" . "\n";
  $gamecp_nav .= "<li><a class='first_nav_link' href='" . $config['gamecp_filename'] . "'>Home</a></li>" . "\n";
  if ($userdata -> loggedin != True)
  {
    $gamecp_nav .= "<li><a href='rfcp_register.php'>Register</a></li>" . "\n";
  }

  $phpExt = "php";
  $navinfo = "";

  if (!($navinfo = get_cache("fileinfo")))
  {
    $dir = @opendir("./modules/");
    $loadingmodules = 1;
    while ($file = @readdir($dir))
    {
      unset($moduleCategory);
      unset($moduleLabel);
      unset($permissions);
      if (!is_dir("./modules/" . $file) && !in_array($file, $dont_allow))
      {
        $this_script = "";
        include( "./modules/" . $file );
        if (isset($moduleCategory) && isset($moduleLabel) && isset($permission) && isset($file))
        {
          $navinfo .= $file . "|" . $moduleCategory . "|" . $moduleLabel . "|" . $permission . "\n";
        }
        unset($module);
        unset($keys);
      }
    }
    @closedir($dir);
    unset($loadingmodules);
    add_cache("fileinfo", $navinfo, 86400);
  }
  $navinfo = explode("\n", $navinfo);
  $modules = array ();

  foreach ($navinfo as $moduleinfo)
  {
    $vars = explode("|", $moduleinfo);
    if (count($vars) > 1)
    {
      $file = $vars[0];
      $cat = $vars[1];
      $label = $vars[2];
      $perm = strtolower($vars[3]);
      switch ($perm)
      {
        case "administrator":
          if ($userdata -> loggedin == True && $userdata -> is_superadmin())
          {
            $modules[$cat][$label] = $file;
          }
          break;
        case "restricted":
          if ($userdata -> loggedin == True && $userdata -> has_permission($file))
          {
            $modules[$cat][$label] = $file;
          }
          break;
        case "player":
          if ($userdata -> loggedin == True)
          {
            $modules[$cat][$label] = $file;
          }
          break;
        case "all":
          $modules[$cat][$label] = $file;
          break;
        default: break;
      }
    }
  }

  $temp_nav = array ();
  foreach ($modules as $category => $pages)
  {
    if ($category != "HIDDEN")
    {
      if (!isset($temp_nav[$category]))
      {
        $temp_nav[$category] = "";
      }

      $temp_nav[$category] .= "<li><a href='#'>" . $category . "</a>";
      $temp_nav[$category] .= "<ul class='submenu'>";

      foreach ($pages as $label => $page)
      {
        $temp_nav[$category] .= "<li><a href='./" . $script_name . "?action=" . substr($page, 0, -4) . "'>" . $label . "</a></li>";
      }

      $temp_nav[$category] .= "</ul></li>";
    }
  }

  foreach ($temp_nav as $category => $submenu)
  {
    $gamecp_nav .= $submenu;
  }

  $gamecp_nav .= "</ul>" . "\n";
}

function get_captcha_html()
{
  $public_key = get_config_item("security_recaptcha_public_key");
  $captcha_html = "<script type=\"text/javascript\" src=\"http://www.google.com/recaptcha/api/challenge?k=" . $public_key . "\"> </script>";
  $captcha_html .= "<noscript>";
  $captcha_html .= "<iframe src=\"http://www.google.com/recaptcha/api/noscript?k=" . $public_key . "\" height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br>";
  $captcha_html .= "<textarea name=\"recaptcha_challenge_field\" rows=\"3\" cols=\"40\"> </textarea>";
  $captcha_html .= "<input type=\"hidden\" name=\"recaptcha_response_field\" value=\"manual_challenge\"></noscript>";

  return $captcha_html;
}

function get_login_html()
{
  global $script_name;
  global $config;

  $return_html = "";

  $return_html .= "<div class='column group'>";
  $return_html .= "<div class='large-100 push-center'>";
  $return_html .= "<center><strong>Welcome! Please login to the RF CP with your game account info.</strong></center>";
  $return_html .= "</div>";
  $return_html .= "<br />";
  $return_html .= "<br />";

  $return_html .= "<div class='large-50 push-center'>";

  $return_html .= "<center>";
  $return_html .= "<form id=\"login_form\" method=\"post\" action=\"rfcp_login.php\" class=\"ink-form login_form\">";
  $return_html .= "<table border=\"0\">";
  $return_html .= "<tr>";
  $return_html .= "<td class=\"form_labels\">Username:</td>";
  $return_html .= "<td><input type=\"text\" name=\"username\"></td>";
  $return_html .= "</tr>";
  $return_html .= "<tr>";
  $return_html .= "<td class=\"form_labels\">Password:</td>";
  $return_html .= "<td><input type=\"password\" name=\"password\"></td>";
  $return_html .= "</tr>";
  if (isset($config['security_recaptcha_enable']) && $config['security_recaptcha_enable'] == 1)
  {
    $return_html .= "\t<tr>" . "\n";
    $return_html .= "\t\t<td colspan=\"2\">" . get_captcha_html() . "</td>";
    $return_html .= "\t</tr>" . "\n";
  }
  $return_html .= "<tr>";
  $return_html .= "<td class='form_submit' colspan=2><input type=\"submit\" value=\"Login\" class=\"ink-button\"><button class='ink-button' type=\"button\" onclick=\"window.location.href='rfcp_register.php'\" style=\"margin-right: 0px;\">Register</button></td>";
  $return_html .= "</tr>";
  $return_html .= "</table>";
  $return_html .= "</form>";
  $return_html .= "<br>";
  $return_html .= "<a href=\"" . $script_name . "?action=user_passwordrecover\">Forgot Password?</a>";

  $return_html .= "<center>";
  $return_html .= "</div>";
  $return_html .= "</div>";

  return $return_html;
}

//can be array of messages, or single message
function get_notification_html($message, $type)
{
  $html_return = "<div class='ink-alert basic " . $type . "'>";

  if (is_array($message))
  {
    foreach ($message as $key => $text)
    {
      $html_return .= "<p>" . $text . "</p>";
    }
  }
  else
  {
    $html_return .= "<p>" . $message . "</p>";
  }

  $html_return .= "</div>";
  return $html_return;
}

// pills for top of each module
function generate_module_nav_html($links, $active_index)
{
  $return = "<nav class=\"ink-navigation push-center module_nav\">";
  $return .= "<ul class=\"pills shadowed rounded\">";
  foreach ($links as $index => $link)
  {
    $classes = "";
    if ($active_index == $index)
    {
      $classes .= " active";
    }

    $return .= "<li class=\"" . $classes . "\">" . $link . "</li>";
  }
  $return .= "</ul>";
  $return .= "</nav>";

  return $return;
}

// Market Menu
function generate_menu($menu_array, $parent, $options = "", $prepend = array (), $item_cat_id = 0)
{
  global $options;
  $prepend[0] = "";
  foreach ($menu_array as $key => $value)
  {
    if ($value['parent'] == $parent)
    {
      $prepend[$value['id']] = $prepend[$parent] . "&nbsp;&nbsp;";
      if ($parent == 0)
      {
        $add_bg = "style=\"background-color: #E7E7E7; font-weight: bold;\"";
      }
      else
      {
        $add_bg = "style=\"background-color: #FFFFFF;\"";
      }
      if ($value['id'] == $item_cat_id)
      {
        $selected = " selected=\"selected\"";
      }
      else
      {
        $selected = "";
      }
      $options .= "<option value=\"" . $value['id'] . "\"" . $add_bg . $selected . " label=\"" . $value['label'] . "\">" . $prepend[$parent] . $value['label'] . "</option>";
      generate_menu($menu_array, $key, $options, $prepend, $item_cat_id);
    }
  }
}

//cart viewer
function get_cart_info_html()
{
  $return = "";

  $return .= "<div class=\"cart_info\">";
  $return .= "<span class=\"simpleCart_quantity\"></span> items";
  $return .= "</div>";
  $return .= "<div id=\"cartPopover\" class=\"ink-blue\" style=\"display:none;\">";
  $return .= "<div id=\"cart_error\" class=\"ink-alert basic error\" style=\"display:none;\"></div>";
  $return .= "<div id=\"triangle\">&#x25B2;</div>";
  $return .= "<div class=\"simpleCart_items\"></div>";
  $return .= "<div id=\"cartData\" class=\"clearfix\">";
  $return .= "<div class=\"left\"><strong>Items: </strong><span class=\"simpleCart_quantity\"></span></div>";
  $return .= "<div class=\"right\"><strong>Total: </strong><span class=\"simpleCart_total\"></span></div>";
  $return .= "</div>";
  $return .= get_character_select_menu();
  $return .= "<hr><div id=\"popoverButtons\" class=\"clearfix\">";
  $return .= "<a href=\"#\" onclick=\"simpleCart.empty(); return false;\" class=\"ink-button left\">Empty</a>";
  $return .= "<a href=\"#\" class=\"ink-button primary right cart_confirm_button\" onclick=\"check_items(); return false;\">Confirm Items</a>";
  $return .= "<a href=\"javascript:;\" class=\"simpleCart_checkout ink-button primary right cart_checkout_button\" style=\"display:none;\">Checkout</a>";
  $return .= "</div>";
  $return .= "</div>";

  return $return;
}

function get_character_select_menu()
{
  global $userdata;
  $return = "";
  $characters = $userdata -> get_character_list_short();

  if (count($characters) > 0)
  {
    $return .= "<div class=\"ink-form char_select\">Select A Character: <select id=\"character_select\">";

    foreach ($characters as $key => $char)
    {
      $return .= "<option value=\"" . $char["Serial"] . "_" . $char["Race"] . "\">";
      $return .= "Level " . $char["Lv"] . " - " . filter_string_for_html($char['Name']);
      $return .= "</option>";
    }

    $return .= "</select></div>";
  }
  else
  {
    $return .= "No characters found.";
  }

  return $return;
}

function get_bonus_character_select_menu()
{
  global $userdata;
  $return = "";
  $characters = $userdata -> get_character_list_short();

  if (count($characters) > 0)
  {
    $return .= "<div class=\"ink-form char_select\">Select a character for item bonus(es): <select id=\"character_select\" name=\"character_select\">";

    foreach ($characters as $key => $char)
    {
      $return .= "<option value=\"" . $char["Serial"] . "_" . $char["Race"] . "\">";
      $return .= "Level " . $char["Lv"] . " - " . filter_string_for_html($char['Name']);
      $return .= "</option>";
    }

    $return .= "</select></div>";
  }
  else
  {
    $return .= "No characters found.";
  }
  return $return;
}

function generate_category_html($current_categories, $guild = false)
{
  $return = "";
  $i = 0;
  $sub_name = "&nbsp;";
  global $script_name;

  while ($i < count($current_categories))
  {
    if ($i == 0)
    {
      $return .= "<div class=\"ink-row category_row\">";
    }
    else if ($i % 3 == 0)
    {
      $return .= "</div><div class=\"ink-row\">";
    }

    $sub_cat_id = $current_categories[$i]["id"];
    $attempt = get_market_categories($sub_cat_id = $current_categories[$i]["id"], -1, $guild);
    $sub_name = "";
    foreach ($attempt["categories_in_order"] as $key => $sub_cat)
    {
      if ($key != 0)
      {
        $sub_name .= "&nbsp;&nbsp;&nbsp;";
      }
      else if ($key == 0)
      {
        $sub_name .= "<span class=\"sub_categories\" style=\"font-size: 10pt; display:block;\">";
      }
      $sub_name .= "<a href=\"" . $script_name . "?action=" . $_GET["action"] . "&cat_id=" . $sub_cat["id"] . "\">" . $sub_cat["label"] . "</a>";
    }

    if ($sub_name != "")
    {
      $sub_name .= "</span>";
    }

    $return .= "<div class=\"box large-30 medium-30 small-30 category_box ink-" . BASE_COLOR . "\">";
    $return .= "<div class=\"category_text\"><a class=\"category_link\" href=\"" . $script_name . "?action=" . $_GET['action'] . "&cat_id=" . $current_categories[$i]['id'] . "\" >" . $current_categories[$i]['label'] . "</a>" . $sub_name . "</div>";
    $return .= "</div>";
    $sub_name = "&nbsp;";
    ++$i;
  }
  if ($i > 0)
  {
    $return .= "</div>";
    $return .= "<script>normalize_category_height();</script>";
  }


  return $return;
}

function get_breadcrumb_html($links)
{
  $return = "<nav class=\"ink-navigation cp_breadcrumbs\">";
  $return .= "<ul class=\"breadcrumbs\">";

  foreach ($links as $key => $link)
  {
    if ($key < count($links) - 1)
    {
      $return .= "<li>" . $link . "</li>";
    }
    else
    {
      $return .= "<li class=\"active\">" . $link . "</li>";
    }
  }

  $return .= "</ul>";
  $return .= "</nav>";

  return $return;
}

function generate_upgrade_input_html($item, $talic_limits = array (0, 1, 2, 3, 4,
  5, 6, 7, 8, 9, 10, 11, 12, 15))
{
//Item should already be parsed
  $talic_limits_js = "";
  if (isset($talic_limits))
  {
    $talic_limits_js = "talicsToDisplay = [";
    foreach ($talic_limits as $index => $talic)
    {
      $talic_limits_js .= $talic;
      if ($index != count($talic_limits) - 1)
      {
        $talic_limits_js .= ",";
      }
    }
    if (!in_array(15, $talic_limits))
    {
      $talic_limits_js .= ",15";
    }
    $talic_limits_js .= "];";
  }

  $return = "";
  $return .= "<div class=\"item_talics\">";
  $return .= "<div style=\"vertical-align:middle; display: inline;\">";

  if (isset($item["slots"]))
  {
    $return .= generate_slot_select_html($item["slots"]);
  }
  else
  {
    $return .= generate_slot_select_html();
  }

  $return .= "</div>";
  if (isset($item["talics"]))
  {
    foreach ($item["talics"] as $key => $talic_id)
    {
      $return .= generate_talic_select_html($talic_id, $talic_limits);
    }
  }
  $return .= "</div>";
  $return .= "<script type=\"text/javascript\">";
  $return .= $talic_limits_js;
  $return .= "jQuery(function(){jQuery(\".item_talics\").find(\".talic\").msDropdown({on:{contextmenu:onTalicContextMenu}});});";
  $return .= "</script>";
  return $return;
}

function generate_upgrade_multi_input_html($item, $talic_limits = array (0, 1, 2,
  3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15))
{
//Item should already be parsed
  $talic_limits_js = "";
  if (isset($talic_limits))
  {
    $talic_limits_js = "talicsToDisplay = [";
    foreach ($talic_limits as $index => $talic)
    {
      $talic_limits_js .= $talic;
      if ($index != count($talic_limits) - 1)
      {
        $talic_limits_js .= ",";
      }
    }
    if (!in_array(15, $talic_limits))
    {
      $talic_limits_js .= ",15";
    }
    $talic_limits_js .= "];";
  }

  $return = "";
  $return .= "<div class=\"item_talics\">";
  $return .= "<div style=\"vertical-align:middle; display: inline;\">";

  if (isset($item["slots"]))
  {
    $return .= generate_slot_select_html($item["slots"], True);
  }
  else
  {
    $return .= generate_slot_select_html(0, True);
  }

  $return .= "</div>";
  if (isset($item["talics"]))
  {
    foreach ($item["talics"] as $key => $talic_id)
    {
      $return .= generate_talic_select_html($talic_id, $talic_limits, True);
    }
  }
  $return .= "</div>";
  $return .= "<script type=\"text/javascript\">";
  $return .= $talic_limits_js;
  $return .= "multiTalicSelectors = true; ";
  $return .= "jQuery(function(){jQuery(\".item_talics\").find(\".talic\").msDropdown({on:{contextmenu:onTalicContextMenu}});});";
  $return .= "</script>";
  return $return;
}

function generate_slot_select_html($slots = 0, $multi = False)
{
  $extra = $multi ? "[]" : "";
  $return = "<select class=\"item_slots\" name=\"item_slots" . $extra . "\" onchange=\"onSlotsChanged(this);\">";
  $i = 0;
  while ($i < 8)
  {
    $selected = "";
    if ($slots == $i)
    {
      $selected = " selected=selected";
    }
    $return .= "<option value=\"" . $i . "\"" . $selected . ">" . $i . "</option>";
    $i++;
  }
  $return .= "</select>";
  return $return;
}

function generate_talic_select_html($talicid, $talic_limits, $multi = False)
{
  $extra = $multi ? "[0][]" : "[]";
  $return = "<select class=\"talic\" name=\"talics" . $extra . "\" style=\"width:18px;\" onchange=\"onTalicChange(this);\">";
  $i = 0;
  while ($i < 16)
  {
    if ($i != 14)
    {
      if (isset($talic_limits) && in_array($i, $talic_limits))
      {
        $talic_name = get_talic_name($i);
        $selected = "";
        if ($i == $talicid)
        {
          $selected = " selected=selected";
        }
        $return .= "<option value=\"" . $i . "\" data-image=\"./images/talics/t-" . sprintf("%02d", $i) . "\" data-title=\"" . $talic_name . "\"" . $selected . "></option>";
      }
    }
    $i++;
  }
  $return .= "</select>";
  return $return;
}

function get_talic_name($id)
{
  switch ($id)
  {
    case 0: return "Ignorant";
    case 1: return "Destruction";
    case 2: return "Darkness";
    case 3: return "Chaos";
    case 4: return "Hatred";
    case 5: return "Favor";
    case 6: return "Wisdom";
    case 7: return "Sacred Flame";
    case 8: return "Belief";
    case 9: return "Guard";
    case 10: return "Glory";
    case 11: return "Grace";
    case 12: return "Mercy";
    case 13: return "Rebirth";
    case 15: return "No Talic";
    default: return "";
  }
}

function get_armor_set_options_html()
{
  global $armor_set_levels;
  $return = "";

  foreach ($armor_set_levels as $key => $level)
  {
    $return .= "<option value=\"{$level}\">{$level}</option>";
  }

  return $return;
}

function get_weapon_set_options_html()
{
  global $weapon_set_levels;
  $return = "";

  foreach ($weapon_set_levels as $key => $level)
  {
    $return .= "<option value=\"{$level}\">{$level}</option>";
  }
  return $return;
}

function get_base_class_options_html($current_base)
{
  $return = "";
  $options = array (0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150);
  $labels = array ("Warrior", "Ranger", "Spiritualist", "Specialist", "Warrior",
    "Ranger",
    "Spiritualist", "Specialist", "Warrior", "Ranger", "Specialist");

  foreach ($options as $key => $value)
  {
    if ($current_base != $value)
    {
      if ($current_base < 46 && $value < 46)
      {
        $return .= "<option value=\"{$value}\">{$labels[$key]}</option>";
      }
      else if ($current_base > 46 && $value > 46 && $current_base < 106 && $value < 106)
      {
        $return .= "<option value=\"{$value}\">{$labels[$key]}</option>";
      }
      else if ($current_base > 106 && $value > 106)
      {
        $return .= "<option value=\"{$value}\">{$labels[$key]}</option>";
      }
    }
  }

  return $return;
}

function get_currency_slider($id, $current_amount, $currency_per_nc, $label, $current_credits)
{
  global $userdata;
  $max = MAX_CURRENCY - $current_amount;
  $max = $max - ($max % $currency_per_nc);
  $max_nc = $max / $currency_per_nc;
  if ($max_nc > $current_credits)
  {
    $max_nc = $current_credits;
  }
  $return = "";

  $return .= "<input id=\"nc_to_convert_" . $id . "\" type=\"hidden\" name=\"nc_to_convert\" value=\"0\">";
  $return .= "<div id=\"nc_currency_slider_" . $id . "\" style=\"width: 400px; display: inline-block; margin-right: 15px;\"></div>";
  $return .= "<div id=\"nc_to_convert_display_" . $id . "\" style=\"display: inline-block;\">" . number_format($current_amount) . " " . $label . "</div>&nbsp;";
  $return .= "<div id=\"nc_to_convert_added_" . $id . "\" style=\"display: inline-block; color: green; font-weight: bold;\"> (+0)</div>";
  $return .= "\n<script type=\"text/javascript\">$(function() { ";
  $return .= "\n$('#nc_currency_slider_" . $id . "').slider({ ";
  $return .= "\nrange: 'min', ";
  $return .= "\nvalue: 0, ";
  $return .= "\nmin: 0, ";
  $return .= "\nmax: " . $max_nc . ", ";
  $return .= "\nslide: function (event, ui){  ";
  $return .= "\n$('#nc_to_convert_" . $id . "').val(ui.value); ";
  $return .= "\n$('#nc_to_convert_display_" . $id . "').html(numberWithCommas(ui.value * " . $currency_per_nc . " + " . $current_amount . ") + ' ' + '" . $label . "'); ";
  $return .= "\n$('#nc_to_convert_added_" . $id . "').html(' (+' + numberWithCommas(ui.value * " . $currency_per_nc . ") + ')'); ";
  $return .= "\n$('#currency_button_" . $id . "').val('Submit (' + ui.value + ' NC)'); ";
  $return .= "\n} ";
  $return .= "\n}); ";
  $return .= "\n}); </script>";

  return $return;
}

function get_paypal_html($price, $total_points, $serial)
{
  global $config;

  $form = "";
  $form .= "<input type='hidden' name='cmd' value='_xclick'>";
  $form .= "<input type='hidden' name='business' value='" . $config['paypal_email'] . "'>";
  $form .= "<input type='hidden' name='custom' value='" . $serial . "'>";
  $form .= "<input type='hidden' name='item_name' value='" . $total_points . " Donation Rank points'>";
  $form .= "<input type='hidden' name='item_number' value='" . $total_points . " Donation Rank points'>";
  $form .= "<input type='hidden' name='amount' value='" . $price . "'>";
  $form .= "<input type='hidden' name='no_shipping' value='1'>";
  $form .= "<input type='hidden' name='no_note' value='1'>";
  $form .= "<input type='hidden' name='currency_code' value='USD'>";
  $form .= "<input type='hidden' name='notify_url' value='" . $config['paypal_ipn_url'] . "'>";
  $form .= "<input type='hidden' name='return' value='" . $config['paypal_return_url'] . "'>";
  $form .= "<input type='hidden' name='cancel_return' value='" . $config['paypal_return_url'] . "'>";
  $form .= "<input class=\"ink-button\" type='submit' border='0' class='button' name='submit' value='Donate Now'>";

  return $form;
}

function get_vote_html()
{
  global $userdata;
  $html = "";
  $username = "";
  $attempt = get_vote_sites();
  $rows = $attempt["rows"];

  if ($userdata -> username != "")
  {
    $username = $userdata -> username;
  }

  if (count($rows) > 0)
  {
    $html .= "<div class=\"box vote_wrapper ink-form\">";
    $html .= "<div class=\"vote_header\">";
    $html .= "Vote For Us";
    $html .= "</div>";
    $html .= "<div class=\"vote_sub_header\">";
    $html .= "By voting for us you are helping to spread the word about our community, as a small token of our appreciation we are offering (<b>NC</b>) for each successful vote.";
    $html .= "</div>";
  }

  foreach ($rows as $key => $vote)
  {
    $html .= "<a href=\"javascript:do_vote('" . $vote['vote_id'] . "','" . $vote['vote_site_name'] . "','" . $vote['vote_site_url'] . "')\" ";
    $html .= "title=\"" . $vote['vote_site_name'] . "\"><img src=\"" . $vote['vote_site_image'] . "\" alt=\"" . $vote['vote_site_name'] . "\" border=\"0\" style=\"padding-right: 15px;\"></a>";
  }

  if (count($rows) > 0)
  {
    $html .= "<div id=\"vote_activity\" style=\"display:none; padding-top: 5px;\"></div>";
    $html .= "</div>";
    $html .= "<script type=\"text/javascript\">var current_username = '" . filter_string_for_html($username) . "';</script>";
  }

  return $html;
}
