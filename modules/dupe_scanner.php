<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Dupe Scanner";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $table = "CORA_DUPES";

    $links = array ();
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "'>Cora</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&page=bell'>Bellato</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&page=acc'>Accretian</a>";

    if ($page == "")
    {
      $active_index = 0;
    }
    else if ($page == "bell")
    {
      $active_index = 1;
    }
    else if ($page == "acc")
    {
      $active_index = 2;
    }

    $out .= generate_module_nav_html($links, $active_index);

    if ($page == "bell")
    {
      $table = "BELL_DUPES";
    }
    else if ($page == "acc")
    {
      $table = "ACC_DUPES";
    }


    if (empty($page) || $page == "bell" || $page == "acc")
    {
      $sql = "Select * from " . $table . " ORDER BY s_value DESC";
      $dupe_result = sqlsrv_query(connectdb(HISTORY), $sql);

      $out .= "<table style=\"width: 100%\">";
      $out .= "<thead style=\"text-align:left;\"><tr>";
      $out .= "<th>Char Serial</th>";
      $out .= "<th>Char Name</th>";
      $out .= "<th>Item</th>";
      $out .= "<th>QTY</th>";
      $out .= "<th>Serial</th>";
      $out .= "<th>S Column</th>";
      $out .= "<th>Source Table</th>";
      $out .= "</tr></thead>";

      $current_serial = -1;
      $new_serial = -1;
      while ($row = sqlsrv_fetch_array($dupe_result))
      {
        $style = "";
        $k_value = $row["k_value"];
        $u_value = $row["u_value"];
        $item_details = get_item_details($k_value, $u_value);
        $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);

        $new_serial = $row["s_value"];
        if ($new_serial != $current_serial)
        {
          $style = "border-top: 1px solid #000;";
          $current_serial = $new_serial;
        }

        $out .= "<tr style=\"" . $style . " height: 32px; vertical-align:bottom;\">";
        $out .= "<td>" . $row["serial"] . "</td>";
        $out .= "<td>" . $row["char_name"] . "</td>";
        $out .= "<td>" . $item_info["item"]["item_name"] . "(" . $item_info["item"]["item_code"] . ") " . $item_details["images"] . "</td>";
        $out .= "<td>" . $row["d_value"] . "</td>";
        $out .= "<td>" . $new_serial . "</td>";
        $out .= "<td>" . $row["s_column"] . "</td>";
        $out .= "<td>" . $row["table"] . "</td>";
        $out .= "</tr>";
      }

      $out .= "</table>";
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
