<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Search Inventory";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $search_fun = isset($_GET['search_fun']) ? $_GET['search_fun'] : "";
    $page_gen = isset($_GET['page_gen']) && ctype_digit($_GET['page_gen']) ? intval($_GET['page_gen']) : 1;
    $item_slots = isset($_GET['item_slots']) && ctype_digit($_GET['item_slots']) ? antiject($_GET['item_slots']) : 0;
    $item_upgrades = isset($_GET['talics']) ? $_GET['talics'] : array ();
    $item_id = isset($_GET['item_id']) ? antiject($_GET['item_id']) : "";
    $item_amount_max = isset($_GET['item_amount_max']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_max']) : 0;
    $item_amount_min = isset($_GET['item_amount_min']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_min']) : 0;
    $character_name = isset($_GET['character_name']) ? antiject($_GET['character_name']) : "";
    $character_serial = isset($_GET['character_serial']) && ctype_digit($_GET['character_serial']) ? intval($_GET['character_serial']) : 0;
    $enable_exit = false;
    $search_query = "";
    $top_limit = 5;
    $max_pages = 10;
    $num_of_bags = 100;
    $query_p2 = "";
    $enable_itemsearch = false;
    $item_upgrade = 268435455;
    if (empty($page))
    {
      $out .= "<form class=\"ink-form\" method=\"GET\" action=\"" . $script_name . "?action=" . $_GET['action'] . "\">";
      $out .= "<table class=\"tborder\" cellpadding=\"2\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td class=\"thead\" colspan=\"2\" style=\"padding: 4px;\"><b>Search for an Item</b></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Charcter Serial:</td>";
      $out .= "<td><input type=\"text\" name=\"character_serial\" /></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Charcter Name:</td>";
      $out .= "<td><input type=\"text\" name=\"character_name\" /></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Item Code:</td>";
      $out .= "<td><input type=\"text\" name=\"item_id\" /></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Upgrades:</td>";
      $out .= "<td>";
      $out .= generate_upgrade_input_html(array ());
      $out .= "</td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\"><input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\" /><input class=\"ink-button\" type=\"submit\" value=\"Search\" name=\"search_fun\" /></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $messages = array ();
      if ($search_fun != "")
      {
        $out .= "<br/><br/>";
        if ($item_id == "" && $character_name == "" && $character_serial == 0 && $item_slots == 0)
        {
          $messages[] = "Sorry, make sure you filled in either the item id or character name";
        }

        if (count($messages) == 0)
        {
          $bag_numbers = "";
          $i = 0;
          while ($i < 100)
          {
            $bag_numbers .= ", k{$i}, u{$i}, d{$i}";
            ++$i;
          }
          if ($character_name != "" || $character_serial != 0 || $item_id != "" || ($item_slots > 0))
          {
            $search_query .= " WHERE ";
          }
          if ($character_name != "" || $character_serial != 0)
          {
            if ($character_serial != 0)
            {
              $search_query .= "Serial = '{$character_serial}'";
            }
            else
            {
              $character_name = str_replace(";\$", "", $character_name);
              $character_name = str_replace("\\\\", "", $character_name);
              $character_name = antiject($character_name);

              $attempt = get_character_info($character_name);
              if ($attempt["error"] == False)
              {
                $char_serial = $attempt["char"]["Serial"];
              }
              else
              {
                $char_serial = -1;
              }

              $search_query .= "Serial = '{$char_serial}'";
            }
          }
          if ($item_id != "")
          {

            $item_id = str_replace(" ", "", $item_id);
            $item_id = antiject($item_id);

            $attempt = get_item_info_from_code($item_id);
            if ($attempt["error"] == True)
            {
              $messages[] = $attempt["errorMessage"];
            }
            else
            {
              $item_kind = parse_item_code($item_id);
              $item_db_id = $attempt['item']["item_id"];

              $search_query .= "(";
              $item_id_start = ceil(65536 * $item_db_id + ( $item_kind * 256 + 0 ));
              $item_id_end = ceil(65536 * $item_db_id + ( $item_kind * 256 + 255 ));
              $i = 0;
              while ($i < 100)
              {
                if ($i != 0)
                {
                  $search_query .= " OR ";
                }
                if ($item_amount_min != 0)
                {
                  if ($item_amount_max == 0)
                  {
                    $item_amount_max = $item_amount_min;
                  }
                  $append_amount = " AND d{$i} >= {$item_amount_min} AND d{$i} <= {$item_amount_max}";
                }
                else
                {
                  $append_amount = "";
                }
                $search_query .= "k{$i} >= {$item_id_start} AND k{$i} <= {$item_id_end}" . $append_amount;
                ++$i;
              }
              $search_query .= ")";
            }
            $enable_itemsearch = true;
          }

          if (count($item_upgrades) > 0)
          {
            $empty = array ();
            $item_ups = 0;
            foreach ($item_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if ($temp < 0 || $temp == 14 || $temp > 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if ($temp > 0 && $temp < 14 && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }
              else if ($temp > 0 && $temp < 14)
              {
                $item_ups++;
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }

            if ($item_slots < $item_ups)
            {
              $item_ups = $item_slots;
            }
            if ($item_slots > 7)
            {
              $item_slots = 7;
            }
            if (count($item_upgrades) < 7)
            {
              $i = count($item_upgrades);
              while ($i < 7)
              {
                $item_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $item_slots . "";
            foreach (array_reverse($item_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);

            if ($character_name != "" || $character_serial != 0 || $enable_itemsearch)
            {
              $search_query .= " AND ";
            }
            $search_query .= "(";

            for ($i = 0; $i < 100; $i++)
            {
              if ($i != 0)
              {
                $search_query .= " OR ";
              }
              if (!$enable_itemsearch)
              {
                $append_k = " AND k{$i} != '-1'";
              }
              else
              {
                $append_k = "";
              }

              $search_query .= "u{$i} = {$item_upgrade}" . $append_k;
            }

            $search_query .= ")";
            $enable_itemsearch = true;
          }

          $item_id = antiject($item_id);

          include( "./core/pagination.php" );
          $sql = "SELECT Serial {$bag_numbers} FROM tbl_inven {$search_query}";
          $sql .= " AND Serial NOT IN ( SELECT TOP [OFFSET] Serial FROM tbl_inven " . $search_query;
          $sql .= " ORDER BY Serial DESC) ORDER BY Serial DESC";
          $sql_count = "SELECT count(Serial) FROM tbl_inven {$search_query}";

          $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
          $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

          $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size = 20, $links_to_show = 11);
          $results = $pager -> get_data();
          $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
          foreach ($results["rows"] as $key => $row)
          {
            $attempt = get_character_info($row["Serial"]);
            $accountserial = $attempt["char"]['AccountSerial'];
            $name = $attempt["char"]['Name'];

            if ($name[0] != "*")
            {
              $out .= "<tr>";
              $out .= "<td class=\"thead\" colspan=\"5\" style=\"border-bottom-width:2px; border-bottom-color: #000;\">&nbsp;</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td colspan=\"5\" style=\" border-bottom: 0px !important;\"><span style=\"font-weight: bold;\">Character Serial:</span> " . $row['Serial'];
              $out .= "<br/><span style=\"font-weight: bold;\">Character Name:</span> " . $name;
              $out .= "<br/><span style=\"font-weight: bold;\">Account Serial:</span> " . $accountserial . "</td>";
              $out .= "</tr>";
              $out .= "<tr>";
              $out .= "<td class=\"thead\" nowrap>Slot #</td>";
              $out .= "<td class=\"thead\" nowrap>Item Code</td>";
              $out .= "<td class=\"thead\" nowrap>Item Name</td>";
              $out .= "<td class=\"thead\" nowrap>Amount</td>";
              $out .= "<td class=\"thead\" nowrap>Upgrades</td>";
              $out .= "</tr>";
              $i = 0;

              for ($i = 0; $i < $num_of_bags; $i++)
              {
                $k_value = $row["k{$i}"];
                $u_value = $row["u{$i}"];

                $item_details = array ();

                if (-1 == $k_value)
                {
                  $item_id = $k_value;
                  $item_code = "-";
                  $images = "";
                  $item_image = "";
                }
                else
                {
                  $item_details = get_item_details($k_value, $u_value);
                  $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                  $item_code = $item_info["item"]["item_code"];
                  $item_id = $item_info["item"]["item_name"];
                  $images = $item_details["images"];
                  $icon_id = $item_info["item"]["item_icon_id"];
                  $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                  if (file_exists(@$item_path[0]))
                  {
                    $item_image = "<img id=\"\" class=\"market_item_image\" src=\"" . $item_path[0] . "\" style=\"height: 40px; width: 40px;\">";
                  }
                  else
                  {
                    $item_image = "";
                  }
                }

                if ($search_fun != "" && $enable_itemsearch && $item_id != -1)
                {
                  if ($item_code == strtolower($_GET['item_id']))
                  {
                    $out .= "<tr>";
                    $out .= "<td nowrap>" . $i . "</td>";
                    $out .= "<td nowrap>" . $item_code . "</td>";
                    $out .= "<td nowrap>" . $item_image . " " . $item_id . "</td>";
                    $out .= "<td nowrap>" . $row["d{$i}"] . "</td>";
                    $out .= "<td nowrap>" . $images . "</td>";
                    $out .= "</tr>";
                  }
                  else
                  {
                    if ($u_value == $item_upgrade && $item_slots > 0)
                    {
                      $out .= "<tr>";
                      $out .= "<td nowrap>" . $i . "</td>";
                      $out .= "<td nowrap>" . $item_code . "</td>";
                      $out .= "<td style=\"\" nowrap>" . $item_image . " " . $item_id . "</td>";
                      $out .= "<td nowrap>" . $row["d{$i}"] . "</td>";
                      $out .= "<td style=\"\" nowrap>" . $images . "</td>";
                      $out .= "</tr>";
                    }
                  }
                }
                else if ($item_id != -1)
                {
                  $bgcolor = "";
                  $out .= "<tr>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $i . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $item_code . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $item_image . " " . $item_id . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row["d{$i}"] . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $images . "</td>";
                  $out .= "</tr>";
                }
              }
            }
          }

          if (count($results["rows"]) <= 0)
          {
            $out .= "<tr>";
            $out .= "<td colspan=\"5\" style=\"text-align: center;\">No such user/item found in the database</td>";
            $out .= "</tr>";
          }
          else
          {
            $out .= "<tr>";
            $out .= "<td colspan=\"5\" style=\"text-align: center;\">" . $pager -> renderFullNav() . "</td>";
            $out .= "</tr>";
          }
          $out .= "</table>";

          if (!isset($item_idq))
          {
            $item_idq = "";
          }
          gamecp_log(0, $userdata -> username, "ADMIN - ITEM SEARCH - Searched for: {$character_name} or {$item_idq}", 1);
        }

        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = INDEX_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}