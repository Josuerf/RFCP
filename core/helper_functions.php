<?php

// We want to define some getters and setters so that we can avoid using "global" allover the place.

/*
 * 	Config Helper Functions
 */
function get_config_item($name, $default_value = "")
{
  global $config;
  return isset($config[$name]) ? $config[$name] : $default_value;
}

function set_config_item($name, $value)
{
  global $config;
  $config[$name] = $value;
}

function is_config_item_set($name)
{
  global $config;
  return (isset($config) && isset($config[$name]));
}

function unset_cookies()
{
  if (isset($_COOKIE['rfcp_session']))
  {
    terminate_session($_COOKIE['rfcp_session']);
  }

  unset($_COOKIE['rfcp_username']);
  unset($_COOKIE['rfcp_session']);
  setcookie('rfcp_username', null, -1);
  setcookie('rfcp_session', null, -1);
}

function exit_with_error($message)
{
  global $notuser;
  global $isuser;
  global $is_superadmin;
  $_SESSION = array ();
  unset_cookies();
  $notuser = true;
  $isuser = false;
  $is_superadmin = false;
  exit($message);
}

function exit_without_error()
{
  global $notuser;
  global $isuser;
  global $is_superadmin;
  $_SESSION = array ();
  unset_cookies();
  $notuser = true;
  $isuser = false;
  $is_superadmin = false;
}

function translation($str)
{
  return $str;
}

function get_url()
{
  $pageURL = 'http';
  if ($_SERVER["HTTPS"] == "on")
  {
    $pageURL .= "s";
  }
  $pageURL .= "://";
  if ($_SERVER["SERVER_PORT"] != "80")
  {
    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
  }
  else
  {
    $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
  }
  return $pageURL;
}

function is_email($email)
{
  if (!filter_var($email, FILTER_VALIDATE_EMAIL))
  {
    return False;
  }
  return True;
}

function sendEmail($to_email, $subject, $message)
{
  global $config;
  if (isset($config['gamecp_smtp_enable']) && $config['gamecp_smtp_enable'] == 1)
  {
    $mail = new PHPMailer( );
    $mail -> IsSMTP();
    $mail -> SetLanguage("en", "./includes/");
    $mail -> SMTPAuth = true;
    if ($config['gamecp_smtp_enable_ssl'] == 1)
    {
      $mail -> SMTPSecure = "ssl";
    }
    $mail -> Host = $config['gamecp_smtp_server'];
    $mail -> Port = $config['gamecp_smtp_port'];
    $mail -> Username = $config['gamecp_smtp_username'];
    $mail -> Password = $config['gamecp_smtp_password'];
    $mail -> From = $config['lostpass_email'];
    $mail -> FromName = $config['server_name'];
    $mail -> Subject = $subject;
    $mail -> Body = $message;
    $mail -> AddAddress($to_email, $subject);
    $result = $mail -> Send();
    return $result;
  }
  return null;
}

function sendBulkEmail($email_list, $subject, $message, $plain_message = "")
{
  global $config;
  $salt = $config['security_salt'];
  $failed = array ();
  set_time_limit(600);
  if (isset($config['gamecp_smtp_enable']) && $config['gamecp_smtp_enable'] == 1)
  {
    $mail = new PHPMailer( );
    $mail -> IsSMTP();
    $mail -> SetLanguage("en", "./includes/");
    $mail -> SMTPAuth = true;
    if ($config['gamecp_smtp_enable_ssl'] == 1)
    {
      $mail -> SMTPSecure = "ssl";
    }
    $mail -> Host = $config['gamecp_smtp_server'];
    $mail -> Port = $config['gamecp_smtp_port'];
    $mail -> Username = $config['gamecp_smtp_username'];
    $mail -> Password = $config['gamecp_smtp_password'];
    $mail -> From = $config['lostpass_email'];
    $mail -> FromName = $config['server_name'];
    $mail -> Subject = $subject;
    $mail -> isHTML(true);

    foreach ($email_list as $key => $email)
    {
      $key = md5($email . ' ' . $salt);
      $temp_message = str_replace("[USEREMAIL]", $email, $message);
      $temp_message = str_replace("[USERKEY]", $key, $temp_message);
      $temp_message_plain = str_replace("[USEREMAIL]", $email, $plain_message);
      $temp_message_plain = str_replace("[USERKEY]", $key, $temp_message_plain);
      $mail -> AddAddress($email, $subject);
      $mail -> Body = $temp_message;
      $mail -> AltBody = $temp_message_plain;
      $result = $mail -> Send();
      if (!$result)
      {
        $failed[] = $email;
      }
      $mail -> ClearAllRecipients();
    }

    return $failed;
  }
  return $failed;
}

function getGUID()
{
  if (function_exists('com_create_guid'))
  {
    return com_create_guid();
  }
  else
  {
    mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45); // "-"
    $uuid = chr(123)// "{"
    . substr($charid, 0, 8) . $hyphen
    . substr($charid, 8, 4) . $hyphen
    . substr($charid, 12, 4) . $hyphen
    . substr($charid, 16, 4) . $hyphen
    . substr($charid, 20, 12)
    . chr(125); // "}"
    return $uuid;
  }
}

function calculate_credits($mty = 1, $num_of_payments = 20, $price_value = 5, $credits_value = 25, $check_price = false)
{
  global $c_price;
  global $c_credits;
  global $c_bonus;
  global $c_total;
  global $config;
  $muntiplier = $mty;
  $c_price = array ();
  $c_credits = array ();
  $c_bonus = array ();
  $c_total = array ();
  $i = 0;
  while ($i < $num_of_payments)
  {
    $raw_price = $price_value * $i;
    $raw_credits = $credits_value * $i * $muntiplier;
    $raw_bonus = ( $raw_credits * 1 + 10 * ( ( $raw_credits - 25 ) / 25 ) - $raw_credits );
    if (isset($config['gamecp_bonus_formula']))
    {
      $formula = ( $config['gamecp_bonus_formula'] );
      if (!empty($formula))
      {
        $formula = antiject($formula);
        $formula = str_replace("\$", "", $formula);
        $formula = str_replace("x", $raw_credits, $formula);
        eval("\$formula = {$formula};");
        if (( $formula ) && 0 <= $formula)
        {
          $raw_bonus = $formula;
        }
        else
        {
          $raw_bonus = 0;
        }
      }
    }
    $c_price[] = $raw_price;
    $c_credits[] = $raw_credits;
    $c_bonus[] = $raw_bonus;
    $c_total[] = $raw_credits + $raw_bonus;
    ++$i;
  }

  if ($check_price === false)
  {
    return false;
  }

  $key = array_search($check_price, $c_price);
  if ($key === FALSE)
  {
    return 0;
  }

  return $c_total[$key];
}

function get_credits_level($num_of_payments, $price_value, $check_price)
{
  global $c_price;
  global $config;
  $c_price = array ();
  $i = 0;
  while ($i < $num_of_payments)
  {
    $raw_price = $price_value * $i;
    $c_price[] = $raw_price;
    ++$i;
  }

  $key = array_search($check_price, $c_price);
  if ($key === FALSE)
  {
    return 1;
  }

  return ($key);
}

/*
 *
 * 	ITEM HELPER FUNCTIONS
 *
 */

function get_item_details($k_value, $u_value = null, $equip_type = null)
{
  global $upgrade_types;
  $item = array ();

  if ($equip_type != null || $equip_type === 0)
  {
    $item["id"] = $k_value;
    $item["type"] = $equip_type;
    $item["slot"] = -1;
  }
  else
  {
    $temp = get_item_code($k_value);
    $item["id"] = $temp["id"];
    $item["type"] = $temp["type"];
    $item["slot"] = $temp["slot"];
  }

  $item["slots"] = -1;
  $item["talics"] = array ();
  $item["images"] = "";

  if (isset($u_value) && in_array($item["type"], $upgrade_types) && -1 < $k_value && -1 < $u_value)
  {
    $temp = get_item_upgrade_info($u_value);
    $item["slots"] = $temp["slots"];
    $item["talics"] = $temp["talics"];
    $item["images"] = $temp["images"];
  }

  return $item;
}

function get_item_code($id)
{
  $item = array ();

  $k_value = $id;

  $item['id'] = ($k_value & 0xffff0000) >> (4 * 4);
  $item['type'] = ($k_value & 0x0000ff00) >> (2 * 4);
  $item['slot'] = ($k_value & 0x000000ff);

  return $item;
}

function get_item_db_code($id, $type, $slot)
{
  $item['id'] = $id << (4 * 4);
  $item['type'] = $type << (2 * 4);
  $item['slot'] = $slot;

  return $item['id'] | $item['type'] | $item['slot'];
}

function get_item_upgrade_info($u_value)
{
  $upgrade = array ();
  $u_value = dechex($u_value);
  $upgrades["slots"] = $u_value[0];
  $u_value = strrev($u_value);
  $upgrades["talics"] = array ();
  $upgrades["images"] = "";

  if (0 < $upgrades["slots"])
  {
    $i = 0;
    while ($i < $upgrades["slots"])
    {
      $talic_id = hexdec($u_value[$i]);
      $upgrades["talics"][] = $talic_id;
      $upgrades["images"] .= "<img class=\"talic\" src=\"./images/talics/t-" . sprintf("%02d", $talic_id) . ".png\" width=\"12\"/>";
      $i++;
    }
  }

  return $upgrades;
}

function add_market_armor_set($level, $categories, $price, $upgrades, $add_mercy, $add_grace, $add_darkness, $add_wisdom, $type, $guild = false)
{
  $armor_sets = array (
    "acc" => array ("war" => "aw", "lau" => "af", "ran" => "ar"),
    "bell" => array ("war" => "bw", "for" => "bf", "ran" => "br"),
    "cora" => array ("war" => "cw", "for" => "cf", "ran" => "cr")
  );

  $parts = array ("iu", "il", "ig", "is", "ih");
  $now = time();
  $status = 1;
  $description = "";
  $amount = 0;
  $custom_amount = 0;
  $level_label = "Level " . $level;

  if ($level == "dragon1" || $level == "dragon2")
  {
    if ($level == "dragon1")
    {
      $type = "d";
      $level = "01";
      $level_label = "Ultima Dragon Armor";
    }
    else if ($level == "dragon2")
    {
      $type = "d";
      $level = "02";
      $level_label = "Freedom Dragon Armor";
    }
  }

  foreach ($armor_sets as $race => $sets)
  {
    $race_code = "1";
    $race_str = "Bellato";
    if ($race == "acc")
    {
      $race_code = "3";
      $race_str = "Accretian";
    }
    if ($race == "cora")
    {
      $race_code = "2";
      $race_str = "Cora";
    }

    foreach ($sets as $class => $item_code_part)
    {
      $category = $categories[$race . "_" . $class . "_cat"];
      $class_label = "Warrior";
      if ($class == "lau")
      {
        $class_label = "Launcher";
      }
      if ($class == "ran")
      {
        $class_label = "Ranger";
      }
      if ($class == "for")
      {
        $class_label = "Force";
      }
      foreach ($parts as $key => $part)
      {
        $part_label = "Upper";
        if ($part == "il")
        {
          $part_label = "Lower";
        }
        if ($part == "ig")
        {
          $part_label = "Gloves";
        }
        if ($part == "is")
        {
          $part_label = "Boots";
        }
        if ($part == "ih")
        {
          $part_label = "Helmet";
        }
        $description = $level_label . " " . $race_str . " " . $class_label . " " . $part_label;
        $item_image = "";
        $full_code = $part . $item_code_part . $type . $level;
        if (file_exists("./images/items/" . $full_code . ".png"))
        {
          $image_url = "./images/items/" . $full_code . ".png";
        }

        $attempt = get_item_info_from_code($full_code);
        $item_info = $attempt["item"];
        $item_id = $item_info["item_id"];
        $item_kind = parse_item_code($full_code);
        $table_name = get_table_name($item_kind);
        $item_name = str_replace("_", " ", $item_info['item_name']);
        $dbcode = get_item_db_code($item_id, $item_kind, 0);

        add_market_item($status, $category, $now, $now, $race_code, $price, $dbcode, $upgrades, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);

        if ($part == "is" && $add_mercy == 1)
        {
          $new_code = substr(dechex($upgrades), 0, 1) . str_replace("5", "C", substr(dechex($upgrades), 1));
          $new_code = hexdec($new_code);
          add_market_item($status, $category, $now, $now, $race_code, $price * 2, $dbcode, $new_code, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);
        }
        else if ($part == "ig")
        {
          if ($add_grace == 1)
          {
            $new_code = substr(dechex($upgrades), 0, 1) . str_replace("5", "B", substr(dechex($upgrades), 1));
            $new_code = hexdec($new_code);
            add_market_item($status, $category, $now, $now, $race_code, $price * 2, $dbcode, $new_code, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);
          }
          if ($add_darkness == 1)
          {
            $new_code = substr(dechex($upgrades), 0, 1) . str_replace("5", "2", substr(dechex($upgrades), 1));
            $new_code = hexdec($new_code);
            add_market_item($status, $category, $now, $now, $race_code, $price * 2, $dbcode, $new_code, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);
          }
        }
        else if ($part == "ih" && $add_wisdom == 1)
        {
          $new_code = substr(dechex($upgrades), 0, 1) . str_replace("5", "6", substr(dechex($upgrades), 1));
          $new_code = hexdec($new_code);
          add_market_item($status, $category, $now, $now, $race_code, $price * 2, $dbcode, $new_code, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);
        }
      }
    }
  }
}

function add_market_weapon_set($level, $category, $price, $upgrades, $type, $guild = false)
{
  $prefix = "iw";
  $parts = array ("kn", "sw", "ax", "ma", "st", "sp", "bo", "fi", "lu", "ge", "da");
  $acc_only = array ("lu", "ge");
  $bell_cora_only = array ("st");
  $type_holder;

  $now = time();
  $status = 1;
  $description = "";
  $amount = 0;
  $custom_amount = 0;

  foreach ($parts as $key => $part)
  {
    //grenade launchers are only every 10 levels until level 50
    if ($part == 'ge' && ($level == 15 || $level = 25 || $level = 35 || $level == 45))
    {
      continue;
    }

    //CCR didn't know what they wanted to do with throwing knives...
    if ($part == "da" && ($level >= 60 || $level <= 30))
    {
      $type = "a";
    }
    if ($part == "da" && $level <= 50)
    {
      $part = "dk";
    }

    $race_code = 0;
    if (in_array($part, $acc_only))
    {
      $race_code = 3;
    }
    if (in_array($part, $bell_cora_only))
    {
      $race_code = 4;
    }

    $type_holder = $type;
    $full_code = $prefix . $part . $type . $level;
    $type = $type_holder;

    $item_image = "";
    if (file_exists("./images/items/" . $full_code . ".png"))
    {
      $image_url = "./images/items/" . $full_code . ".png";
    }

    $attempt = get_item_info_from_code($full_code);
    $item_info = $attempt["item"];
    $item_id = $item_info["item_id"];
    $item_kind = parse_item_code($full_code);
    $table_name = get_table_name($item_kind);
    $item_name = str_replace("_", " ", $item_info['item_name']);
    $dbcode = get_item_db_code($item_id, $item_kind, 0);
    add_market_item($status, $category, $now, $now, $race_code, $price, $dbcode, $upgrades, $item_name, $description, $item_image, $amount, $custom_amount, 0, $guild);
  }
}

function get_ref_code($serial)
{
  $return = $serial;
  $return = $return + 10;
  $return = $return * 12845;
  return $return;
}

function get_serial_from_ref($ref)
{
  $return = $ref;
  $return = $return / 12845;
  $return = $return - 10;
  return $return;
}

function get_table_name($index)
{
  $table_names = Array ();
  $table_names[] = "tbl_code_upper";
  $table_names[] = "tbl_code_lower";
  $table_names[] = "tbl_code_gauntlet";
  $table_names[] = "tbl_code_shoe";
  $table_names[] = "tbl_code_helmet";
  $table_names[] = "tbl_code_shield";
  $table_names[] = "tbl_code_weapon";
  $table_names[] = "tbl_code_cloak";
  $table_names[] = "tbl_code_ring";
  $table_names[] = "tbl_code_amulet";
  $table_names[] = "tbl_code_bullet";
  $table_names[] = "tbl_code_maketool";
  $table_names[] = "tbl_code_bag";
  $table_names[] = "tbl_code_potion";
  $table_names[] = "tbl_code_face";
  $table_names[] = "tbl_code_force";
  $table_names[] = "tbl_code_battery";
  $table_names[] = "tbl_code_ore";
  $table_names[] = "tbl_code_resource";
  $table_names[] = "tbl_code_unitkey";
  $table_names[] = "tbl_code_booty";
  $table_names[] = "tbl_code_map";
  $table_names[] = "tbl_code_town";
  $table_names[] = "tbl_code_battledungeon";
  $table_names[] = "tbl_code_animus";
  $table_names[] = "tbl_code_guardtower";
  $table_names[] = "tbl_code_trap";
  $table_names[] = "tbl_code_siegekit";
  $table_names[] = "tbl_code_ticket";
  $table_names[] = "tbl_code_event";
  $table_names[] = "tbl_code_recovery";
  $table_names[] = "tbl_code_box";
  $table_names[] = "tbl_code_firecracker";
  $table_names[] = "tbl_code_unmannedminer";
  $table_names[] = "tbl_code_radar";
  $table_names[] = "tbl_code_npclink";
  return $table_names[$index];
}

function get_image_folder_name($index)
{
  $table_names = Array ();
  $table_names[] = "upper";
  $table_names[] = "lower";
  $table_names[] = "gauntlet";
  $table_names[] = "shoe";
  $table_names[] = "helmet";
  $table_names[] = "shield";
  $table_names[] = "weapon";
  $table_names[] = "cloak";
  $table_names[] = "ring";
  $table_names[] = "amulet";
  $table_names[] = "bullet";
  $table_names[] = "maketool";
  $table_names[] = "bag";
  $table_names[] = "potion";
  $table_names[] = "face";
  $table_names[] = "force";
  $table_names[] = "battery";
  $table_names[] = "ore";
  $table_names[] = "resource";
  $table_names[] = "unitkey";
  $table_names[] = "booty";
  $table_names[] = "map";
  $table_names[] = "town";
  $table_names[] = "battledungeon";
  $table_names[] = "animus";
  $table_names[] = "guardtower";
  $table_names[] = "trap";
  $table_names[] = "siegekit";
  $table_names[] = "ticket";
  $table_names[] = "potion"; //event
  $table_names[] = "potion"; //recovery
  $table_names[] = "potion"; //box
  $table_names[] = "potion"; //box
  $table_names[] = "unmannedminer";
  $table_names[] = "radar";
  $table_names[] = "npclink";
  return $table_names[$index];
}

function parse_item_code($code)
{
  if (strlen($code) >= 2)
  {
    switch (substr($code, 0, 2))
    {
      case "iu": return 0;
      case "il": return 1;
      case "ig": return 2;
      case "is": return 3;
      case "ih": return 4;
      case "id": return 5;
      case "iw": return 6;
      case "ik": return 7;
      case "ii": return 8;
      case "ia": return 9;
      case "ib": return 10;
      case "im": return 11;
      case "ie": return 12;
      case "ip": return 13;
      case "if": return 14;
      case "ic": return 15;
      case "it": return 16;
      case "io": return 17;
      case "ir": return 18;
      case "in": return 19;
      case "iy": return 20;
      case "iz": return 21;
      case "iq": return 22;
      case "ix": return 23;
      case "ij": return 24;
      case "gt": return 25;
      case "tr": return 26;
      case "sk": return 27;
      case "ti": return 28;
      case "ev": return 29;
      case "re": return 30;
      case "bx": return 31;
      case "fi": return 32;
      case "un": return 33;
      case "rd": return 34;
      case "lk": return 35;
      default: return -1;
    }
  }
  else
  {
    return -1;
  }
}

function getRace($strrace)
{
  $race = "Unknown";
  if (is_numeric($strrace))
  {
    if ((int) $strrace == 0 || (int) $strrace == 1)
    {
      $race = "Bellato";
    }
    if ((int) $strrace == 2 || (int) $strrace == 3)
    {
      $race = "Cora";
    }
    else if ((int) $strrace == 4)
    {
      $race = "Accretia";
    }
  }
  else
  {
    $racechar = substr($strrace, 0, 1);
    if ($racechar == "C")
    {
      $race = "Cora";
    }
    else if ($racechar == "B")
    {
      $race = "Bellato";
    }
    else if ($racechar == "A")
    {
      $race = "Accretia";
    }
  }
  return $race;
}

function parse_base_race($race)
{
  if ($race == 0 || $race == 1)
  {
    return 1;
  }
  if ($race == 2 || $race == 3)
  {
    return 2;
  }
  return 3;
}

function status($status)
{
  switch ($status)
  {
    case 0:
      return "Open";
    case 1:
      return "Pending";
    case 2:
      return "Closed";
  }
}

function get_item_image($icon_id, $type)
{
  $item_image = "./images/items/unknown.gif";
  if (isset($icon_id))
  {
    $image_path = glob("./images/items/" . get_image_folder_name($type) . "/(" . $icon_id . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
    if (file_exists(@$image_path[0]))
    {
      $item_image = $image_path[0];
    }
  }

  return $item_image;
}
