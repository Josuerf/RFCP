<?php

error_reporting(E_ALL);

define("THIS_APP", "rfcp");
define("IN_GAMECP_SALT58585", true);

include( "./core/common.php" );

$action = isset($_REQUEST['action']) ? antiject($_REQUEST['action']) : "";

if ($action == "" && $notuser)
{
  $lefttitle = $program_name;
  $lefttitle = "";
  $title = $program_name;

  $out .= get_login_html();
}
else if ($action == "" && $isuser)
{
  $lefttitle = "Game CP";
  $title = $program_name;
  $out .= "<h2 style=\"text-align: center;\">Welcome <strong><i>" . $userdata -> username . "</i></strong> to the RF Control Panel</h2>" . "\n";
  $out .= "<p><b>Novus Credits:</b> <i>" . number_format($userdata -> credits, 2) . "</i></p>" . "\n";
  $out .= "<p><b>Account E-Mail:</b> <i>" . $userdata -> email . "</i></p>" . "\n";
  $out .= "<p><b>Last Log In Time:</b> <i>" . $userdata -> lastlogintime -> format('m/d/Y h:i:s a') . "</i></p>" . "\n";
  $out .= "<p><b>Last Log Off Time:</b> <i>" . $userdata -> lastlogofftime -> format('m/d/Y h:i:s a') . "</i></p>" . "\n";
  $out .= "<p><b>Last Connect IP Address:</b> <i>" . ( $userdata -> lastconnectip != 0 ? $userdata -> lastconnectip : "None" ) . "</i></p>" . "\n";
  $out .= "<p><b>Current State:</b> <i>" . ( $userdata -> status ? "<span style=\"color: green;\">Online</span>" : "<span style=\"color: red;\">Offline</span>" ) . "</i></p>" . "\n";
  $path = explode("/", $_SERVER['REQUEST_URI']);
  unset($path[count($path) - 1]);
  $path = implode("/", $path);
  $ref_link = "http://" . $_SERVER["HTTP_HOST"] . $path . "/" . "rfcp_register.php?ref=" . get_ref_code($userdata -> serial);
  $out .= "<p class=\"ink-form\"><b>Referral Link:</b> &nbsp;&nbsp; <input type=\"text\" onclick=\"$(this).select();\" style=\"width: 600px;\" value=\"" . $ref_link . "\" /></p>";

  $fbStatus = "<fb:login-button scope='public_profile,email' onlogin='checkLoginState();' id='fb-login-btn' style='display: " . ($userdata -> facebookUserId ? 'none' : 'inline-block') . ";'>Connect with facebook</fb:login-button>";
  $fbStatus .= "<span id='fb-connected' style='display: " . ($userdata -> facebookUserId ? 'inline-block' : 'none') . ";'><a href='https://www.facebook.com/" . $userdata -> facebookUserId . "' target='_blank' id='fb-user' style='color: green;'>Linked with facebook</a> &nbsp;&nbsp; <button class='ink-button' type='button' style='height: 25px; padding: 0px 12px; margin: 0px;' onClick='unlinkFacebook()'>Unlink</button></span>";
  $out .= "<p><b>Facebook:</b> &nbsp;&nbsp;" . $fbStatus . "\n";

  if (isset($config["banner_url"]) && $config["banner_url"] != "")
  {
    $out .= "<p class=\"ink-form\"><b>Referral Banner HTML:</b> &nbsp;&nbsp; <textarea onclick=\"$(this).select();\" style=\"width: 500px; height: 80px;\"><a href=\"" . $ref_link . "\"><img src=\"" . $config["banner_url"] . "\" /></a></textarea></p>";
    $out .= "<p class=\"ink-form\"><b>Referral Banner BB Code:</b> &nbsp;&nbsp; <textarea onclick=\"$(this).select();\" style=\"width: 500px; height: 80px;\">[URL=\"" . $ref_link . "\"][IMG]" . $config["banner_url"] . "[/IMG][/URL]</textarea></p>";
  }
}
else
{
  $action = str_replace(".", "", $action);
  $action = str_replace("\\", "", $action);
  $action = str_replace("/", "", $action);
  if (!file_exists("./modules/" . $action . ".php"))
  {
    $out .= get_notification_html("Page Not Found", ERROR);
    $lefttitle = "Page Not Found";
  }
  else
  {
    include( "./modules/" . $action . ".php" );
  }

  close_all_db();
}

if ($out_type == OUTPUT_HTML)
{
  $output = gamecp_template("gamecp");
  print_outputs($output, $redirect, $redirecturl);
}
else
{
  print_json($out_json);
}
