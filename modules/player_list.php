<?php

/**
 * @author Mauro Tamm   @ mauro.tamm@gmail.com
 * @author Agony @ agony@nxtdeveloper.com
 * @copyright 2014 http://nxtdeveloper.com/
 * @ver 5.0
 */
if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Server";
  $moduleLabel = "Player List";
  $permission = "all";
  return;
}
if ($this_script == $script_name)
{

  //functions
  function race($race)
  {
    switch ($race)
    {
      case 0:
      case 1:
        return "<img src=\"./images/bell_colored.png\" title=\"Bellato\" style=\"height: 35px;\">";
      case 2:
      case 3:
        return "<img src=\"./images/cora_colored.png\" title=\"Cora\" style=\"height: 35px;\">";
      case 4:
        return "<img src=\"./images/acc_colored.png\" title=\"Accretia\" style=\"height: 35px;\">";
      default:
        return "Invalid Race";
    }
  }

  $sort_by = isset($_GET["sortby"]) ? $_GET["sortby"] : "";
  if ($sort_by == "level")
  {
    $sort = "B.lv";
  }
  else if ($sort_by == "time")
  {
    $sort = "G.TotalPlayMin";
  }
  else
  {
    $sort = "G.PvpPoint";
  }

  //pagination
  include( "./core/pagination.php" );

  $sql = "SELECT B.Serial, B.Name, B.race, B.Class, B.lv, B.LastConnTime, G.TotalPlayMin, G.PvpPoint, P.GuildName ";
  $sql .= "FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial LEFT JOIN tbl_PvpRankToday AS P ON B.Serial = P.Serial ";
  $sql .= "WHERE B.DCK = '0' AND B.Account NOT LIKE '!%' AND P.GuildName IS NOT NULL AND B.Serial NOT IN ";
  $sql .= "(SELECT TOP [OFFSET] B.Serial FROM tbl_base AS B INNER JOIN tbl_general AS G ON B.Serial = G.Serial ";
  $sql .= "WHERE B.DCK = '0' AND B.Account NOT LIKE '!%' AND P.GuildName IS NOT NULL ";
  $sql .= "ORDER BY " . $sort . " DESC) ORDER BY " . $sort . " DESC";

  $sql_count = "SELECT COUNT(*) FROM tbl_base AS B LEFT JOIN tbl_PvpRankToday AS P ON B.Serial = P.Serial WHERE B.DCK = '0' AND B.Account NOT LIKE '!%' AND P.GuildName IS NOT NULL";
  $url = "" . $script_name . "?action=" . $_GET['action'] . "&sortby=" . $sort_by;
  $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size = 20, $links_to_show = 11);
  $results = $pager -> get_data();

  $out .= "<table class = 'ink-table'>";
  $out .= "<thead>";
  $out .= "<tr>";
  $out .= "<th class='align-center' style=\"width: 30px;\">Rank</th>";
  $out .= "<th class='align-center'>Name</th>";
  $out .= "<th class='align-center'>Race</th>";
  $out .= "<th class='align-center' style=\"width: 30px;\"><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=level\">Level</a></th>";
  $out .= "<th class='align-center'>Class</th>";
  $out .= "<th class='align-center'><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=time\">Play Time</a></th>";
  $out .= "<th class='align-center'><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=pvp\">PvP Points</a></th>";
  $out .= "<th class='align-center'>Guild</th>";
  $out .= "</tr>";
  $out .= "</thead>";
  $out .= "<tbody>";

  foreach ($results["rows"] as $key => $row)
  {
    $hours = intval($row['TotalPlayMin'] / 60);
    $minutes = $row['TotalPlayMin'] - ($hours * 60);
    $rank = (($pager -> getCurrentPage() - 1) * $page_size) + $key + 1;


    $class = get_classes();
    $class_by_id = $class['classesByCode'];


    $out .= "   <tr>";
    $out .= "       <td align='middle'>" . $rank . "</td>";
    $out .= "       <td align='middle'>" . $row['Name'] . "</td>";
    $out .= "       <td align='middle'>" . race($row['race']) . "</td>";
    $out .= "       <td align='middle'>" . $row['lv'] . "</td>";
    $out .= "       <td align='middle'>" . $class_by_id[$row['Class']] . "</td>";
    $out .= "       <td align='middle'>" . number_format($hours) . "h &nbsp; " . $minutes . "m" . "</td>";
    $out .= "       <td align='middle'>" . number_format(round($row['PvpPoint'])) . "</td>";
    $out .= "       <td align='middle'>" . $row['GuildName'] . "</td>";
    $out .= "   </tr>";
  }
  $out .= "</tbody>";
  $out .= "<tfoot>";

  if (count($results["rows"]) <= 0)
  {
    $out .= "<tr>";
    $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">No Characters found.</td>";
    $out .= "</tr>";
  }
  else
  {
    $out .= "<tr>";
    $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
    $out .= "</tr>";
  }
  $out .= "</tfoot>";
  $out .= "</table>";
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
