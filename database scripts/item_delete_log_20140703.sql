USE [RF_GAMECP]
GO

/****** Object:  Table [dbo].[gamecp_delete_item_log]    Script Date: 07/03/2014 23:12:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gamecp_delete_item_log](
	[delete_id] [int] IDENTITY(1,1) NOT NULL,
	[delete_account_id] [int] NOT NULL,
	[delete_char_id] [int] NOT NULL,
	[delete_k] [int] NOT NULL,
	[delete_u] [int] NOT NULL,
	[delete_d] [bigint] NOT NULL,
	[delete_r] [int] NOT NULL,
	[delete_remover_id] [int] NOT NULL,
	[delete_time] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_delete_log] PRIMARY KEY CLUSTERED 
(
	[delete_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


