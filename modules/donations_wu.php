<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Novus Market";
  $moduleLabel = "Recharge Credits - Western Union";
  $permission = "player";
  return;
}
if ($this_script == $script_name)
{
  $exit_stage = 0;
  if ($isuser == true)
  {
    $out .= "<b>All Western Union donations are manual and will take time to confirm.</b></br>";
    $out .= "<span>Prices are in USD, same as paypal donations.</span></br>";
    $out .= "<span>Minimum donation amount 50$, all fees paid by YOU.</span></br>";
    $out .= "<span>Please add account name to the payment description.</span></br>";
    $out .= "<span>Make sure the amount donated is not a odd amount.</span></br>";
    $out .= "<span>Wrong amounts will be rounded downwards.</span></br>";
    $out .= "<span><b>PS!!! DONATIONS are final AND are not eligible for refund.</b></span></br>";
    $out .= "</br>";
    $out .= "<span>Name: ARON VAN DE VEN</span></br>";
    $out .= "<span>Email: SUP_PUPPY@HOTMAIL.COM</span></br>";
    $out .= "<span>Country: Nederland</span></br>";
    $out .= "<span>City: VELDHOVEN</span></br>";
    $out .= "<a href='https://www.westernunion.com/us/en/send-money/start.html'>Western Union</a></br>";
    $out .= "</br>";
    $out .= "<span></span>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
