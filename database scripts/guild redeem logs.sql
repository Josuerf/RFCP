USE [RF_GAMECP]
GO

/****** Object:  Table [dbo].[gamecp_g_redeem_log]    Script Date: 01/12/2017 23:49:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gamecp_g_redeem_log](
	[redeem_id] [int] IDENTITY(1,1) NOT NULL,
	[redeem_account_id] [int] NOT NULL,
	[redeem_char_id] [int] NOT NULL,
	[redeem_price] [real] NOT NULL,
	[redeem_total_gp] [real] NOT NULL,
	[redeem_item_id] [int] NOT NULL,
	[redeem_item_name] [varchar](255) NULL,
	[redeem_item_dbcode] [bigint] NULL,
	[redeem_item_amount] [bigint] NOT NULL,
	[redeem_time] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_g_redeem_log] PRIMARY KEY CLUSTERED 
(
	[redeem_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[gamecp_g_redeem_log] ADD  CONSTRAINT [DF_gamecp_g_redeem_log_redeem_price]  DEFAULT ((0)) FOR [redeem_price]
GO

ALTER TABLE [dbo].[gamecp_g_redeem_log] ADD  CONSTRAINT [DF_gamecp_g_redeem_log_redeem_total_gp]  DEFAULT ((0)) FOR [redeem_total_gp]
GO

ALTER TABLE [dbo].[gamecp_g_redeem_log] ADD  CONSTRAINT [DF_gamecp_g_redeem_log_redeem_item_amount]  DEFAULT ((0)) FOR [redeem_item_amount]
GO


