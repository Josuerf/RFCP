USE [RF_GAMECP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_referrals](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[referred_by_serial] [int] NULL,
	[new_user_name] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


