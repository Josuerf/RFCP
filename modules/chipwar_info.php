<?php

/**
 * @author Mauro Tamm   @ mauro.tamm@gmail.com
 * @author Agony @ agony@nxtdeveloper.com
 * @copyright 2014 http://nxtdeveloper.com/
 * @ver 5.0
 */
if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Server";
  $moduleLabel = "CW Status";
  $permission = "all";
  return;
}
if ($this_script == $script_name)
{

  //get cw numbers data
  function get_race($code)
  {
    switch ($code)
    {
      case 0:
        return "<img src=\"./images/bell_colored.png\" title=\"Bellato\" style=\"height: 35px;\">";
      case 1:
        return "<img src=\"./images/cora_colored.png\" title=\"Cora\" style=\"height: 35px;\">";
      case 2:
        return "<img src=\"./images/acc_colored.png\" title=\"Accretia\" style=\"height: 35px; margin-left: -3px;\">";
      case 255:
        return "-";
    }
  }

  $dbcon = connectdb(DATA);

  //win&lose count queries
  $b_win_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE winrace = '0'";
  $c_win_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE winrace = '1'";
  $a_win_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE winrace = '2'";

  $b_lose_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE loserace = '0'";
  $c_lose_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE loserace = '1'";
  $a_lose_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE loserace = '2'";

  $a_draw_q = "SELECT COUNT(idx) FROM tbl_racebattle_log WHERE winrace = '255' AND loserace = '255'";

  $b_win_s = sqlsrv_query($dbcon, $b_win_q, array (), array ("Scrollable" => 'static'));
  $c_win_s = sqlsrv_query($dbcon, $c_win_q, array (), array ("Scrollable" => 'static'));
  $a_win_s = sqlsrv_query($dbcon, $a_win_q, array (), array ("Scrollable" => 'static'));

  $b_lose_s = sqlsrv_query($dbcon, $b_lose_q, array (), array ("Scrollable" => 'static'));
  $c_lose_s = sqlsrv_query($dbcon, $c_lose_q, array (), array ("Scrollable" => 'static'));
  $a_lose_s = sqlsrv_query($dbcon, $a_lose_q, array (), array ("Scrollable" => 'static'));

  $a_draw_s = sqlsrv_query($dbcon, $a_draw_q, array (), array ("Scrollable" => 'static'));

  //win&lose count
  $b_win = sqlsrv_fetch_array($b_win_s);
  $b_win = $b_win[0];
  $c_win = sqlsrv_fetch_array($c_win_s);
  $c_win = $c_win[0];
  $a_win = sqlsrv_fetch_array($a_win_s);
  $a_win = $a_win[0];
  $b_lose = sqlsrv_fetch_array($b_lose_s);
  $b_lose = $b_lose[0];
  $c_lose = sqlsrv_fetch_array($c_lose_s);
  $c_lose = $c_lose[0];
  $a_lose = sqlsrv_fetch_array($a_lose_s);
  $a_lose = $a_lose[0];
  $a_draw = sqlsrv_fetch_array($a_draw_s);
  $a_draw = $a_draw[0];


  $out .= "<script type='text/javascript' src='https://www.google.com/jsapi'></script>
            <script type='text/javascript'>
                google.load('visualization', '1', {packages:['corechart']});
                google.setOnLoadCallback(drawChart1);
                function drawChart1() {
                    var data = google.visualization.arrayToDataTable([
                      ['Race', 'Victories'],
                      ['Bellato',   $b_win],
                      ['Cora',      $c_win],
                      ['Accretia',  $a_win]
                    ]);

                    var options = {
                    title: 'CW Victories',
                    slices: {
                                0: { color: 'cc6699' },
                                1: { color: '#9933cc' },
                                2: { color: '#cccccc' }
                            }
                    };
                var chart = new google.visualization.PieChart(document.getElementById('piechartwin'));
                chart.draw(data, options);
                };

                google.setOnLoadCallback(drawChart2);
                function drawChart2() {
                    var data = google.visualization.arrayToDataTable([
                      ['Race', 'Losses'],
                      ['Bellato',   $b_lose],
                      ['Cora',      $c_lose],
                      ['Accretia',  $a_lose]
                    ]);

                    var options = {
                    title: 'CW Losses',
                    slices: {
                                0: { color: 'cc6699' },
                                1: { color: '#9933cc' },
                                2: { color: '#cccccc' }
                            }
                    };

                var chart = new google.visualization.PieChart(document.getElementById('piechartloss'));
                chart.draw(data, options);
                };

                google.setOnLoadCallback(drawChart3);
                function drawChart3() {
                    var data = google.visualization.arrayToDataTable([
                      ['','Number'],
                      ['',$a_draw]
                    ]);

                var options = {
                  title: 'Draws',
                  legend: 'none'
                };

                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                chart.draw(data, options);
              };


            </script>";

  $out .= "<div id='charts'style='width: 1080px; height: 250px;'>
              <div id='piechartwin' style='width: 490px; height: 250px; float: left'></div>
              <div id='piechartloss' style='width: 490px; height: 250px; float: left'></div>
              <div id='chart_div' style='width: 100px; height: 250px; float: right'></div>
              </div>";

  //pagination
  include( "./core/pagination.php" );

  $sql = "SELECT starttime,winrace,loserace,regdate FROM racebattle_log_view ";
  $sql .= "WHERE starttime NOT IN (SELECT TOP [OFFSET] ";
  $sql .= "starttime FROM racebattle_log_view ";
  $sql .= "ORDER BY starttime DESC) ORDER BY starttime DESC";

  $sql_count = "SELECT COUNT(*) FROM racebattle_log_view";
  $url = "" . $script_name . "?action=" . $_GET['action'];
  $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size = 50, $links_to_show = 11);
  $results = $pager -> get_data();

  $y = 0;
  $z = 0;
  $date = 0;
  $cwdata = array ();

  foreach ($results["rows"] as $key => $row)
  {
    $cwrow = array ();

    if ($row['starttime'] -> format('Y-m-d') == $date)
    {
      $cwrow['time'] = $row['starttime'];
      $cwrow['endtime'] = $row['regdate'];
      $cwrow['win'] = $row['winrace'];
      $cwrow['lose'] = $row['loserace'];
      $z += 1;
    }
    else
    {
      $z = 0;
      $cwrow['time'] = $row['starttime'];
      $cwrow['endtime'] = $row['regdate'];
      $cwrow['win'] = $row['winrace'];
      $cwrow['lose'] = $row['loserace'];
      $y += 1;
    }

    $cwdata[$y][$z] = $cwrow;
    $date = $row['starttime'] -> format('Y-m-d');
  }

  $cwtime = array (
    DateTime::createFromFormat('H-i-s', '04-00-00'),
    DateTime::createFromFormat('H-i-s', '12-00-00'),
    DateTime::createFromFormat('H-i-s', '20-00-00')
  );

  $out .= "<table class = 'ink-table'>";
  $out .= "<thead>";
  $out .= "<tr>";
  $out .= "<th rowspan='2' class='align-left'>Date</th>";
  $out .= "<th colspan='3' class='align-left'>05:00 UTC</th>";
  $out .= "<th colspan='3' class='align-left'>13:00 UTC</th>";
  $out .= "<th colspan='3' class='align-left'>21:00 UTC</th>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<th class='align-left'>Win</th>";
  $out .= "<th class='align-left'>Lose</th>";
  $out .= "<th class='align-left'>Duration</th>";
  $out .= "<th class='align-left'>Win</th>";
  $out .= "<th class='align-left'>Lose</th>";
  $out .= "<th class='align-left'>Duration</th>";
  $out .= "<th class='align-left'>Win</th>";
  $out .= "<th class='align-left'>Lose</th>";
  $out .= "<th class='align-left'>Duration</th>";
  $out .= "</tr>";
  $out .= "</thead>";
  $out .= "<tbody>";

  foreach ($cwdata as $data)
  {
    $out .= "<tr>";

    $out .= "<td>" . $data[0]['time'] -> format('Y-m-d') . "</td>";
    if (array_key_exists(0, $data))
    {
      $out .= "<td>" . get_race($data[0]['win']) . "</td>";
      $out .= "<td>" . get_race($data[0]['lose']) . "</td>";
      $out .= "<td>" . str_replace(array ("0h", " 0min"), "", $data[0]['time'] -> diff($data[0]['endtime']) -> format('%hh %imin')) . "</td>";
    }
    else
    {
      $out .= "<td>-</td>";
      $out .= "<td>-</td>";
      $out .= "<td>NO CW</td>";
    }
    if (array_key_exists(1, $data))
    {
      $out .= "<td>" . get_race($data[1]['win']) . "</td>";
      $out .= "<td>" . get_race($data[1]['lose']) . "</td>";
      $out .= "<td>" . str_replace(array ("0h", " 0min"), "", $data[1]['time'] -> diff($data[1]['endtime']) -> format('%hh %imin')) . "</td>";
    }
    else
    {
      $out .= "<td>-</td>";
      $out .= "<td>-</td>";
      $out .= "<td>NO CW</td>";
    }
    if (array_key_exists(2, $data))
    {
      $out .= "<td>" . get_race($data[2]['win']) . "</td>";
      $out .= "<td>" . get_race($data[2]['lose']) . "</td>";
      $out .= "<td>" . str_replace(array ("0h", " 0min"), "", $data[2]['time'] -> diff($data[2]['endtime']) -> format('%hh %imin')) . "</td>";
    }
    else
    {
      $out .= "<td>-</td>";
      $out .= "<td>-</td>";
      $out .= "<td>NO CW</td>";
    }
    $out .= "</tr>";
  }
  $out .= "</tbody>";
  $out .= "<tfoot>";
  if (count($results["rows"]) <= 0)
  {
    $out .= "<tr>";
    $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">No CW entries found.</td>";
    $out .= "</tr>";
  }
  else
  {
    $out .= "<tr>";
    $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
    $out .= "</tr>";
  }
  $out .= "</tfoot>";
  $out .= "</table>";
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
